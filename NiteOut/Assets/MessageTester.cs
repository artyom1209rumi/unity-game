using NiteOut.Model;
using NiteOut.View;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageTester : MonoBehaviour
{
    private const string abbesID = "aeSmcKcKqbToh2Q5pYdW6yrI9uk2";
    private const string ddID = "NvPIZ32jKSSLVdGfb4hZhjnigNt1";
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.O))
        {
            DatabaseHelper.Database.OpenConversation(abbesID, DatabaseHelper.User.UserID, () =>
            {
                print("Open Chat Here");
                //ChatUI.Instance.OpenChat();
            });
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            DatabaseHelper.Database.OpenConversation(ddID, DatabaseHelper.User.UserID, () =>
            {
                print("Open Chat Here");
                //ChatUI.Instance.OpenChat();
            });
        }
        */
        if (Input.GetKeyDown(KeyCode.A))
        {
            DatabaseHelper.Database.OpenConversationWith(ddID, () =>
            {
                print("Open Chat Here");
            });
        }
        if (Input.GetKeyDown(KeyCode.S))
            DatabaseHelper.Database.SendChatMessage(new ChatMessage(DatabaseHelper.User.UserID, abbesID, "Hello Abbes", MessageType.Text));
        if (Input.GetKeyDown(KeyCode.R))
            DatabaseHelper.Database.SendChatMessage(new ChatMessage(abbesID, DatabaseHelper.User.UserID, "Hello Hafid", MessageType.Text));
        if (Input.GetKeyDown(KeyCode.B))
            DatabaseHelper.Database.SendChatMessage(new ChatMessage(ddID, DatabaseHelper.User.UserID, "Hello Hafid", MessageType.Text));
    }
}
