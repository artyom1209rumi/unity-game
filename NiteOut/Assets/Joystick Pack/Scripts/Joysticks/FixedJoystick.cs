﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FixedJoystick : Joystick
{

    public override void OnPointerUp(PointerEventData eventData)
    {
        StopAllCoroutines();
        StartCoroutine(WaitForFrame(eventData));
    }

    public bool IsJoystickMoving()
    {
        return input != Vector2.zero;
    }

    private IEnumerator WaitForFrame(PointerEventData eventData)
    {
        yield return new WaitForEndOfFrame();
        base.OnPointerUp(eventData);
    }
}