using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfileModelStudio : MonoBehaviour
{
    public static ProfileModelStudio Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<ProfileModelStudio>();
            return instance;
        }
    }

    private static ProfileModelStudio instance;
    private GameObject curModel;
    [SerializeField] Vector3 SpawnRotation;

    public void UpdateModel(GameObject model)
    {
        if (curModel)
        {
            Destroy(curModel);
        }
        curModel = Instantiate(model,transform.position, Quaternion.Euler(SpawnRotation),transform);
        curModel.SetActive(true);
    }
}
