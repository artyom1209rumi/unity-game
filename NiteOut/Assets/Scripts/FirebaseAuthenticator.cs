﻿using UnityEngine;
using Firebase.Auth;
using System;

namespace NiteOut.Model
{
    public class FirebaseAuthenticator : MonoBehaviour, IAuthenticator
    {
        private FirebaseAuth auth;

        public event Action LoggedIn;
        public event Action LoggedOut;
        public event Action Registered;

        public FirebaseAuth GetFirebaseAuth()
        {
            return auth;
        }

        public void Initialize()
        {
            auth = FirebaseAuth.DefaultInstance;
        }

        public void Login(string email, string password)
        {
            auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                else
                {
                    Firebase.Auth.FirebaseUser firebaseUser = task.Result;
                    print(firebaseUser.Email + " has been logged in successfully!");
                    LoggedIn?.Invoke();
                }
            });
        }

        public void Logout()
        {
            LoggedOut?.Invoke();
        }

        public void Register(string email, string password)
        {
            Registered?.Invoke();
        }
    }
}
