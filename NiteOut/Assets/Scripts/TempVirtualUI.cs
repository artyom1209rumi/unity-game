using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TempVirtualUI : MonoBehaviour
{
    public static TempVirtualUI Instance;
    public Joystick Joystick;

    private void Awake()
    {
        Instance = this;
    }
}
