﻿using System;

namespace NiteOut.Model
{
    public interface IAuthenticator
    {
        event Action LoggedIn;

        event Action LoggedOut;

        event Action Registered;

        void Initialize();

        void Register(string email, string password);

        void Login(string email, string password);

        void Logout();
    }
}
