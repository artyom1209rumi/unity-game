﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using API;

public interface ISpotifyApiDataRepository 
{
    void GetTopData(GetSpotifyUserTopDataRequest request, Action<GetSpotifyUserTopArtistResult> success, Action<string> error);
}
