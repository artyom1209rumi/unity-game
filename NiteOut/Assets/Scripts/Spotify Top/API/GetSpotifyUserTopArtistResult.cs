using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace API
{

    [System.Serializable]
    public class ExternalUrls
    {
        public string spotify;
    }

    [System.Serializable]
    public class Followers
    {
        public object href;
        public int total;
    }

    [System.Serializable]
    public class Image
    {
        public int height;
        public string url;
        public int width;
    }

    [System.Serializable]
    public class Item
    {
        public ExternalUrls external_urls;
        public Followers followers;
        public List<string> genres;
        public string href;
        public string id;
        public List<Image> images;
        public string name;
        public int popularity;
        public string type;
        public string uri;
    }

    public class GetSpotifyUserTopArtistResult
    {
        public List<Item> items;
        public int total;
        public int limit;
        public int offset;
        public object previous;
        public string href;
        public string next;
    }


}

