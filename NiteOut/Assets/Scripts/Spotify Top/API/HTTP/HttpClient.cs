﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class HttpClient : MonoBehaviour
{
	public static HttpClient instance;
	private readonly int TIMEOUT = 50000;

	private void Awake()
	{
		if (instance == null)
			instance = this;
	}



	public IEnumerator Get(string url, string jsonPrefix, Dictionary<string, string> headers, Action<HttpResult> action)
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			action.Invoke(new HttpResult("No internet"));
			yield break;
		}

		UnityWebRequest www = UnityWebRequest.Get(url);

        if(headers != null)
        {
			foreach (KeyValuePair<string, string> header in headers)
			{
				www.SetRequestHeader(header.Key, header.Value);
			}
		}
		
		yield return Send(www);
		action.Invoke(BuildResult(www, jsonPrefix));
	}

	public IEnumerator PostWithFormData(string url, string jsonPrefix, string jsonPerameter, Dictionary<string, string> headers, WWWForm formData, Action<HttpResult> action)
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			action.Invoke(new HttpResult("No Internet"));
			yield break;
		}
		UnityWebRequest request = UnityWebRequest.Post(url, formData);

		if (jsonPerameter != null && jsonPerameter != "")
		{
			byte[] bytes = Encoding.UTF8.GetBytes(jsonPerameter);

			UploadHandlerRaw uH = new UploadHandlerRaw(bytes);
			//uH.contentType = "application/json"; //this is ignored?
			request.uploadHandler = uH;

		}

		if (headers != null)
        {
			foreach (KeyValuePair<string, string> header in headers)
			{
				try
				{
					request.SetRequestHeader(header.Key, header.Value);
				}
				catch (Exception e)
				{
					//Debug.Log("UNITY>> Post req error - " + e.Message);
				}
			}
		}
		
		yield return Send(request);
		action.Invoke(BuildResult(request,jsonPrefix));
	}




	private IEnumerator Send(UnityWebRequest www)
	{
		float addTime = 0f;
		UnityWebRequestAsyncOperation op = www.SendWebRequest();

		while (true)
		{
			if (!op.isDone)
			{
				yield return null;
			}
			addTime += Time.deltaTime;
			if ((int)addTime >= TIMEOUT)
			{
				www.Abort();
				break;
			}
		}
	}

	private HttpResult BuildResult(UnityWebRequest www, string jsonPrefix = null)
	{
		string resText = "";
        if(jsonPrefix == null)
        {
			resText = www.downloadHandler.text;

        }
        else
        {
			resText = "{\""  + jsonPrefix + "\":" + www.downloadHandler.text + "}";
		}

		//Debug.Log("UNITY>> Res tExt " + resText);
		long resCode = www.responseCode;
		string url = www.url;
		Dictionary<string, string> headers = www.GetResponseHeaders();
		if (200 <= resCode && resCode <= 299) // 200 is success
		{
			return new HttpResult(headers, url, resText);
		}
	
		return new HttpResult("UNITY>> Can not get result from Api");
	}


	public interface Delegate
	{
		void OnNetworkCallComplete(bool isSuccess,string responseJson);
	}

}
