using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInstagramRepository 
{
    void GetAccessToken(InstagramAccessTokenRequest request, Action<InstagramAccessTokenResult> success, Action<string> error);
    void GetInstagramUserPosts(InstagramUserPostsRequest request, Action<InstagramUserPostResult> success, Action<string> error);

    //Local
    string GetInstagramLocalAccessToken();
    void StoreAccessTokenLocally(string token);
    void ClearInstagramAccessToken();
}
