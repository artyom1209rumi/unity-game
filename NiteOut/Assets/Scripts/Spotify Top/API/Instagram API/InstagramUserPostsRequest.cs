using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstagramUserPostsRequest 
{
    private string _accessToken;

    public InstagramUserPostsRequest(string accessToken)
    {
        _accessToken = accessToken;
    }

    public string ToUrl()
    {
        string url = ApiConfig.API_INSTAGRAM_GET_USER_POSTS;
        string parameters = "id,caption,media_url,username&access_token=";
        string accessToken = _accessToken;//ApiConfig.INSTAGRAM_ACCESS_TOKEN;

        url = url + parameters + accessToken;

        return url;
    }
    //public WWWForm ToFormData()
    //{
    //    WWWForm form = new WWWForm();
    //    form.AddField("client_id", "342102897919786");
    //    form.AddField("client_secret", "0ef9938804fd6ad9d7b41eb73d40377a");
    //    form.AddField("grant_type", "authorization_code");
    //    form.AddField("redirect_uri", "https://ashtoy.com/");
    //    form.AddField("code", ApiConfig.authCode);
    //    return form;
    //}

    //public Dictionary<string, string> ToCustomHeader()
    //{
    //    return ApiConfig.GetHeaders();
    //}
}
