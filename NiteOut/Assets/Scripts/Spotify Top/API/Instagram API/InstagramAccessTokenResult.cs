using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InstagramAccessTokenResult 
{
    public string access_token;
    public long user_id;
}
