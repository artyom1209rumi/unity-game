using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstagramAccessTokenRequest : MonoBehaviour
{
    private string _authCode;

    public InstagramAccessTokenRequest(string code)
    {
        _authCode = code;
    }
    public WWWForm ToFormData()
    {
        WWWForm form = new WWWForm();
        form.AddField("client_id", ApiConfig.INSTAGRAM_APP_ID);
        form.AddField("client_secret",ApiConfig.INSTAGRAM_APP_SECRECT);
        form.AddField("grant_type", "authorization_code");
        form.AddField("redirect_uri", ApiConfig.INSTAGRAM_REDIRECT_URL);
        form.AddField("code", _authCode);
        return form;
    }

    //public Dictionary<string, string> ToCustomHeader()
    //{
    //    return ApiConfig.GetHeaders();
    //}
}
