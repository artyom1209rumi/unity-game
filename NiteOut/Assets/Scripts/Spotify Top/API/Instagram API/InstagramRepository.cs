using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstagramRepository : MonoBehaviour,IInstagramRepository
{

    private HttpClient client = new HttpClient();
    //Local data
    private string pref_token = "PREF_INSTAGRAM_TOKEN";

    public void GetAccessToken(InstagramAccessTokenRequest request, Action<InstagramAccessTokenResult> success, Action<string> error)
    {
         PostRequest(ApiConfig.API_INSTAGRAM_GET_ACCESS_TOKEN, null, request.ToFormData(), success, error);
    }

    public void GetInstagramUserPosts(InstagramUserPostsRequest request, Action<InstagramUserPostResult> success, Action<string> error)
    {
        GetRequest(request.ToUrl(), null, success, error);
       // GetRequest(request.ToUrl(), null, request.ToFormData(), success, error);
    }

    private void PostRequest<RESULT>(string url, Dictionary<string, string> headers, WWWForm form, Action<RESULT> success, Action<string> error, string jsonPrefix = null, string jsonParams = null)
   {
       StartCoroutine(client.PostWithFormData(url, jsonPrefix, jsonParams, headers, form, (result) =>

       {
           if (result.success)
           {
               try
               {
                   RESULT entity = JsonUtility.FromJson<RESULT>(result.resultText);
                   success.Invoke(entity);
               }
               catch (Exception e)
               {
                   error.Invoke(e.Message);
               }
           }
           else
           {
               error.Invoke(result.error);
           }
       }));
   }

    private void GetRequest<RESULT>(string url, Dictionary<string, string> headers, Action<RESULT> success, Action<string> error, string jsonPrefix = null)
    {
        StartCoroutine(client.Get(url, jsonPrefix, headers, (result) =>
        {
            if (result.success)
            {
                try
                {
                    RESULT entity = JsonUtility.FromJson<RESULT>(result.resultText);
                    success.Invoke(entity);
                }
                catch (Exception e)
                {
                    error.Invoke(e.Message);
                }
            }
            else
            {
                error.Invoke(result.error);
            }
        }));
    }



    //Local 
    public string GetInstagramLocalAccessToken()
    {
        string value = PlayerPrefs.GetString(pref_token,"");
        Debug.Log("UNITY>> ACCESS TOKEN --- " + value);
        return value;
    }

    public void StoreAccessTokenLocally(string token)
    {
        PlayerPrefs.SetString(pref_token, token);
    }

    public void ClearInstagramAccessToken()
    {
        PlayerPrefs.SetString(pref_token, "");
    }
}
