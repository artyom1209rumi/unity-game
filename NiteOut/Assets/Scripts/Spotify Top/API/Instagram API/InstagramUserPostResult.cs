using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InstagramUserPostResult
{
    public List<Datum> data;
    public Paging paging;
}

[System.Serializable]
public class Datum
{
    public string id;
    public string caption;
    public string media_url;
    public string username;
}

[System.Serializable]
public class Cursors
{
    public string before;
    public string after;
}

[System.Serializable]
public class Paging
{
    public Cursors cursors;
}

