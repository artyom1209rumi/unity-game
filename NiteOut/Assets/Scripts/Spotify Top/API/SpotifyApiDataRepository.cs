﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using API;

public class SpotifyApiDataRepository : MonoBehaviour,ISpotifyApiDataRepository
{

    private HttpClient client = new HttpClient();

    public void GetTopData(GetSpotifyUserTopDataRequest request, Action<GetSpotifyUserTopArtistResult> success, Action<string> error)
    {
        GetRequest(ApiConfig.API_GET_CHANNEL_DATA_URL, request.ToCustomHeader(), success, error);
    }


    private void GetRequest<RESULT>(string url, Dictionary<string, string> headers, Action<RESULT> success, Action<string> error, string jsonPrefix = null)
    {
        StartCoroutine(client.Get(url, jsonPrefix, headers, (result) =>
        {
            if (result.success)
            {
                try
                {
                    RESULT entity = JsonUtility.FromJson<RESULT>(result.resultText);
                    success.Invoke(entity);
                }
                catch (Exception e)
                {
                    error.Invoke(e.Message);
                }
            }
            else
            {
                error.Invoke(result.error);
            }
        }));
    }
}
