﻿using System.Collections.Generic;
using UnityEngine;

public class ApiConfig : MonoBehaviour
{
	
	public static ApiConfig instance;

	public static readonly string API_GET_CHANNEL_DATA_URL = "https://api.spotify.com/v1/me/top/artists?limit=3";
	public static string AUTH_TOKEN; 
	//public static string AUTH_TOKEN = "IGQVJWVDR5M1lOb2hrcUJERlpid2RROG82VFlBOFJkRUdCRy13RHAyRUh0WHhJSkZA0ZAXZAtQzRSblU1X0tEcXdoNzVUTkxDckRXb0c4N1NqcmtabUdjN3JFZA0o1VTg4b2pIMWtZAcEZAHbHhsQ3FUb3BaWgZDZD";
	//Instagram
	public static string API_INSTAGRAM_GET_ACCESS_TOKEN = "https://api.instagram.com/oauth/access_token/";
	public static string API_INSTAGRAM_GET_USER_POSTS = "https://graph.instagram.com/me/media?fields=";


	//"https://api.instagram.com/oauth/authorize?app_id=342102897919786&redirect_uri=https://ashtoy.com/&scope=user_profile,user_media&response_type=code";
	public static readonly string INSTAGRAM_APP_ID = "342102897919786";
	public static readonly string INSTAGRAM_APP_SECRECT = "0ef9938804fd6ad9d7b41eb73d40377a";
	public static readonly string INSTAGRAM_REDIRECT_URL = "https://ashtoy.com/";//"https://ashtoy.com/";


	//Test
	//public static string authCode = "AQAN9uHPbO8jmKeTPmtgciDGc7qOHjKbgUC2WtS454tFLktKNDhw6VtvR2-4AaP18iZa6rmACcRahn4t8f27dClIBL0ku6gZhsSoljC2mbxwTvIlRF-MGFJHfjGROUAh6ZCJ450jJ5k0KM31D3PU6sFPredyDcee6vMQqDSlTeTNArBB1t8_w_AvaviLjO_4AU9mFeSnclPJZNBd10xrf69SwGh5lUpHBikbsPflM7jZJg";
	//public static string INSTAGRAM_ACCESS_TOKEN = "IGQVJWZAzZAkTlYxMXRhZA2o3aFBjcW8wLVBLZAVhIbU5LV2FEOWtYQ3QzM0VfVGNPWHF3MXVsNjY5ZAGY3QzRObmpZATzdBSGRmcjZAoS1M4UWEzQmFpYmp6WF9rSnVheU90NFRNdVRTUm1oLWRyUzNVNFhTYQZDZD";


	public static string GetInstagramAuthURL()
	{
		string url = "https://api.instagram.com/oauth/authorize?app_id=" + INSTAGRAM_APP_ID + "&redirect_uri=" +
							INSTAGRAM_REDIRECT_URL + "&scope=user_profile,user_media&response_type=code";

		return url;
	}

	public static string GetAuthToken()
    {
		//AUTH_TOKEN = "BQCbeEGV3Sq6SHDFByPdbO9d_0Lmi14djRCML8Au4w31pFT-FIxMmKvz5Z8uWk_Sy3jbO0FCaGo3vdZKxDM9JxOPdVdbpzsVwTXbcrZK11EA5usM-ab5g5XrcbhqodRV3YF_-8BoPTi9CgyxOzI2cWmSFyfLhUO0eoYasMxSUsf9nz1iZzyreLBBIWTCE2eyyGRMRFO8xtVTHlngqoTckdppYzBwcq2a3Y54vgtaOcaHO6mpcKGzWji7QBwHhvihmsU5nOT-GfdUcdudgyg";
	    return "Bearer " + AUTH_TOKEN;
	}

    public static Dictionary<string, string> GetHeaders()
	{
		Dictionary<string, string> headers = new Dictionary<string, string>();

		headers.Add("Authorization",GetAuthToken());  // Muse Api Key Production
		Debug.Log("UNITY>> Get Auth Token " + GetAuthToken());
		return headers;
	}

}
