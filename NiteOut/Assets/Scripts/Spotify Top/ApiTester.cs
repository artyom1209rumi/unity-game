using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using API;

public class ApiTester : MonoBehaviour
{

    ISpotifyApiDataRepository _apiRepository;

    void Start()
    {
        _apiRepository = FindObjectOfType<SpotifyApiDataRepository>();
        GetTopData();
    }

    public void GetTopData()
	{
		Debug.Log("UNITY>> Fetct Data .....................................................");
		
		GetSpotifyUserTopDataRequest videoListReq = new GetSpotifyUserTopDataRequest();
		_apiRepository.GetTopData(videoListReq, (GetSpotifyUserTopArtistResult result) =>
		{
			if (result.items != null && result.items.Count > 0)
			{
				for (int i = 0; i < result.items.Count; i++)
				{
					string title = result.items[i].name;
					string posterUrl = result.items[i].images[0].url;

					SpotifyUserTopData data = new SpotifyUserTopData();
					data.title = title;
					data.posterUrl = posterUrl;
					//dataList.Add(data);

					Debug.Log("UNITY>> title " + title + " url  " + posterUrl);
				}

				//LoadList(dataList);
			}

			Debug.Log("UNITY>> total " + result.total +  " item count  " + result.items.Count);

		}, (string error) =>
		{
			
			Debug.Log("UNITY>> API TEST error is " + error);
		});


	}
}
