using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SpotifyTopItem : MonoBehaviour
{

    [SerializeField] private Text titleLbl;
    [SerializeField] private RawImage image;

    SpotifyUserTopData _data;

    public void Setup(SpotifyUserTopData data)
    {
        _data = data;
        SetUI();
    }

    private void SetUI()
    {
        titleLbl.text = _data.title;
       // Debug.Log("UNITY>> title " + _data.title + "  Url  " + _data.posterUrl);
        StartCoroutine(DownloadImage(_data.posterUrl));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
            image.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
    }


}
