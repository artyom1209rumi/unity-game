using System.Collections;
using System.Collections.Generic;
using API;
using UnityEngine;

public class SpotifyUserTopdDataPanel : MonoBehaviour
{
    [SerializeField] private GameObject itemPrefab;
    [SerializeField] private GameObject parent;
	[SerializeField] private List<GameObject> ui_during_disconnect;
	[SerializeField] private List<GameObject> ui_during_connection;

	private List<SpotifyTopItem> _itemList = new List<SpotifyTopItem>();

	private ISpotifyApiDataRepository _apiRepository;

	void Start()
	{
		//LoadUserTopData();
	}


	public void LoadUserTopData()
    {
		if(ApiConfig.AUTH_TOKEN == null){
			Debug.Log("Unity>> Auth token Null");
			SetUiGraphics(false);
			return;
        }
		Debug.Log("Unity>> Auth token Not Null");
		SetUiGraphics(false);

		//Debug.Log("UNITY>> Auth Token Null.......");
		if(_apiRepository == null)
			_apiRepository = FindObjectOfType<SpotifyApiDataRepository>();

		GetTopData();
	}

	public void OnConnectButtonPressed()
    {
		Debug.Log("Unity>> OnConnect Button Pressed ... ");
		MusicManager.instance.AuthorizeSpotify(OnAuthProcessCompleted);
		
    }

	public void OnAuthProcessCompleted()
	{
		Debug.Log("Unity>> Spotify Auth Completed -- do not kow the result .. ");

		Invoke("LoadUserTopData", 1.0f);
	}

	private void GetTopData()
	{
		//Debug.Log("UNITY>> Fetct Data .....................................................");

		GetSpotifyUserTopDataRequest videoListReq = new GetSpotifyUserTopDataRequest();
		_apiRepository.GetTopData(videoListReq, (GetSpotifyUserTopArtistResult result) =>
		{
			//Debug.Log("UNITY>> total " + result.total + " item count  " + result.items.Count);
			SetUiGraphics(true);
			List<SpotifyUserTopData> dataList = new List<SpotifyUserTopData>();
			if(result.items != null && result.items.Count > 0)
            {
				for(int i = 0; i < result.items.Count; i++)
                {
					
					string title = result.items[i].name;
					string posterUrl = result.items[i].images[0].url;

					SpotifyUserTopData data = new SpotifyUserTopData();
					data.title = title;
					data.posterUrl = posterUrl;
					dataList.Add(data);
				}

				LoadList(dataList);
            }
            else
            {
				SetUiGraphics(false);
            }


		}, (string error) =>
		{

			//Debug.Log("UNITY>> API TEST error is " + error);
		});
	}

	private void LoadList(List<SpotifyUserTopData> dataList)
    {
		DeleteALl();

		if (dataList == null || dataList.Count <= 0)
			return;


		 for(int i = 0; i < dataList.Count; i++)
        {
			GameObject obj = Instantiate(itemPrefab, parent.transform);
			obj.transform.parent = parent.transform;
			SpotifyTopItem itemScript = obj.GetComponent<SpotifyTopItem>();
			itemScript.Setup(dataList[i]);
		   _itemList.Add(itemScript);
		}

		
    }


	private void DeleteALl()
    {
		for(int i = 0; i < _itemList.Count; i++)
        {
			if (_itemList[i].gameObject != null)
				Destroy(_itemList[i].gameObject);
        }
    }

	private void SetUiGraphics(bool isConnected)
    {

        if (isConnected)
        {
			for (int i = 0; i < ui_during_connection.Count; i++)
			{
				ui_during_connection[i].SetActive(true);
			}
			for (int i = 0; i < ui_during_disconnect.Count; i++)
			{
				ui_during_disconnect[i].SetActive(false);
			}
        }
        else
        {
			for (int i = 0; i < ui_during_connection.Count; i++)
			{
				ui_during_connection[i].SetActive(false);
			}
			for (int i = 0; i < ui_during_disconnect.Count; i++)
			{
				ui_during_disconnect[i].SetActive(true);
			}
		}
    }

}


public class SpotifyUserTopData
{
	public string title;
	public string posterUrl;
}
