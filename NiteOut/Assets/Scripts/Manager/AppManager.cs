using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppManager instance;

    [SerializeField] private InstagramDataController instagramController;
    [SerializeField] private SpotifyUserTopdDataPanel spotifyController;


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public InstagramDataController GetInstagramController()
    {
        return instagramController;
    }

    public SpotifyUserTopdDataPanel GetSpotifyController()
    {
        return spotifyController;
    }

}
