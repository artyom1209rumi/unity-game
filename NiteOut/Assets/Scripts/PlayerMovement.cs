using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    PlayerManager pm;

    public CharacterController controller;
    public float speed = 3f;
    public float gravity = -9.81f;
    public float jumpHeight = 1f;

    public Transform groundCheck;
    public float groundDistance = 0.02f;
    public LayerMask groundMask;

    [HideInInspector]
    public Vector2 runAxis;

    [HideInInspector]
    public bool jumpAxis;

    private Vector3 velocity;
    private bool isGrounded;

    private void Start()
    {
        pm = GetComponent<PlayerManager>();
        if (!pm.isLocalPlayer)
        {
            Debug.Log("Not local player, disable movement component.");
            this.enabled = false;
        }
    }


    // Update is called once per frame
    private void Update()
    {

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        UpdateControls();
        float x = runAxis.x + Input.GetAxis("Horizontal");
        float z = runAxis.y + Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        if (jumpAxis && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
        if (Input.GetKey(KeyCode.Z))
        {
            RotatePlayer(-1f);
        }
        if (Input.GetKey(KeyCode.X))
        {
            RotatePlayer(1f);
        }
        //Forward/Backwards
        //Debug.Log(z.ToString() + " : "+x.ToString());
        pm.animator.SetFloat("movX", z * speed);
        pm.animator.SetFloat("movZ", x * speed);
        //pm.animator.int
        /*
        if (transform.position.y < 0f)
        {
            Debug.Log("Pos y -1f Dropped from bar? Fixing.");
            velocity = Vector3.down;
            transform.position = new Vector3(transform.position.x, 1.4f, transform.position.z);
        }*/
    }

    private void UpdateControls()
    {
        //throw new NotImplementedException();
        runAxis.y = TempVirtualUI.Instance.Joystick.Vertical;
        RotatePlayer(TempVirtualUI.Instance.Joystick.Horizontal);
    }

    public void RotatePlayer(float amount)
    {
        transform.Rotate(new Vector3(0, amount*Time.deltaTime*80, 0));
    }
}
