using System;
using System.Collections.Generic;
using UnityEngine;

public class BadgeManager : MonoBehaviour
{
    public static BadgeManager Instance { get; private set; }
    [SerializeField]
    private Badge[] badges;
    public ProfileUI profileUI;

    private void Awake()
    {
        Instance = this;
        //SaveBadge(0);
    }

    public void SaveBadge(int id)
    {
        Badge badge = FindBadgeByID(id);
        if (badge == null)
        {
            print("Couldn't locate badge");
            return;
        }
        string badgesString = PlayerPrefs.GetString("Badges", "");
        if (HasBadgeAlready(badgesString, id))
        {
            print("Badge " + id + " has been already obtained");
            return;
        }    
        print("Saving badge : " + badge.Title);
        badgesString += id + "\n";
        PlayerPrefs.SetString("Badges", badgesString);
    }

    public void DeleteBadges()
    {
        PlayerPrefs.DeleteKey("Badges");
    }

    public void DeleteBadge(int id)
    {
        string badgesString = PlayerPrefs.GetString("Badges", "");
        if (string.IsNullOrEmpty(badgesString))
            return;
        List<string> badgesStringList = new List<string>(badgesString.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries));
        badgesString = "";
        for (int i = 0; i < badgesStringList.Count; ++i)
        {
            if (badgesStringList[i] == id.ToString())
                badgesStringList.RemoveAt(i);
            badgesString += id + "\n";
        }
        PlayerPrefs.SetString("Badges", badgesString);
    }

    private bool HasBadgeAlready(string badgesString, int id)
    {
        string[] badgesStringArray = badgesString.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < badgesStringArray.Length; ++i)
        {
            if (badgesStringArray[i] == id.ToString())
                return true;
        }
        return false;
    }
    // this should load from playerprefs
    public Badge[] LoadBadges()
    {
        string badgesString = PlayerPrefs.GetString("Badges", "");
        if (badgesString == "")
            return new Badge[0];
        string[] badgesStringArray = badgesString.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        Badge[] badges = new Badge[badgesStringArray.Length];
        for (int i = 0; i < badges.Length; ++i)
            badges[i] = FindBadgeByID(int.Parse(badgesStringArray[i]));
        return badges;
    }

    private Badge FindBadgeByID(int id)
    {
        foreach (Badge badge in badges)
        {
            if (badge.ID == id)
                return badge;
        }
        return null;
    }
}
