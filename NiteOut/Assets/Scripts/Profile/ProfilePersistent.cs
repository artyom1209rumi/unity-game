using System;
using System.Text;
using UnityEngine;

public static class ProfilePersistent
{
    public static void SaveProfileData(ProfileData profileData)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(profileData.FirstName);
        sb.AppendLine(profileData.LastName);
        sb.AppendLine(profileData.Description);
        /*
        if (string.IsNullOrEmpty(profileData.PhotoRemotePath))
        {
            sb.AppendLine("NULL_PATH");
        }
        else
        {
            sb.AppendLine(profileData.PhotoRemotePath);
        }
        */
        PlayerPrefs.SetString("ProfileData", sb.ToString());
    }

    public static ProfileData LoadProfileData()
    {
        string profileDataString = PlayerPrefs.GetString("ProfileData", "");
        if (profileDataString == "")
            return null;
        string[] profileDataStringArray = profileDataString.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        ProfileData profileData = new ProfileData(profileDataStringArray[0],
                                                      profileDataStringArray[1],
                                                      profileDataStringArray[2]);
        return profileData;
    }
}