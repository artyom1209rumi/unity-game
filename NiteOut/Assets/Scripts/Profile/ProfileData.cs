using Newtonsoft.Json;
using NiteOut.Model;
using System.IO;
using UnityEngine;

public class ProfileData
{
    [JsonIgnore]
    public string UserID { get; private set; }
    public string FirstName { get; private set; }
    public string LastName { get; private set; }
    public string Description { get; private set; }
    [JsonIgnore]
    public string PhotoRemotePath { get { return "Users/" + UserID + "/ProfilePhoto.jpg"; } }

    [JsonConstructor]
    public ProfileData(string firstName, string lastName, string description)
    {
        UserID = DatabaseHelper.User.UserID;
        FirstName = firstName;
        LastName = lastName;
        Description = description;
    }

    public void SetUserID(string userID)
    {
        UserID = userID;
    }
}