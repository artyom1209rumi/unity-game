using UnityEngine;
using TMPro;
using UnityEngine.UI;
using NiteOut.View;
using NiteOut.Model;
using System.Collections;

public class ProfileUI : MonoBehaviour
{
    //How was this meant to be called? Had to create this temporary instance system that uses Find...
    public static ProfileUI Instance
    {
        get 
        { 
            if (instance == null)
                instance = FindObjectOfType<ProfileUI>();                    
            return instance;
        }
    }

    public bool ProfileOpen;

    private static ProfileUI instance;
    //[SerializeField]
    //private Animator profilePanelAnimator;
    [SerializeField]
    private ChatUI chatUI;
    [SerializeField]
    private CompleteProfileUI completeProfileUI;
    [SerializeField]
    private AvatarSelectionUI avatarSelectionUI;
    [SerializeField]
    private GameObject eventCanvas;
    [SerializeField]
    private GameObject barCanvas;
    [SerializeField]
    private Transform badge;
    [SerializeField]
    private Transform badgesContent;
    [SerializeField]
    private GameObject ownerButtons;
    [SerializeField]
    private GameObject ownerMorePanel;
    [SerializeField]
    private GameObject signOutPanel;
    [SerializeField]
    private GameObject friendsButtons;
    [SerializeField]
    private TextMeshProUGUI profileNameText;
    [SerializeField]
    private TextMeshProUGUI profileDescriptionText;
    [SerializeField]
    private RawImage profileRawImage;
    //[SerializeField]
    //private SpotifyUserTopdDataPanel spotifyTopDataPanel;
    //[SerializeField]
    //private InstagramDataController instagramDataController;
    private ProfileData profileData;
    private string userID;
    private bool isComingFromCompleteProfileOrAvatarSelection;

    // swipe
    private Vector2 firstTouchPosition;
    private Vector2 secondTouchPosition;
    private Vector2 currentSwipe;

    private void OnEnable()
    {
       AppManager.instance.GetSpotifyController().LoadUserTopData();
       // spotifyTop.gameObject.SetActive(MusicManager.instance.spotifyService.IsConnected); //Sync later.
    }
    //private void OnEnable()
    //{
    //    spotifyTopDataPanel.LoadUserTopData();
    //}

    private void Start()
    {
        instance = this;
        //profilePanelAnimator.keepAnimatorControllerStateOnDisable = true;
        gameObject.SetActive(false);
        LoadBadges();
    }

    public void OnSwipeDown()
    {
        if (gameObject.activeSelf)
            OnCloseButtonClicked();
    }

    private void DisplayProfileData(ProfileData profileData)
    {
        if (profileData == null)
        {
            Debug.LogWarning("Could not get profile data from firebase database");
            return;
        }
        profileNameText.text = profileData.FirstName + " " + profileData.LastName;
        profileDescriptionText.text = profileData.Description;
        if (!string.IsNullOrEmpty(profileData.PhotoRemotePath))
        {
            DatabaseHelper.Storage.DownloadFile(profileData.UserID, profileData.PhotoRemotePath, (imageBytes) =>
            {
                profileRawImage.texture = TextureLoader.LoadFromBytes(imageBytes);
                profileRawImage.SizeToParent();
            });
        }
        //Disabled due using local paths that just wont work for multiplayer.
        //profileRawImage.texture = profileData.Photo;
    }

    private void LoadBadges()
    {
        Badge[] badges = BadgeManager.Instance.LoadBadges();
        foreach(Badge badge in badges)
        {
            Transform badgeInstance = Instantiate(this.badge, badgesContent, false);
            badgeInstance.Find("BadgeTitle").GetComponent<TextMeshProUGUI>().text = badge.Title;
            badgeInstance.Find("BadgeMask").Find("BadgeImage").GetComponent<Image>().sprite = badge.Sprite;
        }
    }

    public void ShowProfile(string UUID, ProfileData profileData = null)
    {
	    profileNameText.text = "";
        profileDescriptionText.text = "";
        profileRawImage.texture = null;
        ProfileOpen = true;
        bool isOwner = UUID == Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        if (isOwner)
        {
            //profileData = ProfilePersistent.LoadProfileData();
            ownerButtons.SetActive(true);
            friendsButtons.SetActive(false);
        }
        else
        {
            friendsButtons.SetActive(true);
            ownerButtons.SetActive(false);
        }
        barCanvas.SetActive(false);
        eventCanvas.SetActive(false);
        gameObject.SetActive(true);
        if (isComingFromCompleteProfileOrAvatarSelection)
        {
            this.profileData = profileData;
            DisplayProfileData(profileData);
            isComingFromCompleteProfileOrAvatarSelection = false;
        }
        else
        {
            //profilePanelAnimator.SetBool("IsSlidingIn", true);
            DatabaseHelper.Database.GetProfileData(UUID, (data) =>
            {
                this.profileData = data;
                this.userID = UUID;
                DisplayProfileData(this.profileData);
            });
            //StopAllCoroutines();
            //StartCoroutine(ShowProfileCoroutine(UUID, profileData));
        }

        AppManager.instance.GetSpotifyController()?.LoadUserTopData();
        AppManager.instance.GetInstagramController()?.FetchData();
    }

    public void HideProfile()
    {
        ProfileOpen = false;
        StopAllCoroutines();
        //StartCoroutine(HideProfileCoroutine());
        HideProfileCoroutine();
        Debug.Log("Close panel ");
    }

    /*
    private IEnumerator ShowProfileCoroutine(string UUID, ProfileData profileData)
    {
        if (isComingFromCompleteProfileOrAvatarSelection)
        {
            this.profileData = profileData;
            DisplayProfileData(profileData);
            isComingFromCompleteProfileOrAvatarSelection = false;
        }
        else
        {
            profilePanelAnimator.SetBool("IsSlidingIn", true);
            if (DatabaseHelper.User.UserID == UUID && this.profileData != null)
                DisplayProfileData(this.profileData);
            else
            {
                DatabaseHelper.Database.GetProfileData(UUID, (data) =>
                {
                    this.profileData = data;
                    this.userID = UUID;
                    DisplayProfileData(this.profileData);
                });
            }
            yield return new WaitForSeconds(0);
            //yield return new WaitForSeconds(.167f);
        }
    }
    */

    private void HideProfileCoroutine()
    {
        //if (!isComingFromCompleteProfileOrAvatarSelection)
        //{
        //   // profilePanelAnimator.SetBool("IsSlidingIn", false);
        //    yield return new WaitForSeconds(.167f);
        //}
        if (!eventCanvas.activeSelf)
        {
            barCanvas.SetActive(true);
            eventCanvas.SetActive(false);
        }
        gameObject.SetActive(false);
    }

    public void OnCloseButtonClicked()
    {
        HideProfile();    
    }

    public void OnEditProfileButtonClicked()
    {
        eventCanvas.SetActive(true);
        completeProfileUI.UpdateProfileData(profileData, true);
        isComingFromCompleteProfileOrAvatarSelection = true;
        HideProfile();
    }

    public void OnSelectAvatarButtonClicked()
    {
        // update the currently selected avatar
        eventCanvas.SetActive(true);
        avatarSelectionUI.UpdateAvatarSelection();
        isComingFromCompleteProfileOrAvatarSelection = true;
        HideProfile();
    }

    public void OnMessageButtonClicked()
    {
        chatUI.OpenConversation(userID, profileData);
        barCanvas.SetActive(false);
        gameObject.SetActive(false);
    }

    public void OnMoreButtonClicked(bool isOwner)
    {
        if (isOwner)
        {
            // do something
            ownerMorePanel.SetActive(!ownerMorePanel.activeSelf);
        }
        else
        {
            // do something else
        }
    }

    public void OnSignOutMenuButtonClicked()
    {
        signOutPanel.SetActive(true);
        ownerMorePanel.SetActive(false);
    }

    public void OnGoBackButtonClicked()
    {
        signOutPanel.SetActive(false);
    }

    public void OnSignOutButtonClicked()
    {
        AuthenticationManager.SignoutStatic();
    }
}
