using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using NiteOut.Model;

public class PlayerManager : NetworkBehaviour, IPointerClickHandler
{
    public static PlayerManager LocalPlayer;

    [SyncVar(hook = nameof(SetUUID))][SerializeField] //UUID
    string uniqueUserIdentification;

    private void SetUUID(string oldUUID, string newUUID)
    {
        DatabaseHelper.Database.GetProfileData(newUUID, (profileData) =>
        {
            if(profileData != null)
                UpdateText("", profileData.FirstName);

        });
    }

    public string UUID
    {
        get { return uniqueUserIdentification; }
        set {
            if (isServer)
            {
                Debug.Log("SERVER: User updates UUID: " + value);
                uniqueUserIdentification = value;
            }
            else
            {
                CmdUpdateUUID(value);
            }
        }
    }

    #region Profile backend
    public string firstName
    {
        get {
                if (string.IsNullOrEmpty(FirstName))
                    Debug.LogError("No user details loaded");
                return FirstName;
            }
    }
    public string lastName
    {
        get
        {
            if (string.IsNullOrEmpty(LastName))
                Debug.LogError("No user details loaded");
            return LastName;
        }
    }

    public string description
    {
        get
        {
            if (string.IsNullOrEmpty(Description))
                Debug.LogError("No user details loaded");
            return Description;
        }
    }
    public string profileImageURL
    {
        get
        {
            if (string.IsNullOrEmpty(ProfileImageURL))
                Debug.LogError("No user details loaded");
            return ProfileImageURL;
        }
    }

    [SyncVar][SerializeField]
    private string FirstName;
    [SyncVar][SerializeField]
    private string LastName;
    [SyncVar][SerializeField]
    private string Description;
    [SyncVar][SerializeField]
    private string ProfileImageURL;

    [SerializeField]
    private PlayerText playerText;
    #endregion

    public Animator animator;
    public GameObject[] avatars;
    [SyncVar] public string avatarID = "";


    [Command]
    void CmdUpdateUUID(string newUUID)
    {
        //TODO Security problem. Change to token authentication.
        UUID = newUUID;
    }

    private void UpdateText(string oldText, string newText)
    {
        if (isLocalPlayer)
            playerText.gameObject.SetActive(false);
        else
            playerText.text.text = newText;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<NetworkAnimator>().animator;
        if (isLocalPlayer)
        {
            LocalPlayer = this;

            //Set player camera
            PlayerCamera.instance.SetPlayerCamera(transform);

            //Update UUID.
            if (Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser != null)
                UUID = Firebase.Auth.FirebaseAuth.DefaultInstance.CurrentUser.UserId;

            //Load player 3D model.
            if (PlayerPrefs.HasKey("MVP_Character"))
            {
                //Load character //Int
                CmdLoadCharacter(PlayerPrefs.GetInt("MVP_Character").ToString());
            }
            else
            {
                //Load random character?
                CmdLoadCharacter(Random.Range(0, avatars.Length).ToString());
            }
            //Sync model to all clients.

            //Upload profile data.
            UploadProfileData();
            Globals.AddProfile(UUID, new ProfileData(firstName, lastName, description));
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        //Debug.Log("OnStartClient: AvatarID: " + avatarID);
        LoadCharacterByID(avatarID);
    }

    private void UploadProfileData()
    {
        ProfileData pd = ProfilePersistent.LoadProfileData();
        if (pd != null)
        {
            CmdUpdateProfileData(pd.FirstName, pd.LastName, pd.Description, pd.PhotoRemotePath);
        }
    }

    [Command]
    void CmdUpdateProfileData(string first, string last, string desc, string pfpURL)
    {
        FirstName = first;
        LastName = last;
        gameObject.name = first + " " + last;
        Description = desc;
        ProfileImageURL = pfpURL;
    }

    public void UpdateAvatarInsideBar(int id)
    {
        CmdLoadCharacter(id.ToString());
    }

    [Command]
    void CmdLoadCharacter(string id)
    {
        avatarID = id;
        RpcLoadCharacter(id);
    }

    [ClientRpc]
    void RpcLoadCharacter(string id)
    {
        LoadCharacterByID(id);
    }

    private void LoadCharacterByID(string id)
    {
        Debug.Log("Loading character by id: " + id);
        if (string.IsNullOrWhiteSpace(id))
            return;

        //Delete old char if it exists.
        if (GetComponent<NetworkAnimator>().animator.transform.childCount > 0)
            Destroy(GetComponent<NetworkAnimator>().animator.transform.GetChild(0).gameObject);

        var go = Instantiate(avatars[int.Parse(id)], GetComponent<NetworkAnimator>().animator.transform);
        //Disable animator incase its left on.
        if (go.GetComponent<Animator>())
            go.GetComponent<Animator>().enabled = false;

        go.GetComponentInParent<Animator>().enabled = false;

        //To activate animator, we need to re-enable player model. Dont ask why, weird unity stuff.
        //Else it doesnt recognize object to be animated.
        animator.gameObject.SetActive(false);
        Invoke("EnableAnimator", 0.1f);


        //MeshBakerController.instance.BakeModel(GetComponentsInChildren<MeshRenderer>());
    }

    void EnableAnimator()
    {
        Debug.Log("Enabling animator");
        animator.enabled = true;
        animator.gameObject.SetActive(true);

        if (LocalPlayer == this)
            ProfileModelStudio.Instance.UpdateModel(LocalPlayer.animator.gameObject);
    }

    public void PlayDance(string danceName)
    {
        CmdPlayDance(danceName);
    }

    [Command]
    void CmdPlayDance(string danceName)
    {
        RpcPlayDance(danceName);
    }

    [ClientRpc]
    void RpcPlayDance(string danceName)
    {
        animator.SetTrigger(danceName);

    }


    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Player clicked " + gameObject.name);
        //Open profile.        
        ProfileData tempProfile = new ProfileData(firstName, lastName, description);
        tempProfile.SetUserID(UUID);
        Debug.Log(ProfileUI.Instance.name);
        Debug.Log(UUID);
        Debug.Log(tempProfile.ToString());
        //if profile already open, do not open it.
        if (ProfileUI.Instance.ProfileOpen)
        {
            return;
        }
        ProfileUI.Instance.ShowProfile(UUID);
        //ProfileUI.Instance.ShowProfile(UUID, tempProfile);
        ProfileModelStudio.Instance.UpdateModel(GetComponent<NetworkAnimator>().animator.gameObject);
    }
}
