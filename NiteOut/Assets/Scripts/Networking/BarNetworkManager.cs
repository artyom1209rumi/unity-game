﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BarNetworkManager : NetworkManager
{
    public static BarNetworkManager instance;
    [Header("Settings")]
    public bool ForceHost;
    public bool SelfHost;


    public override void Awake()
    {
        base.Awake();
        instance = this;
        /*
#if UNITY_ANDROID
        if(!ForceHost)
            StartClient();
#endif*/

    }

    public override void Start()
    {
        base.Start();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        /*
#if UNITY_EDITOR
        if (ForceHost)
        {
            StartHost();
            return;
        }
#endif*/
    }

    public void ConnectToServer()
    {
        if (SelfHost)
        {
            StartHost();
        }
        else
        {
            StartClient();

        }
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);

    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftControl)  && Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
            Firebase.Auth.FirebaseAuth.DefaultInstance.SignOut();
            Debug.Log("All PlayerPrefs deleted and signed out");
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        Debug.Log("Unwanted Disconnect, reconnecting.");
        
        StartClient();

    }

    

}
