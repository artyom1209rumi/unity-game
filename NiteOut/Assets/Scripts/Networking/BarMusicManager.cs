﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BarMusicManager : NetworkBehaviour
{
    public static BarMusicManager Manager;
    public string currentSongSpotifyURL;
    public float CurrentSongTimeSpotify
    {
        get { return Time.time - currentSongStartTime; }
    }
    private float currentSongStartTime;

    public string DebugSongURL = "https://open.spotify.com/track/0s8A5JK6GPggOi7nLNmEIA";
    
    [SyncVar]public string currentSongURL;

    private void Awake()
    {
        Manager = this;       

    }


    [Server]
    public void Admin_PlaySong(string songURL)
    {
        //TODO Check if admin/bar owner.
        Debug.Log("Server received command to play: " + songURL);
        RpcPlaySong(songURL);
    }

    [Server]
    public void Admin_PlayPlaylist(string URL)
    {
        //TODO Check if admin/bar owner.
        Debug.Log("Server received command to play paylist: " + URL);
        RpcPlayPlaylist(URL);
    }
    [Server]
    public void Admin_PlayAlbum(string URL)
    {
        //TODO Check if admin/bar owner.
        Debug.Log("Server received command to play album: " + URL);
        RpcPlayPlaylist(URL);
    }

    [ClientRpc]
    public void RpcPlayPlaylist(string songURL)
    {
        Debug.Log("RPC Play Playlist: " + songURL);
        //MusicPlayer.spotifyPlayer.PlayAlbum(songURL);
    }

    [ClientRpc]
    public void RpcPlaySong(string songURL)
    {
        Debug.Log("RPC Play Song: " + songURL);
        //MusicPlayer.spotifyPlayer.PlaySong(songURL);
    }

}
