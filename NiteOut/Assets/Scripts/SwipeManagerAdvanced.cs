using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SwipeManagerAdvanced : MonoBehaviour
{
    [SerializeField]
    private FixedJoystick joystick;
    public float swipeThreshold = 50f;

    public UnityEvent OnSwipeLeft;
    public UnityEvent OnSwipeRight;
    public UnityEvent OnSwipeUp;
    public UnityEvent OnSwipeDown;

    private Vector2 fingerDown;
    private Vector2 fingerUp;

    private void Update()
    {
        /*
        if (IsPointerOverGameObject())
        {
            this.fingerDown = fingerUp;
            return;
        }
        */
        #if UNITY_STANDALONE || UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            fingerDown = Input.mousePosition;
            fingerUp = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (EventSystem.current.IsPointerOverGameObject(-1) || (joystick != null && joystick.IsJoystickMoving()))
                fingerDown = fingerUp;
            else
            {
                fingerDown = Input.mousePosition;
                CheckSwipe();
            }
        }
        #endif
        #if UNITY_ANDROID || UNITY_IOS
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerDown = touch.position;
                fingerUp = touch.position;
            }
            if (touch.phase == TouchPhase.Ended)
            {
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId) || (joystick != null && joystick.IsJoystickMoving())) // our finger is over UI
                    fingerDown = fingerUp;
                else
                {
                    fingerDown = touch.position;
                    CheckSwipe();
                }
            }
        }
        #endif
    }

    private void CheckSwipe()
    {
        float deltaX = fingerDown.x - fingerUp.x;
        float deltaY = fingerDown.y - fingerUp.y;
        /*
        if (fingerUp.y < Screen.height * .5f)
            return;
        */
        if (Mathf.Abs(deltaX) > swipeThreshold && Math.Abs(deltaX) > Mathf.Abs(deltaY))
        {
            if (deltaX > 0)
            {
                OnSwipeRight.Invoke();
                Debug.Log("right");
            }
            else if (deltaX < 0)
            {
                OnSwipeLeft.Invoke();
                Debug.Log("left");
            }
        }
        else if (Mathf.Abs(deltaY) > swipeThreshold && Mathf.Abs(deltaY) > Mathf.Abs(deltaX))
        {
            if (deltaY > 0)
            {
                OnSwipeUp.Invoke();
                Debug.Log("up");
            }
            else if (deltaY < 0)
            {
                OnSwipeDown.Invoke();
                Debug.Log("down");
            }
        }
        fingerUp = fingerDown;
    }

    public static bool IsPointerOverGameObject()
    {
        #if UNITY_STANDALONE || UNITY_EDITOR
                // Check mouse
                if (EventSystem.current.IsPointerOverGameObject(-1))
                    return true;
#endif
#if UNITY_ANDROID || UNITY_IOS
                // Check touches
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Touch touch = Input.GetTouch(i);
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                            return true;
                    }
                }
                return false;
#endif
        return false;
    }
}