﻿using AdvancedInputFieldPlugin;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TestLayout : MonoBehaviour
{
    public static TestLayout Instance { get; private set; }

    [SerializeField]
    private KeyboardScroller keyboardScroller;
    [SerializeField]
    private AdvancedInputField chatInputField;
    [SerializeField]
    private float defaultHeight = 100;
    [SerializeField]
    private float maxHeight = 300;
    [SerializeField]
    private LayoutElement inputFieldPanelLayoutElement;
    [SerializeField]
    private RectTransform inputFieldContent;
    [SerializeField]
    private RectTransform inputFieldBackground;
    [SerializeField]
    private VerticalLayoutGroup verticalContentLayoutGroup;
    private float inputFieldOffset;
    private const float INPUTFIELDSTARTSIZE = 62.5625f;
    public bool IsKeyboardOpen { get { return isKeyboardOpen; } }
    private bool isKeyboardOpen;

    private void Start()
    {
        Instance = this;
        inputFieldOffset = defaultHeight - INPUTFIELDSTARTSIZE;
        SetInputFieldPanelLayout(defaultHeight);
        //StartCoroutine(FixInputContent());
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
            StartCoroutine(SetScrollOffset());
    }

    private void SetInputFieldPanelLayout(float height)
    {
        inputFieldPanelLayoutElement.minHeight =
        inputFieldPanelLayoutElement.flexibleHeight =
        inputFieldPanelLayoutElement.preferredHeight = Mathf.Max(height, defaultHeight);
    }

    public void OnValueChanged(string text)
    {
        SetInputFieldPanelLayout(Mathf.Min(inputFieldContent.rect.height + inputFieldOffset, maxHeight));
        /*
        RectOffset currentPadding = new RectOffset()
        {
            left = verticalContentLayoutGroup.padding.left,
            right = verticalContentLayoutGroup.padding.right,
            top = verticalContentLayoutGroup.padding.top,
            bottom = verticalContentLayoutGroup.padding.bottom
        };
        currentPadding.top = Mathf.Min(25 + (int)(inputFieldContent.rect.height + inputFieldOffset - defaultHeight), 25 + (int)maxHeight);
        verticalContentLayoutGroup.padding = currentPadding;
        */
        StartCoroutine(SetScrollOffset());
        //SetInputFieldPanelLayout(Mathf.Min(Mathf.Max(defaultHeight, inputFieldContent.rect.height), maxHeight));
    }

    public void OnBeginEdit(BeginEditReason reason)
    {
        chatInputField.ShouldBlockDeselect = true;
        isKeyboardOpen = true;
        StopAllCoroutines();
        StartCoroutine(SetScrollOffset());
    }

    public void OnEndEdit(string text, EndEditReason reason)
    {
        if (reason != EndEditReason.USER_DESELECT)
        {
            chatInputField.ShouldBlockDeselect = false;
            isKeyboardOpen = false;
            ScrollBackChat();
        }
    }

    private IEnumerator SetScrollOffset()
    {
        if (!isKeyboardOpen) // if we are closing the kyeboard then wait untill it is invisible
        {
            while (NativeKeyboardManager.Keyboard.State == KeyboardState.VISIBLE)
                yield return null;
        }
        else
        {
            while (NativeKeyboardManager.Keyboard.State != KeyboardState.VISIBLE)
                yield return null;
        }
        int keyboardHeight = KeyboardHelper.GetKeyboardHeight(true);
        int keyboardOffset = isKeyboardOpen ? keyboardHeight + 31 : 31;
        //float lastNormalizedVerticalPosition = verticalContentLayoutGroup.transform.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition;
        // for some reason when the chat input field is empty the inputfieldcontentheight stratches on its own
        int inputFieldContentHeight = string.IsNullOrEmpty(chatInputField.Text) ? 36 : (int)inputFieldContent.rect.height;
        verticalContentLayoutGroup.padding.top = Mathf.Min(keyboardOffset + 25 + (int)(inputFieldContentHeight + inputFieldOffset - defaultHeight), keyboardOffset + 25 + (int)(maxHeight - defaultHeight));
        LayoutRebuilder.ForceRebuildLayoutImmediate(verticalContentLayoutGroup.transform as RectTransform);
        //verticalContentLayoutGroup.transform.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = lastNormalizedVerticalPosition;
    }

    public void ScrollBackChat()
    {
        isKeyboardOpen = false;
#if UNITY_IOS || UNITY_Mobile || UNITY_ANDROID
        keyboardScroller.OnKeyboardHeightChanged(0);
#endif
        NativeKeyboardManager.HideKeyboard();
        StartCoroutine(SetScrollOffset());
    }

    private IEnumerator FixInputContent()
    {
        yield return new WaitForSeconds(1f);
        inputFieldContent.sizeDelta = new Vector2(inputFieldContent.sizeDelta.x, 36);
    }
}
