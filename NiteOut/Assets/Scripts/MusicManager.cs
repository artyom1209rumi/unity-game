using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SpotifyAPI.Web;
using Spotify4Unity;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    public MobileSpotifyService spotifyService
    {
        get
        {
            return service;
            /*
#if UNITY_IOS
                return iOSSpotify.GetComponent<MobileSpotifyService>();
#else
                return AndroidSpotify.GetComponent<MobileSpotifyService>();
#endif*/
        }
    }
    [SerializeField] GameObject AndroidSpotify;
    [SerializeField] GameObject iOSSpotify;
    [SerializeField] SpotifyIOSPlugin SpotifyIOSPlugin;
    [SerializeField] MobileSpotifyService service;

    private void Awake()
    {
        instance = this;
    }
    internal void AuthorizeSpotify(Action onContinue)
    {
        #if UNITY_IOS && !UNITY_EDITOR

        SpotifyIOSPlugin.Authenticate("spotify:playlist:1vXOE0Mrx7Qu7XLxvPz2dO");

        #else

        //spotifyService.AuthorizeUser();
        spotifyService.Connect();

        #endif

        //Wait
        onContinue.Invoke();
    }

    public void AuthorizeSpotify()
    {
        //spotifyService.AuthorizeUser();
        #if UNITY_IOS

        SpotifyIOSPlugin.Authenticate("spotify:playlist:1vXOE0Mrx7Qu7XLxvPz2dO");

        #else

        //spotifyService.AuthorizeUser();
        spotifyService.Connect();

        #endif
        
    }

    public void PlaySong(string url)
    {
        Debug.Log("Playing: " + url);
#if UNITY_IOS && !UNITY_EDITOR

        spotifyService.PlaySong(url); //Lets force song on both.
        SpotifyIOSPlugin.Play(url);

#else

        spotifyService.PlaySong(url);
        #endif
        /* Single Song
        List<string> songsToPlay = new List<string>();
        songsToPlay.Add(url);
        spotifyService.PlaySongs(songsToPlay);*/
    }

    /* Spotify 2.0 stuff Waiting mobile support.
    [SerializeField] SpotifyService spotifyService;
    SpotifyClient spotifyClient;



    private void Awake()
    {
        instance = this;
        spotifyService = GetComponentInChildren<SpotifyService>();
    }

    internal void AuthorizeSpotify(Action onContinue)
    {
        spotifyService.AuthorizeUser();
        
        //Wait
        onContinue.Invoke();
    }




    

    public void PlaySong(string uri)
    {
        if(spotifyClient == null)
            spotifyClient = spotifyService.GetSpotifyClient();
        
        if (spotifyClient != null)
        {
            
            PlayerResumePlaybackRequest request = new PlayerResumePlaybackRequest()
            {
                ContextUri = uri        // Play within context of just the individual track
                             
            };
            spotifyClient.Player.ResumePlayback(request);

            Debug.Log($"Spotify App | Playing song: "+ uri);
        }

    }


    public async void PlayingItemChanged(IPlayableItem newPlayingItem)
    {
        if (newPlayingItem == null)
        {
            BarUI.instance.UpdateSong("Not playing.");
        }
        else
        {
            if (newPlayingItem.Type == ItemType.Track)
            {
                if (newPlayingItem is FullTrack track)
                {
                    BarUI.instance.UpdateSong("'"+track.Name+"' - "+track.Artists[0].Name);
                    //Now playing: track.Name
                }
            }
            else if (newPlayingItem.Type == ItemType.Episode)
            {
                if (newPlayingItem is FullEpisode episode)
                {
                    BarUI.instance.UpdateSong("'"+ episode.Name+"'");
                    //Now playing episode.Name
                }
            }
        }
    }*/



}
