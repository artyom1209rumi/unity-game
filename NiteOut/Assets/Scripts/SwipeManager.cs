using NiteOut.View;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SwipeManager : MonoBehaviour
{
    [SerializeField]
    public UnityEvent OnSwipeLeft;
    [SerializeField]
    public UnityEvent OnSwipeRight;
    [SerializeField]
    public UnityEvent OnSwipeUp;
    [SerializeField]
    public UnityEvent OnSwipeDown;

    private Vector2 firstTouchPosition;
    private Vector2 secondTouchPosition;

    // Update is called once per frame
    void Update()
    {
        DetectSwiping();
    }

    public void DetectSwiping()
    {
        if (IsPointerOverGameObject())
        {
            firstTouchPosition = secondTouchPosition;
            return;
        }
#if UNITY_STANDALONE || UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
            firstTouchPosition = Input.mousePosition;
        if (Input.GetMouseButtonUp(0))
        {
            secondTouchPosition = Input.mousePosition;
            CheckSwipeDirection();
        }
#endif
#if UNITY_ANDROID || UNITY_IOS
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                firstTouchPosition = touch.position;
                secondTouchPosition = touch.position;
            }
            if (touch.phase == TouchPhase.Ended)
            {
                secondTouchPosition = touch.position;
                CheckSwipeDirection();
            }
        }
#endif
    }

    private void CheckSwipeDirection()
    {
        Vector2 currentSwipe = new Vector2(secondTouchPosition.x - firstTouchPosition.x, secondTouchPosition.y - firstTouchPosition.y);
        currentSwipe.Normalize();
        if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            OnSwipeUp.Invoke();
        else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            OnSwipeDown.Invoke();
        else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            OnSwipeLeft.Invoke();
        else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            OnSwipeRight.Invoke();
        firstTouchPosition = secondTouchPosition;
    }

    public static bool IsPointerOverGameObject()
    {
#if UNITY_STANDALONE || UNITY_EDITOR
        // Check mouse
        if (EventSystem.current.IsPointerOverGameObject())
            return true;
#endif
#if UNITY_ANDROID || UNITY_IOS
        // Check touches
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    return true;
            }
        }
        return false;
#endif
        return false;
    }

    public enum SwipeDirection
    {
        None,
        Right,
        Left,
        Up,
        Down
    }
}