﻿using Newtonsoft.Json;
using System;

namespace NiteOut.Model
{
    public class ChatMessage
    {
        [JsonIgnore]
        public string ID { get; private set; }
        public string SenderID { get; private set; }
        public string ReceiverID { get; private set; }
        public string Content { get; private set; }
        public MessageType Type { get; private set; }
        public bool HasRead { get; private set; }
        public object TimeStamp { get; private set; }
        [JsonIgnore]
        public DateTime PostDate { get { return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((long)TimeStamp / 1000d).ToLocalTime(); } }

        // this is for uploading
        public ChatMessage(string senderID, string receiverID, string content, MessageType type)
        {
            SenderID = senderID;
            ReceiverID = receiverID;
            Content = content;
            Type = type;
            TimeStamp = Firebase.Database.ServerValue.Timestamp;
        }

        [JsonConstructor]
        public ChatMessage(string senderID, string receiverID, string content, MessageType type, bool hasRead, object timeStamp)
        {
            SenderID = senderID;
            ReceiverID = receiverID;
            Content = content;
            Type = type;
            HasRead = hasRead;
            TimeStamp = timeStamp;
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public void SetContent(string content)
        {
            Content = content;
        }
    }

    public enum MessageType
    {
        Text,
        Image,
        GIF,
        Video
    }
}
