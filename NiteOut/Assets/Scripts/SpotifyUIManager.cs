using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spotify4Unity;
using Spotify4Unity.Events;
using Spotify4Unity.Dtos;
using Spotify4Unity.Enums;
using Spotify4Unity.Helpers;
using System;
using System.Linq;
using UnityEngine.UI;

public class SpotifyUIManager : SpotifyUIBase
{
    /*
    MobileSpotifyService spotifyService;
    private void Awake()
    {
        spotifyService = GetComponent<MusicManager>().spotifyService;
        spotifyService.OnConnectedEvent += SpotifyService_OnConnectedEvent;
    }

    private void SpotifyService_OnConnectedEvent()
    {
        //throw new NotImplementedException();
    }*/
    public string NiteoutPlaylist = "https://open.spotify.com/playlist/1vXOE0Mrx7Qu7XLxvPz2dO?si=fb46c8a425c747a0";
    protected override void Awake()
    {
    }

    private void Start()
    {
        BarUI.instance.UpdateSong(null);
        SpotifyService = GetComponent<MusicManager>().spotifyService;
        base.Awake();
    }

    protected override void OnTrackChanged(TrackChanged e)
    {
        Debug.Log("OnTrackChanged();");
        BarUI.instance.UpdateSong(e.NewTrack.Title + " - @" + String.Join(", @", e.NewTrack.Artists.Select(x => x.Name)));

    }
    protected override void OnPlaylistsChanged(PlaylistsChanged e)
    {
        Debug.Log("OnPlaylistChanged();");
        var curSong = GetCurrentSongInfo();
        if(curSong != null)
            BarUI.instance.UpdateSong(curSong.Title + " - @" + String.Join(", @", curSong.Artists.Select(x => x.Name)));
        //BarUI.instance.UpdateSong("'" + e.Playlists. .Title + "' - " + String.Join(", ", e.NewTrack.Artists.Select(x => x.Name)));

    }

    protected override void OnConnectedChanged(ConnectedChanged e)
    {
        MusicManager.instance.PlaySong(NiteoutPlaylist);
        Debug.Log("OnConnectedChanged " + e.IsConnected);
        //TODO Lets start our playlist.
        var curSong = GetCurrentSongInfo();
        if (curSong != null)
            BarUI.instance.UpdateSong(curSong.Title + " - @" + String.Join(", @", curSong.Artists.Select(x => x.Name)));

        //Launch app on this device.

        StartSpotify();

        var devices = SpotifyService.GetDevices();
        Debug.Log("List all devices:");
        foreach (var item in devices)
        {
            Debug.Log("Device: "+item.Name);
        }
    }

    public void StartSpotify()
    {
#if UNITY_ANDROID
        Debug.Log("Launching Spotify on Android.");
        AndroidJavaClass activityClass;
        AndroidJavaObject activity, packageManager;
        AndroidJavaObject launch;


        activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        packageManager = activity.Call<AndroidJavaObject>("getPackageManager");
        launch = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", "com.spotify.music");
        activity.Call("startActivity", launch);

#elif UNITY_IOS

#endif
    }
}
