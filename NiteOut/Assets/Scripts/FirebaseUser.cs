﻿using Firebase.Auth;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace NiteOut.Model
{
    public class FirebaseUser : IUser
    {
        private readonly FirebaseAuth firebaseAuth;

        public FirebaseUser(FirebaseAuth firebaseAuth)
        {
            this.firebaseAuth = firebaseAuth;
        }

        public string UserID { get { return firebaseAuth.CurrentUser.UserId; } }

        //public string Username { get { return firebaseAuth.CurrentUser.DisplayName; } }
        public string Username { get { return DatabaseHelper.Database.ProfileData.FirstName; } }

        public string Email { get { return firebaseAuth.CurrentUser.Email; } }

        public string[] GetUserFriends()
        {
            return new string[0];
        }

        public void SendChatMessage(string receiverID, string content, MessageType type)
        {
            DatabaseHelper.Database.SendChatMessage(new ChatMessage(UserID, receiverID, content, type));
        }

        public Task<ChatMessage[]> GetChatMessages()
        {
            //return DatabaseHelper.Database.GetChatMessages(UserID);
            return null;
        }

        public void UpdateProfileData(ProfileData profileData, Action callback)
        {
            DatabaseHelper.Database.UploadProfileData(UserID, profileData, callback);
        }
    }
}
