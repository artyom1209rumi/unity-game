/**
 I had to do the following to get the app to build in xcode
 1. add
 <key>LSApplicationQueriesSchemes</key>
 <array>
 <string>spotify</string>
 </array>
 to info.plist to allow spotify app to open.
 
 2. add SpotifyiOS.framework to frameworks/ in xcode and add as target for unity-framework.
 3. pod installs v6 of GoogleSignIn which does not build. Change GoogleSignIn in PodFile to '= 5.0.2' and run pod install
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SpotifyiOS/SpotifyiOS.h>
#import "UnityAppController.h"
#import "AppDelegateListener.h"

@interface SpotifyIOSPlugin: UnityAppController <SPTSessionManagerDelegate, SPTAppRemoteDelegate, SPTAppRemotePlayerStateDelegate>
@property (nonatomic, strong) SPTSessionManager *sessionManager;
@property (nonatomic, strong) SPTConfiguration *configuration;
@property (nonatomic, strong) SPTAppRemote *appRemote;
@end

// Set SpotifyIOSPlugin to be the loaded UnityAppController
//IMPL_APP_CONTROLLER_SUBCLASS(SpotifyIOSPlugin)

@implementation SpotifyIOSPlugin

- (id) init {
    NSLog(@"SPOTIFY_PLUGIN INIT");
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onOpenURL:)
                                                     name:kUnityOnOpenURL object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                selector:@selector(onApplicationDidBecomeActive:)
//                                                    name:UIApplicationDidBecomeActiveNotification
//                                                  object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                selector:@selector(applicationWillResignActive:)
//                                                    name:UIApplicationWillResignActiveNotification
//                                                  object:nil];
    }
    return self;
}

//- (void)applicationWillResignActive:(UIApplication *)application
//{
//    NSLog(@"SPOTIFY_PLUGIN applicationWillResignActive");
//  if (self.appRemote.isConnected) {
//    [self.appRemote disconnect];
//  }
//}

//- (void)applicationDidBecomeActive:(UIApplication *)application
//{
//    NSLog(@"SPOTIFY_PLUGIN applicationDidBecomeActive");
//
//  if (self.appRemote.connectionParameters.accessToken && !self.appRemote.isConnected) {
//    [self.appRemote connect];
//  }
//}

- (void) onOpenURL:(NSNotification*)notification {
    NSURL* url = [[notification userInfo] objectForKey:@"url"];
    UIApplication* app = [[notification userInfo] objectForKey:@"application"];
    NSDictionary<UIApplicationOpenURLOptionsKey,id> * options = [[notification userInfo] objectForKey:@"options"];
    
    NSLog(@"SPOTIFY_PLUGIN OPEN URL");
    NSDictionary *params = [self.appRemote authorizationParametersFromURL:url];
    NSString *token = params[SPTAppRemoteAccessTokenKey];
    
    // We must call application so that sessionManager could create it's session from the openUrl callback
    [self.sessionManager application:app openURL:url options:options];
    
    if (token) {
        self.appRemote.connectionParameters.accessToken = token;
        self.appRemote.delegate = self;
        [self.appRemote connect];
    } else if (params[SPTAppRemoteErrorDescriptionKey]) {
        NSLog(@"SPOTIFY_PLUGIN appRemote openURL handler error getting token. %@", params[SPTAppRemoteErrorDescriptionKey]);
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
//{
//    NSLog(@"OPEN URL");
//    NSDictionary *params = [self.appRemote authorizationParametersFromURL:url];
//    NSString *token = params[SPTAppRemoteAccessTokenKey];
//    if (token) {
//        self.appRemote.connectionParameters.accessToken = token;
//    } else if (params[SPTAppRemoteErrorDescriptionKey]) {
//        NSLog(@"%@", params[SPTAppRemoteErrorDescriptionKey]);
//    }
//    return [super application:app openURL:url options:options];;
//}

//-(BOOL)application:(UIApplication*) application didFinishLaunchingWithOptions:(NSDictionary*) options
//{
//    NSLog(@"[OverrideAppDelegate application:%@ didFinishLaunchingWithOptions:%@]", application, options);
//    return [super application:application didFinishLaunchingWithOptions:options];
//}
//
//- (void)applicationWillResignActive:(UIApplication *)application
//{
//  if (self.appRemote.isConnected) {
//    [self.appRemote disconnect];
//  }
//  return [super applicationWillResignActive:application];
//}
//
//- (void)applicationDidBecomeActive:(UIApplication *)application
//{
//  if (self.appRemote.connectionParameters.accessToken) {
//    [self.appRemote connect];
//  }
//  return [super applicationDidBecomeActive:application];
//}


+(void)alertView:(NSString *)title addMessage:(NSString *)  message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [UnityGetGLViewController() presentViewController:alert animated:YES completion:nil];
}

#pragma mark - SPTSessionManagerDelegate

- (void)sessionManager:(SPTSessionManager *)manager didInitiateSession:(SPTSession *)session
{
    NSLog(@"SPOTIFY_PLUGIN sessionManager success: %@", session);
    [self handleSessionManagerConnect:session];
}

- (void)sessionManager:(SPTSessionManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"SPOTIFY_PLUGIN sessionManager fail: %@", error);
    
    NSLog(@"SPOTIFY_PLUGIN removing bad savedSession");
    // since something went wrong we should delete the object
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    [currentDefaults removeObjectForKey:@"savedSession"];
    
    
}

- (void)sessionManager:(SPTSessionManager *)manager didRenewSession:(SPTSession *)session
{
    NSLog(@"SPOTIFY_PLUGIN sessionManager renewed: %@", session);
    [self handleSessionManagerConnect:session];
}

#pragma mark - SPTAppRemoteDelegate

- (void)appRemoteDidEstablishConnection:(SPTAppRemote *)appRemote
{
    NSLog(@"SPOTIFY_PLUGIN appRemote CONNECTED");
    // Connection was successful, you can begin issuing commands
    self.appRemote.playerAPI.delegate = self;
    [self.appRemote.playerAPI subscribeToPlayerState:^(id _Nullable result, NSError * _Nullable error) {
        if (error) {
            NSLog(@"SPOTIFY_PLUGIN appRemote error: %@", error.localizedDescription);
        }
    }];
    // seems like we need to call play here. it does not play automatically
    [SpotifyIOSPlugin play:self.configuration.playURI];
}

- (void)appRemote:(SPTAppRemote *)appRemote didDisconnectWithError:(NSError *)error
{
    NSLog(@"SPOTIFY_PLUGIN appRemote disconnected");
}

- (void)appRemote:(SPTAppRemote *)appRemote didFailConnectionAttemptWithError:(NSError *)error
{
    NSLog(@"SPOTIFY_PLUGIN appRemote FAILED");
}

- (void)playerStateDidChange:(id<SPTAppRemotePlayerState>)playerState
{
    NSLog(@"SPOTIFY_PLUGIN appRemote player state changed");
    NSLog(@"SPOTIFY_PLUGIN appRemote Track name: %@", playerState.track.name);
    
}

- (void)saveSessionObject:(SPTSession *)session
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    //    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:session];
    NSError *error;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:session requiringSecureCoding:NO error:&error];
    
    if (error != nil) {
        NSLog(@"SPOTIFY_PLUGIN error archiving session object");
    } else {
        [currentDefaults setObject:data forKey:@"savedSession"];
        [currentDefaults synchronize];
        NSLog(@"SPOTIFY_PLUGIN session saved");
    }
}

- (SPTSession *)getSavedSessionObject
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [currentDefaults objectForKey:@"savedSession"];
    NSError *error;
    SPTSession *savedSession = [NSKeyedUnarchiver unarchivedObjectOfClass:[SPTSession class] fromData:data error:&error];
    
    if (error != nil) {
        NSLog(@"SPOTIFY_PLUGIN error unarchiving saved session object");
        // since something went wrong we should delete the object
        [currentDefaults removeObjectForKey:@"savedSession"];
        return nil;
    }
    return savedSession;
}

- (void)handleSessionManagerConnect:(SPTSession *)session
{
    [self saveSessionObject:session];
    
    [SPTAppRemote checkIfSpotifyAppIsActive:^(BOOL active) {
        if (active) {
            NSLog(@"SPOTIFY_PLUGIN spotify app IS ACTIVE");
            // if the spotify app is already active, we can just connect.
            self.appRemote.connectionParameters.accessToken = session.accessToken;
            self.appRemote.delegate = self;
            [self.appRemote connect];
        } else {
            NSLog(@"SPOTIFY_PLUGIN spotify app IS NOT ACTIVE");
            // if spotify is not active we need to call authorizeAndPlayURI to open the app.
            // there is no other way to start the app.
            BOOL spotifyInstalled = [self.appRemote authorizeAndPlayURI:self.configuration.playURI];
            if (!spotifyInstalled) {
                // we might want to handle this
            }
        }
    }];
}


+(void)authenticate:(NSString *)url
{
    NSLog(@"SPOTIFY_PLUGIN authenticate with url: %@", url);
    
    NSString *spotifyClientID = @"aba1a161df6340889f8592eadbaacf32";
    NSURL *spotifyRedirectURL = [NSURL URLWithString:@"niteoutapp://oauth/callback/spotify/"];
    
    NSURL *tokenSwapURL = [NSURL URLWithString:@"https://accounts.spotify.com/api/token"];
    NSURL *tokenRefreshURL = [NSURL URLWithString:@"https://accounts.spotify.com/api/refresh_token"];
    
    SpotifyIOSPlugin *sharedManager = [SpotifyIOSPlugin sharedManager];
    
    sharedManager.configuration.tokenSwapURL = tokenSwapURL;
    sharedManager.configuration.tokenRefreshURL = tokenRefreshURL;
    //    sharedManager.configuration.playURI = @"spotify:track:20I6sIOMTCkB6w7ryavxtO";
    
    if (sharedManager.appRemote.connected) {
        NSLog(@"SPOTIFY_PLUGIN sharedManager already connected");
        // if we are already connected lets just call play
        [self play:url];
    }
    
    // initiate the SDK
    sharedManager.configuration  = [[SPTConfiguration alloc] initWithClientID:spotifyClientID redirectURL:spotifyRedirectURL];
    sharedManager.configuration.playURI = url;
    
    // initiate the sessionManager
    sharedManager.sessionManager = [[SPTSessionManager alloc] initWithConfiguration:sharedManager.configuration delegate:sharedManager];
    
    // get saved session and check if it exists
    SPTSession *savedSession = [sharedManager getSavedSessionObject];
    
    if (savedSession == nil) {
        // invoke the auth modal if we have no saved session. There's no way to refresh the token. probably first time authenticating
        // this opens the modal which asks for permissions. This is required to obtain refresh token.
        // Calling appRemote.authorizeAndPlayURI only obtains token and can't be used to persist permissions
        NSLog(@"SPOTIFY_PLUGIN no savedsession. opening authenticate modal");
        SPTScope requestedScope = SPTAppRemoteControlScope;
        [sharedManager.sessionManager initiateSessionWithScope:requestedScope options:SPTDefaultAuthorizationOption];
    } else {
        // if there is a session, set it and try renewing
        sharedManager.sessionManager.session = savedSession;
        NSLog(@"SPOTIFY_PLUGIN Found savedsession. attempting renewSession");
        [sharedManager.sessionManager renewSession];
    }
    
    // initialize the app remote
    sharedManager.appRemote = [[SPTAppRemote alloc] initWithConfiguration:sharedManager.configuration logLevel:SPTAppRemoteLogLevelDebug];
    
    //    BOOL spotifyInstalled = [sharedManager.appRemote authorizeAndPlayURI:@"spotify:track:7iOTBeLm5qR9N23fjQy8Kl"];
    // app remote
    //    sharedManager.appRemote = [[SPTAppRemote alloc] initWithConfiguration:sharedManager.configuration logLevel:SPTAppRemoteLogLevelDebug];
    //    sharedManager.appRemote.delegate = sharedManager;
    
}

+(void)play:(NSString *)url
{
    NSLog(@"SPOTIFY_PLUGIN play url: %@", url);
    if (url == nil) {
        NSLog(@"SPOTIFY_PLUGIN play called with no url");
        return;
    }
    
    SpotifyIOSPlugin *sharedManager = [SpotifyIOSPlugin sharedManager];
    if (sharedManager.appRemote.connected) {
        NSLog(@"SPOTIFY_PLUGIN play appRemote IS CONNECTED");
        //        BOOL spotifyInstalled = [sharedManager.appRemote authorizeAndPlayURI:url];
        [sharedManager.appRemote.playerAPI play:url callback:^(id  _Nullable result, NSError * _Nullable error) {
        }];
    } else {
        NSLog(@"SPOTIFY_PLUGIN play appRemote IS NOT CONNECTED");
        [sharedManager.appRemote connect];
    }
}



+ (id)sharedManager {
    static SpotifyIOSPlugin *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

@end

extern "C"
{
void _ShowAlert(const char *title, const char *message)
{
    [SpotifyIOSPlugin alertView:[NSString stringWithUTF8String:title] addMessage:[NSString stringWithUTF8String:message]];
}
void _Authenticate(const char *url)
{
    [SpotifyIOSPlugin authenticate:[NSString stringWithUTF8String:url]];
}
void _Play(const char *url)
{
    [SpotifyIOSPlugin play:[NSString stringWithUTF8String:url]];
}
}
