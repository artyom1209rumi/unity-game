using System.Runtime.InteropServices;
using UnityEngine;

public class SpotifyIOSPlugin : MonoBehaviour
{

    #if UNITY_IOS
    
    [DllImport("__Internal")]
    private static extern void _ShowAlert(string title, string message);

    public static void ShowAlert(string title, string message)
    {
        _ShowAlert(title, message);
    }

    [DllImport("__Internal")]
    // authenticate and play initial url
    private static extern void _Authenticate(string url);

    public static void Authenticate(string url)
    {
        _Authenticate(url);
    }

    [DllImport("__Internal")]
    // play any spotify url (track, album, playlist)
    private static extern void _Play(string url);

    public static void Play(string url)
    {
        _Play(url);
    }

    #else

    #endif
}
