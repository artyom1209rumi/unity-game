using System;
using UnityEngine;

public class GridDancePanelReziser : MonoBehaviour
{
    [SerializeField]
    private Padding padding;
    [SerializeField]
    private Vector2 cellSpacing;
    [SerializeField]
    private int rowCount = 1;
    [SerializeField]
    private float textHeight = 15f;
    [SerializeField]
    private RectTransform danceButton;

    private RectTransform myRectTransform;

    private void Start()
    {
        myRectTransform = transform as RectTransform;
        PositionAndResizeDanceMovesButtons();
        PositionDanceMoveButton();
        gameObject.SetActive(false);
    }

    private float CalculateCellSize()
    {
        return (myRectTransform.rect.width - (padding.Left + padding.Right) - (cellSpacing.x * (rowCount - 1))) / (float)rowCount;
    }

    private void PositionAndResizeDanceMovesButtons()
    {
        float cellSize = CalculateCellSize();
        float danceMoveButtonOffest = (cellSize - danceButton.rect.height) * .5f;
        int r = 0, c = 1;
        for (int i = 0; i < 8; ++i)
        {
            RectTransform child = myRectTransform.GetChild(i) as RectTransform;
            float positionX = myRectTransform.anchoredPosition.x + padding.Left + ((cellSize + cellSpacing.x) * r);
            float positionY = myRectTransform.anchoredPosition.y + padding.Bottom - danceMoveButtonOffest + ((cellSize + cellSpacing.y + textHeight) * c);
            child.anchoredPosition = new Vector2(positionX, positionY);
            child.sizeDelta = Vector2.one * cellSize;
            ++r;
            if (r == rowCount)
            {
                r = 0;
                --c;
            }
        }
    }

    private void PositionDanceMoveButton()
    {
        RectTransform closeDanceButton = myRectTransform.Find("CloseDancePanel") as RectTransform;
        RectTransform closeDanceButtonChild = closeDanceButton.GetChild(0) as RectTransform;
        danceButton.anchoredPosition = closeDanceButton.anchoredPosition + new Vector2((closeDanceButton.rect.width - closeDanceButtonChild.rect.width) * .5f, (closeDanceButton.rect.height - closeDanceButtonChild.rect.height) * .5f);
    }

    [Serializable]
    public class Padding
    {
        public float Top { get { return top; } }
        public float Bottom { get { return bottom; } }
        public float Right { get { return right; } }
        public float Left { get { return left; } }
        [SerializeField]
        private float top;
        [SerializeField]
        private float bottom;
        [SerializeField]
        private float right;
        [SerializeField]
        private float left;
    }
}
