using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Firebase.Auth;
using Firebase.Extensions;
using System;
using System.IO;
using NiteOut.Model;
using AdvancedInputFieldPlugin;
using System.Collections;
using Firebase.Storage;

public class CompleteProfileUI : MonoBehaviour
{
    [SerializeField]
    private GameObject spotifyPanel;
    [SerializeField]
    private GameObject loadingPanel;
    [SerializeField]
    private TextMeshProUGUI addOrChangePictureText;
    [SerializeField]
    private Button allSetButton;
    [SerializeField]
    private RawImage profileRawImage;
    [SerializeField]
    private AdvancedInputField firstNameInputField;
    [SerializeField]
    private AdvancedInputField lastNameInputField;
    [SerializeField]
    private AdvancedInputField aboutYouInputField;
    [SerializeField]
    private RectTransform eventCanvasRectTransform;
    [SerializeField]
    private VerticalLayoutGroup verticalContentLayoutGroup;
    [SerializeField]
    private ScrollRect contentScrollRect;
    [SerializeField]
    private RectTransform background;
    private RectTransform myRectTransform;
    /*
    private TextMeshProUGUI currentOperationText;
    private Slider progressBarSlider;
    private FirebaseAuth firebaseAuth;
    private string selectedProfileImageFilePath;
    private string lastPhotoRemotePath, lastPhotoLocalPath;
    */
    private bool isComingFromProfile;
    private AdvancedInputField lastSelectedInputField;
    private int keyboardHeight = 630;
    private const int BASE_SCREEN_HEIGHT = 1920;
    private const float PANEL_OFFSET = 249.39f;
    //private Texture2D lastScaledPickedTexture;

    private void Start()
    {
        print("SH : " + Screen.height);
        print("SR : " + Screen.currentResolution);
        myRectTransform = transform as RectTransform;
        /*
        currentOperationText = loadingPanel.transform.Find("CurrentOperationText").GetComponent<TextMeshProUGUI>();
        progressBarSlider = loadingPanel.transform.Find("ProgressBarSlider").GetComponent<Slider>();
        firebaseAuth = FirebaseAuth.DefaultInstance;
        */
        ButtonStateHandler.Instance.SetInteractable(allSetButton, CanInteract());
        if (!isComingFromProfile)
            UpdateProfileData(DatabaseHelper.Database.ProfileData, false);
        //UpdateProfileData(ProfilePersistent.LoadProfileData());
        /*
        try
        {
            if (firebaseAuth.CurrentUser.DisplayName != "")
            {
                string[] str = firebaseAuth.CurrentUser.DisplayName.Split(' ');
                if (str[0] != null)
                    firstNameInputField.text = str[0];
                if (str[1] != null)
                    lastNameInputField.text = str[1];
            }
        }
        catch
        {
            print("LastName does not exist in display name.");
        }
        */
    }

    /*
        private void Update()
        {
            GameObject selectedObject = EventSystem.current.currentSelectedGameObject;
            if(selectedObject != null)
            {
                AdvancedInputField selectedInputField = selectedObject.GetComponent<AdvancedInputField>();
                if (selectedInputField != null && selectedInputField != lastSelectedInputField)
                    lastSelectedInputField = selectedInputField;
            }
        }
    */

    private void OnEnable()
    {
        GalleryPicker.OnImagePicked += OnProfileImagePicked;
        //NativeKeyboardManager.AddKeyboardHeightChangedListener(OnKeyboardHeightChanged);
    }

    private void OnDisable()
    {
        GalleryPicker.OnImagePicked -= OnProfileImagePicked;
        //NativeKeyboardManager.RemoveKeyboardHeightChangedListener(OnKeyboardHeightChanged);
    }

    private void OnProfileImagePicked(string filePath)
    {
        //selectedProfileImageFilePath = filePath;
        //profileRawImage.texture = TextureLoader.LoadFromFilePath(filePath);
        //lastScaledPickedTexture = NativeGallery.LoadImageAtPath(filePath, 512, false);
        profileRawImage.texture = NativeGallery.LoadImageAtPath(filePath, 512, false);
        profileRawImage.SizeToParent();
    }

    public void OnInteractionDetected()
    {
        ButtonStateHandler.Instance.SetInteractable(allSetButton, CanInteract());
    }

    private bool CanInteract()
    {
        return !string.IsNullOrEmpty(firstNameInputField.Text) &&
               !string.IsNullOrEmpty(lastNameInputField.Text) &&
               !string.IsNullOrEmpty(aboutYouInputField.Text);
    }

    public void OnAddOrChangeImageButtonClicked()
    {
        //if (addOrChangePictureText.text == "Add Picture")
        addOrChangePictureText.text = "Change Picture";
        //else
        //addOrChangePictureText.text = "Add Picture";
        GalleryPicker.PickImage(512);
    }

    public void UpdateProfileData(ProfileData profileData, bool isComingFromProfile)
    {
        if (profileData == null)
            return;
        this.isComingFromProfile = isComingFromProfile;
        //selectedProfileImageFilePath = profileData.RemotePath;
        if (!string.IsNullOrEmpty(profileData.PhotoRemotePath))
        {
            DatabaseHelper.Storage.DownloadFile(profileData.UserID, profileData.PhotoRemotePath, (imageBytes) =>
            {
                profileRawImage.texture = TextureLoader.LoadFromBytes(imageBytes);
                profileRawImage.SizeToParent();
            });
        }
        firstNameInputField.Text = profileData.FirstName;
        lastNameInputField.Text = profileData.LastName;
        aboutYouInputField.Text = profileData.Description;
        //lastPhotoRemotePath = profileData.PhotoRemotePath;
        addOrChangePictureText.text = "Change Picture";
        gameObject.SetActive(true);
    }

    public void OnAllSetButtonClicked()
    {
        gameObject.SetActive(false);
        ProfileData profileData = new ProfileData(firstNameInputField.Text, lastNameInputField.Text, aboutYouInputField.Text);
        //if (!string.IsNullOrEmpty(selectedProfileImageFilePath) && lastPhotoLocalPath != selectedProfileImageFilePath)
        //{
            //string uniqueFileName = Guid.NewGuid().ToString() + Path.GetFileName(selectedProfileImageFilePath);
            //lastPhotoLocalPath = selectedProfileImageFilePath;
            byte[] scaledImageBytes = ((Texture2D)profileRawImage.mainTexture).EncodeToJPG();
            DatabaseHelper.Storage.UploadFile(scaledImageBytes, profileData.PhotoRemotePath, new StorageProgress<UploadState>(state =>
            {
                /*
                loadingPanel.SetActive(true);
                progressBarSlider.value = (float)(state.BytesTransferred / (double)state.TotalByteCount);
                currentOperationText.text = "Uploading profile data..." + (progressBarSlider.value * 100f).ToString("F0") + "%";
                */
                print("Pecentage : " + (state.BytesTransferred / (double)state.TotalByteCount) * 100f);
                print(state.BytesTransferred + "B/" + state.TotalByteCount + "B");
            }), null);
        //}
        DatabaseHelper.User.UpdateProfileData(profileData, OnProfileDataUploaded);
        if (isComingFromProfile)
            ProfileUI.Instance.ShowProfile(DatabaseHelper.User.UserID, profileData);
        else
            spotifyPanel.SetActive(true);
        /*
        string uniqueFileName = Guid.NewGuid().ToString() + Path.GetFileName(selectedProfileImageFilePath);
        string destinationFilePath = "Users/" + uniqueFileName;
        DatabaseHelper.Storage.UploadFile(selectedProfileImageFilePath, destinationFilePath, (remoteFilePath) =>
        {
            DatabaseHelper.User.
            profileData.SetPhotoLocalPath(selectedProfileImageFilePath);
            profileData.SetPhotoRemotePath(remoteFilePath);
            ProfilePersistent.SaveProfileData(profileData);
            if (isComingFromProfile)
                ProfileUI.Instance.ShowProfile(DatabaseHelper.User.UserID);
            else
                spotifyPanel.SetActive(true);
        });
        */
        /*
        UserProfile userProfile = new UserProfile() { DisplayName = firstNameInputField.text };
        string uniqueFileName = Guid.NewGuid().ToString() + Path.GetFileName(selectedProfileImageFilePath);
        string destinationFilePath = "Users/" + uniqueFileName;
        if (!string.IsNullOrEmpty(selectedProfileImageFilePath))
        {
            DatabaseHelper.Storage.UploadFile(selectedProfileImageFilePath, destinationFilePath, (remoteFilePath) =>
            {
                DatabaseHelper.Storage.GetRemoteFileURL(remoteFilePath, (url) =>
                {
                    userProfile.PhotoUrl = new Uri(url);
                    UpdateUserProfile(userProfile);
                });
            });
        }
        else
            UpdateUserProfile(userProfile);
        */
    }

    private void OnProfileDataUploaded()
    {
        /*
        if (!PlayerPrefs.HasKey("ProfileData"))
            PlayerPrefs.SetInt("ProfileData", 1);
        if (isComingFromProfile)
            ProfileUI.Instance.ShowProfile(DatabaseHelper.User.UserID);
        else
            spotifyPanel.SetActive(true);
        currentOperationText.text = "";
        progressBarSlider.value = 0;
        loadingPanel.SetActive(false);
        */
    }

    /*
    private void UpdateUserProfile(UserProfile userProfile)
    {
        firebaseAuth.CurrentUser.UpdateUserProfileAsync(userProfile).ContinueWithOnMainThread((task) =>
        {
            if (task.IsCanceled)
                Debug.LogError("UpdateUserProfileAsync was canceled.");
            else if (task.IsFaulted)
                Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
            else
            {
                Debug.Log("User Profile has been updated successfully!");
                spotifyPanel.SetActive(true);
            }
        });
    }
    */

    public void OnBeginEdit(BeginEditReason reason)
    {
        if (lastSelectedInputField != null)
            lastSelectedInputField.ShouldBlockDeselect = false;
        GameObject selectedObject = EventSystem.current.currentSelectedGameObject;
        if (selectedObject != null)
        {
            AdvancedInputField selectedInputField = selectedObject.GetComponent<AdvancedInputField>();
            if (selectedInputField != null)
            {
                lastSelectedInputField = selectedInputField;
                lastSelectedInputField.ShouldBlockDeselect = true;
                // keyboard open
                StopAllCoroutines();
                StartCoroutine(SetScrollOffset());
            }
        }
    }

    /*
    private void OnKeyboardHeightChanged(int keyboardHeight)
    {
        #if UNITY_ANDROID || UNITY_IOS
        if (keyboardHeight > 0)
        {
            this.keyboardHeight = keyboardHeight;
            SetScrollOffset();
        }
        #endif
    }
    */

    public void OnEndEdit(string text, EndEditReason reason)
    {
        if (reason != EndEditReason.USER_DESELECT)
        {
            lastSelectedInputField.ShouldBlockDeselect = false;
            ResetScrollOffset();
        }
    }

    private IEnumerator SetScrollOffset()
    {
        while (NativeKeyboardManager.Keyboard.State != KeyboardState.VISIBLE)
            yield return null;
        //firstNameInputField.Text = GetKeyboardHeight(false).ToString();
        this.keyboardHeight = KeyboardHelper.GetKeyboardHeight(true);
        float keyboardHeight = this.keyboardHeight * ((Screen.height / (float)BASE_SCREEN_HEIGHT) / eventCanvasRectTransform.localScale.y);
        float completeProfileHalfPadding = (eventCanvasRectTransform.rect.height - myRectTransform.rect.height) * .5f;
        int scrollOffset = (int)((keyboardHeight - (completeProfileHalfPadding + PANEL_OFFSET)) + 25f);
        verticalContentLayoutGroup.padding.bottom = scrollOffset;
        LayoutRebuilder.ForceRebuildLayoutImmediate(verticalContentLayoutGroup.transform as RectTransform);
        AutoScrollToInputField(lastNameInputField.name);
    }


    private void ResetScrollOffset()
    {
        verticalContentLayoutGroup.padding.bottom = 0;
        LayoutRebuilder.ForceRebuildLayoutImmediate(verticalContentLayoutGroup.transform as RectTransform);
    }

    private void AutoScrollToInputField(string selectedInputFieldName)
    {
        switch (selectedInputFieldName)
        {
            case "FirstNameInputField":
                contentScrollRect.verticalNormalizedPosition = .6f;
                break;
            case "LastNameInputField":
                contentScrollRect.verticalNormalizedPosition = .3f;
                break;
            case "AboutYouInputField":
                contentScrollRect.verticalNormalizedPosition = 0f;
                break;
        }
    }
}


