using UnityEngine;

public class EventUI : MonoBehaviour
{
    [SerializeField]
    private GameObject avatarSelectionPanel;

    public void OnLetsPartyButtonClicked()
    {
        gameObject.SetActive(false);
        avatarSelectionPanel.SetActive(true);
    }
}
