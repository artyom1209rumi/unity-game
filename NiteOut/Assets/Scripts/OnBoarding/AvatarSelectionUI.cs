using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using NiteOut.Model;

public class AvatarSelectionUI : MonoBehaviour
{
    [SerializeField]
    private GameObject completeProfilePanel;
    [SerializeField]
    private GameObject spotifyPanel;
    [SerializeField]
    private Transform avatarsPanel;
    [SerializeField]
    private Button confirmAvatarButton;

    private Image lastAvatarSelectionImage;
    private bool isComingFromProfile;

    private void Start()
    {
        if(ButtonStateHandler.Instance)
            ButtonStateHandler.Instance.SetInteractable(confirmAvatarButton, false);
    }

    public void UpdateAvatarSelection()
    {
        isComingFromProfile = true;
        int templateIndex = PlayerPrefs.GetInt("MVP_Character", 0);
        Image avatarSelectionImage = avatarsPanel.GetChild(templateIndex).Find("AvatarSelectionImage").GetComponent<Image>();
        gameObject.SetActive(true);
        OnAvatarSelected(avatarSelectionImage);
    }

    public void OnAvatarSelected(Image selectionImage)
    {
        PlayerPrefs.SetInt("MVP_Character", selectionImage.transform.parent.GetSiblingIndex());
        ImageSelecter.Select(ref lastAvatarSelectionImage, selectionImage);
        if(PlayerManager.LocalPlayer != null)
        {
            PlayerManager.LocalPlayer.UpdateAvatarInsideBar(PlayerPrefs.GetInt("MVP_Character"));
        }
        ButtonStateHandler.Instance.SetInteractable(confirmAvatarButton, true);

        //Update UI.


    }

    public void OnConfirmAvatarButtonClicked()
    {
        gameObject.SetActive(false);
        //if (string.IsNullOrEmpty(FirebaseAuth.DefaultInstance.CurrentUser.DisplayName))
        if (isComingFromProfile)
            ProfileUI.Instance.ShowProfile(DatabaseHelper.User.UserID, DatabaseHelper.Database.ProfileData);
        else
        {
            if (DatabaseHelper.Database.ProfileData == null)
                completeProfilePanel.SetActive(true);
            else
                spotifyPanel.SetActive(true);
        }
    }
}
