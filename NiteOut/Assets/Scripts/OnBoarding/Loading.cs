using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Loading : MonoBehaviour
{
    [SerializeField]
    private string[] hints;
    [SerializeField]
    private Sprite[] backgroundSprites;
    [SerializeField]
    private TextMeshProUGUI hintText;
    [SerializeField]
    private Image backgroundImage;
    [SerializeField]
    private Slider progressBarSlider;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private string GetRandomHint()
    {
        return hints[Random.Range(0, hints.Length)];
    }

    public void LoadScene(string sceneName, int spriteBackgroundIndex = -1)
    {
        Debug.Log("LoadScene: " + sceneName);
        gameObject.SetActive(true);
        hintText.text = GetRandomHint();
        if (spriteBackgroundIndex > -1 && spriteBackgroundIndex < backgroundSprites.Length)
        {
            backgroundImage.color = Color.white;
            backgroundImage.sprite = backgroundSprites[spriteBackgroundIndex];
        }
        StartCoroutine(LoadSceneCoroutine(sceneName));
    }

    private IEnumerator LoadSceneCoroutine(string sceneName)
    {
        AsyncOperation loadingOperation = SceneManager.LoadSceneAsync(sceneName);
        while (!loadingOperation.isDone)
        {
            progressBarSlider.value = Mathf.Clamp01(loadingOperation.progress / .9f);
            yield return null;
        }
        gameObject.SetActive(false);
    }
}
