using UnityEngine;

public class SpotifyUI : MonoBehaviour
{
    [SerializeField]
    private GameObject barUI;

    public void OnConnectToSpotifyPremiumButtonClicked()
    {
        MusicManager.instance.AuthorizeSpotify(OnContinue);
    }

    public void OnContinueWithoutMusicButtonClicked()
    {

        OnContinue();
    }

    public void OnContinue()
    {
        barUI.SetActive(true);
        BarNetworkManager.instance.ConnectToServer();
        transform.parent.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
