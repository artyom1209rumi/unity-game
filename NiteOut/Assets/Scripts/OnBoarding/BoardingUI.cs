using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Firebase;
using Firebase.Auth;
using Facebook.Unity;
using Firebase.Extensions;
using Google;
using Google.Impl;
using System.Threading;
using System.Threading.Tasks;
using NiteOut.Model;
using FirebaseUser = Firebase.Auth.FirebaseUser;
using System.Security.Cryptography;
using System.Text;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using System;

public class BoardingUI : MonoBehaviour
{
    [SerializeField]
    private Loading loading;
    [Header("SignUp")]
    [SerializeField]
    private GameObject signUpPanel;
    [SerializeField]
    private TMP_InputField signUpEmailInputField;
    [SerializeField]
    private TMP_InputField signUpPasswordInputField;
    [SerializeField]
    private TMP_InputField confirmPasswordInputField;
    [SerializeField]
    private Toggle[] showPasswordToggles = new Toggle[2]; 
    [SerializeField]
    private Sprite eyeOffSprite, eyeOnSprite;
    [SerializeField]
    private TextMeshProUGUI signUpPasswordStrengthText;
    [SerializeField]
    private TextMeshProUGUI signUpErrorText;
    [SerializeField]
    private Toggle signUpCheckBoxToggle;
    [SerializeField]
    private Button signUpCreateAccountButton;
    [Header("SignIn")]
    [SerializeField]
    private GameObject signInPanel;
    [SerializeField]
    private TMP_InputField signInEmailInputField;
    [SerializeField]
    private TMP_InputField signInPasswordInputField;
    [SerializeField]
    private TextMeshProUGUI signInErrorText;
    [SerializeField]
    private Button forgotPasswordButton;
    [SerializeField]
    private Button signInButton;
    [SerializeField]
    private int maxLoginFailedAttempts = 3;
    private const string EMAIL_PATTERN = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
    private int loginFailedAttempts;

    private FirebaseAuth auth;

    [SerializeField]
    private Button AppleSignIn;
    [SerializeField]
    private Button AppleSignUp;

    #region Sign Up Callbacks
    public void OnSignUpFacebookButtonClicked()
    {
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, OnFacebookLoginResult);
    }

    public void OnSignUpAppleButtonClicked()
    {
        OnSignInAppleButtonClicked();
    }

    public void OnSignUpGoogleButtonClicked()
    {
        OnSignInGoogleButtonClicked();
    }

    public void OnSignUpCreateAccountButtonClicked()
    {
        auth.CreateUserWithEmailAndPasswordAsync(signUpEmailInputField.text, signUpPasswordInputField.text).ContinueWithOnMainThread(task => {
            if (task.IsCanceled)
            {
                CreateErrorMessage();

                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                CreateErrorMessage();

                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            CheckAuthenticated(true);

        });
    }
    // this is the sign text if you have already an account
    public void OnSignUpSignInButtonClicked()
    {
        signInPanel.SetActive(true);
        signUpPanel.SetActive(false);
    }

    public void OnSignUpTermsOfUseButtonClicked()
    {
        Application.OpenURL("www.niteout.co/TOS");
    }

    public void OnSignUpPrivacyPolicyButtonClicked()
    {
        Application.OpenURL("www.niteout.co/Privacy"); 
    }

    public void OnSignUpPasswordValueChanged()
    {
        // check the email, password, and if they agreed on terms and condition then make the create button interactable
        PasswordStrength passwordStrength = PasswordChecker.GetPasswordStrength(signUpPasswordInputField.text);
        switch (passwordStrength)
        {
            case PasswordStrength.Blank:
                signUpPasswordStrengthText.text = "";
                break;
            case PasswordStrength.VeryWeak:
                signUpPasswordStrengthText.text = "Very Weak";
                break;
            case PasswordStrength.VeryStrong:
                signUpPasswordStrengthText.text = "Very Strong";
                break;
            default:
                signUpPasswordStrengthText.text = passwordStrength.ToString();
                break;
        }
        OnSignUpInteractionDetected();
    }

    public void OnSignUpShowPassword(Toggle toggle)
    {
        foreach (Toggle showPasswordToggle in showPasswordToggles)
        {
            showPasswordToggle.SetIsOnWithoutNotify(toggle.isOn);
            showPasswordToggle.image.sprite = toggle.isOn ? eyeOnSprite : eyeOffSprite;
            TMP_InputField parentInputField = showPasswordToggle.GetComponentInParent<TMP_InputField>();
            parentInputField.contentType = toggle.isOn ? TMP_InputField.ContentType.Standard : TMP_InputField.ContentType.Password;
            parentInputField.ForceLabelUpdate();
        }     
    }

    public void OnSignUpInteractionDetected()
    {
        ButtonStateHandler.Instance.SetInteractable(signUpCreateAccountButton, IsValidEmail(signUpEmailInputField.text) && PasswordChecker.IsValidPassword(signUpPasswordInputField.text) && IsPasswordConfirmed() && signUpCheckBoxToggle.isOn);
    }
    #endregion

    #region Sign In Callbacks
    public void OnSignInFacebookButtonClicked()
    {
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, OnFacebookLoginResult);
    }

    public void OnSignInAppleButtonClicked()
    {
        PerformLoginWithAppleIdAndFirebase();
    }

    public void OnSignInGoogleButtonClicked()
    {
        GoogleSignIn.Configuration = new GoogleSignInConfiguration
        {
            RequestIdToken = true,
            // Copy this value from the google-service.json file.
            // oauth_client with type == 3
            WebClientId = "712082895466-irqnarcofn60bscrbifsa5tjov6no8d3.apps.googleusercontent.com"            
        };

        Task<GoogleSignInUser> signIn = GoogleSignIn.DefaultInstance.SignIn();

        TaskCompletionSource<FirebaseUser> signInCompleted = new TaskCompletionSource<FirebaseUser>();
        signIn.ContinueWithOnMainThread(task => {
            if (task.IsCanceled)
            {
                signInCompleted.SetCanceled();
            }
            else if (task.IsFaulted)
            {
                signInCompleted.SetException(task.Exception);
            }
            else
            {

                Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(((Task<GoogleSignInUser>)task).Result.IdToken, null);
                auth.SignInWithCredentialAsync(credential).ContinueWith(authTask => {
                    if (authTask.IsCanceled)
                    {
                        signInCompleted.SetCanceled();
                    }
                    else if (authTask.IsFaulted)
                    {
                        signInCompleted.SetException(authTask.Exception);
                    }
                    else
                    {
                        signInCompleted.SetResult(((Task<FirebaseUser>)authTask).Result);
                        FirebaseUser newUser = authTask.Result;
                        Debug.LogFormat("User signed in successfully: {0} ({1})",
                            newUser.DisplayName, newUser.UserId);

                        CheckAuthenticated(false,3.0f);
                    }
                });
            }
        });

    }

    public void OnSignInButtonClicked()
    {
        auth.SignInWithEmailAndPasswordAsync(signInEmailInputField.text, signInPasswordInputField.text).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                CreateErrorMessage();
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                CreateErrorMessage();

                return;
            }

            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            CheckAuthenticated(true);
        });

        OnFailedLogin();
    }

    void CreateErrorMessage()
    {
        Debug.Log("Creating error message");
        loginFailedAttempts++;
        /*
        errorText.SetText("Invalid email or password");
        Canvas.ForceUpdateCanvases();*/
        //StartCoroutine(CreateErrorMsg());
    }

    IEnumerator CreateErrorMsg()
    {
        yield return new WaitForSeconds(0.1f);
        //errorText.text = "Invalid email or password";
        //errorText.SetText("Invalid email or password");
        //Canvas.ForceUpdateCanvases();
    }

    private void OnFailedLogin()
    {
        //forgotPasswordButton.gameObject.SetActive(true);
        //errorText.gameObject.SetActive(false);

        /*
        // each time there is a failed attempt increase the loginFailedAttempts (loginFailedAttempts++)
        if (errorText.gameObject.activeSelf && loginFailedAttempts >= maxLoginFailedAttempts)
        {
            forgotPasswordButton.gameObject.SetActive(true);
            errorText.gameObject.SetActive(false);
            loginFailedAttempts = maxLoginFailedAttempts;
        }*/
    }

    public void OnSignInRegisterButtonClicked()
    {
        signUpPanel.SetActive(true);
        signInPanel.SetActive(false);
    }

    public void OnSignInForgotPasswordButtonClicked()
    {
        auth.SendPasswordResetEmailAsync(signInEmailInputField.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                signInErrorText.text = "Invalid email.";
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                signInErrorText.text = "Invalid email.";
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                return;
            }

            Debug.Log("Password reset email sent successfully.");
            signInErrorText.text = "Password reset has been sent to your email.";
        });
    }

    public void OnSignInInteractionDetected()
    {
        ButtonStateHandler.Instance.SetInteractable(signInButton, IsValidEmail(signInEmailInputField.text) && PasswordChecker.IsValidPassword(signInPasswordInputField.text));
    }
    #endregion

    private bool IsValidEmail(string email)
    {
        return Regex.IsMatch(email, EMAIL_PATTERN);
    }

    private bool IsPasswordConfirmed()
    {
        return signUpPasswordInputField.text == confirmPasswordInputField.text;
    }
    /// <summary>
    /// Checks if user has been authenticated. If authenticated load next scene.
    /// </summary>
    private void CheckAuthenticated(bool usedEmail, float waitingTime = 1.0f)
    {
        if (auth.CurrentUser != null)
        {
            Debug.Log("Current user not null, so authenticated?");
            //print(auth.CurrentUser.Email);
            //Initialize the database helper to user firebase authenticator
            IAuthenticator authenticator = GetComponent<IAuthenticator>();
            authenticator.Initialize();
            DatabaseHelper.Initialize(authenticator);
            loading.LoadScene("VirtualBar");
            //StartCoroutine(LoadVirtualBarSceneAsync(waitingTime));
            //loading.LoadScene("VirtualBar");
        }
        else
        {
            //Debug.Log("User not authenticated.");
            OnFailedLogin();
            if (usedEmail)
            {
                //TODO Create failed Credentials error.
                //errorText.text = "Invalid email or password";
            }
        }

    }

    private IEnumerator LoadVirtualBarSceneAsync(float time)
    {
        yield return new WaitForSeconds(time);
        loading.LoadScene("VirtualBar");
    }

    void OnFacebookLoginResult(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            Credential credential = FacebookAuthProvider.GetCredential(AccessToken.CurrentAccessToken.TokenString);
            auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task => {
                if (task.IsCanceled)
                {
                    CreateErrorMessage();
                    Debug.LogError("SignInWithCredentialAsync was canceled.");

                    return;
                }
                if (task.IsFaulted)
                {
                    CreateErrorMessage();

                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);

                    return;
                }

                FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
                CheckAuthenticated(false);
            });
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    private void Start()
    {
        FB.Init();
        auth = FirebaseAuth.DefaultInstance;

        // If the current platform is supported
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
            var deserializer = new PayloadDeserializer();
            // Creates an Apple Authentication manager with the deserializer
            this.appleAuthManager = new AppleAuthManager(deserializer);
        }
        else
        {
            AppleSignIn.gameObject.SetActive(false);
            AppleSignUp.gameObject.SetActive(false);
        }
        CheckAuthenticated(false);
        OnSignInInteractionDetected();

        //Remove Dev Credentials. 
        //TODO CRITICAL, THIS IS TEMP FIX!
#if !UNITY_EDITOR
        signInEmailInputField.text = "";
        signInPasswordInputField.text = "";
        signUpEmailInputField.text = "";
        signUpPasswordInputField.text = "";
        
#endif
    }

    private void OnEnable()
    {
        FirebaseAuth.DefaultInstance.StateChanged += DefaultInstance_StateChanged;

    }

    private void OnDisable()
    {
        FirebaseAuth.DefaultInstance.StateChanged -= DefaultInstance_StateChanged;

    }

    private void DefaultInstance_StateChanged(object sender, System.EventArgs e)
    {
        //CheckAuthenticated(true);
/*
#if !UNITY_EDITOR
        if(FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            loading.LoadScene("TestScene");

        }
#endif*/
    }

    // this is just an example of how to call the load scene from here
    private void Update()
    {
        if(loginFailedAttempts > 0 && string.IsNullOrEmpty(signInErrorText.text))
        {
            signInErrorText.text = "Invalid credentials";
        }

        if (this.appleAuthManager != null)
        {
            this.appleAuthManager.Update();
        }
        /*
        if (Input.GetKeyDown(KeyCode.L))
        {
            loading.LoadScene("TestScene");
            // this loads the TestScene and set the background sprite to the first index on backgroundSprites located on the loading script
            //loading.LoadScene("TestScene", 0);
        }
        */
    }
    //[Header("Apple Sign In")]
    private IAppleAuthManager appleAuthManager;
    #region Apple Sign in Stuff

    public void PerformLoginWithAppleIdAndFirebase()
    {
        Debug.Log("Starting apple sign in.");
        var rawNonce = GenerateRandomString(32);
        var nonce = GenerateSHA256NonceFromRawNonce(rawNonce);

        var quickLoginArgs = new AppleAuthQuickLoginArgs(nonce);

        if(appleAuthManager == null)
            this.appleAuthManager = new AppleAuthManager(new PayloadDeserializer());

        Debug.Log("UNITY>> LoginWithAppleId.");


        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        this.appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
        // Obtained credential, cast it to IAppleIDCredential
        var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
            // Apple User ID
            // You should save the user ID somewhere in the device
            //var userId = appleIdCredential.User;
            //        PlayerPrefs.SetString(AppleUserIdKey, userId);

            // Email (Received ONLY in the first login)
            var email = appleIdCredential.Email;

            // Full name (Received ONLY in the first login)
            var fullName = appleIdCredential.FullName;

            // Identity token
            var identityToken = Encoding.UTF8.GetString(
                        appleIdCredential.IdentityToken,
                        0,
                        appleIdCredential.IdentityToken.Length);

            // Authorization code
            var authorizationCode = Encoding.UTF8.GetString(
                        appleIdCredential.AuthorizationCode,
                        0,
                        appleIdCredential.AuthorizationCode.Length);

             PerformFirebaseAuthentication(appleIdCredential, rawNonce);

             Debug.Log("UNITY>> email " + email + " fullName  " + fullName + " Token  " + authorizationCode);

            // And now you have all the information to create/login a user in your system
        }
            },
            error =>
            {
        // Something went wrong
        var authorizationErrorCode = error.GetAuthorizationErrorCode();
            });


        //this.appleAuthManager.QuickLogin(
        //    quickLoginArgs,
        //    credential =>
        //    {
        //        var appleIdCredential = credential as IAppleIDCredential;
        //        if (appleIdCredential != null)
        //        {
        //            this.PerformFirebaseAuthentication(appleIdCredential, rawNonce);
        //        }
        //    },
        //    error =>
        //    {
        //    // Something went wrong
        //        Debug.LogError("LoginApple error");
        //});
    }

    private void PerformFirebaseAuthentication(IAppleIDCredential appleIdCredential,string rawNonce)
    {
        var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken);
        var authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode);
        var firebaseCredential = OAuthProvider.GetCredential("apple.com",identityToken,rawNonce,authorizationCode);

        Debug.Log("Received credentials, Continue with signin.");

        this.auth.SignInWithCredentialAsync(firebaseCredential)
            .ContinueWithOnMainThread(task => HandleSignInWithUser(task));
    }

    private static void HandleSignInWithUser(Task<FirebaseUser> task)
    {
        if (task.IsCanceled)
        {
            Debug.Log("Firebase auth was canceled");
            //firebaseUserCallback(null);
        }
        else if (task.IsFaulted)
        {
            Debug.Log("Firebase auth failed");
            //firebaseUserCallback(null);
        }
        else
        {
            var firebaseUser = task.Result;
            Debug.Log("Firebase auth completed | User ID:" + firebaseUser.UserId);
            
            //firebaseUserCallback(firebaseUser);
        }
    }

    private static string GenerateRandomString(int length)
    {
        if (length <= 0)
        {
            Debug.LogError("Expected nonce to have positive length");
        }

        const string charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._";
        var cryptographicallySecureRandomNumberGenerator = new RNGCryptoServiceProvider();
        var result = string.Empty;
        var remainingLength = length;

        var randomNumberHolder = new byte[1];
        while (remainingLength > 0)
        {
            var randomNumbers = new List<int>(16);
            for (var randomNumberCount = 0; randomNumberCount < 16; randomNumberCount++)
            {
                cryptographicallySecureRandomNumberGenerator.GetBytes(randomNumberHolder);
                randomNumbers.Add(randomNumberHolder[0]);
            }

            for (var randomNumberIndex = 0; randomNumberIndex < randomNumbers.Count; randomNumberIndex++)
            {
                if (remainingLength == 0)
                {
                    break;
                }

                var randomNumber = randomNumbers[randomNumberIndex];
                if (randomNumber < charset.Length)
                {
                    result += charset[randomNumber];
                    remainingLength--;
                }
            }
        }

        return result;
    }

    private static string GenerateSHA256NonceFromRawNonce(string rawNonce)
    {
        var sha = new SHA256Managed();
        var utf8RawNonce = Encoding.UTF8.GetBytes(rawNonce);
        var hash = sha.ComputeHash(utf8RawNonce);

        var result = string.Empty;
        for (var i = 0; i < hash.Length; i++)
        {
            result += hash[i].ToString("x2");
        }

        return result;
    }
    #endregion
}
