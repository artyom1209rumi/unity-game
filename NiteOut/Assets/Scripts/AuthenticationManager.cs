using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AuthenticationManager : MonoBehaviour
{
    public void Signout()
    {
        SignoutStatic();
    }

    public static void SignoutStatic()
    {
        //Signout, delete data and load startup scene.
        Firebase.Auth.FirebaseAuth.DefaultInstance.SignOut();
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
        Application.Quit();
    }
}
