using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class PlayerCamera : MonoBehaviour
{
    CinemachineVirtualCamera vcam;
    private static PlayerCamera Instance;
    public static PlayerCamera instance
    {
        get { return Instance; }
        private set { Instance = value; }
    }

    private void Awake()
    {
        instance = this;
        vcam = GetComponent<CinemachineVirtualCamera>();
    }

    public void SetPlayerCamera(Transform player)
    {
        vcam.Follow = player;
        vcam.LookAt = player;
    }
}
