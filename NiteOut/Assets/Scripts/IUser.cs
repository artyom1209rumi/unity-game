﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace NiteOut.Model
{
    public interface IUser
    {
        string UserID { get; }

        string Username { get; }

        string Email { get; }

        string[] GetUserFriends();

        void UpdateProfileData(ProfileData profideData, Action callback);

        void SendChatMessage(string receiverID, string content, MessageType type);

        Task<ChatMessage[]> GetChatMessages();
    }
}
