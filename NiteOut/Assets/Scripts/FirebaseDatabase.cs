﻿using Firebase.Database;
using Firebase.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace NiteOut.Model
{
    public class FirebaseDatabase : IDatabase
    {
        public event Action<string> ConversationAdded;
        public event Action<string> ConversationRemoved;
        public event Action<string, ChatMessage> ChatMessageAdded;
        public event Action<string, ChatMessage> ChatMessageChanged;
        public event Action<string, ChatMessage> ChatMessagedRemoved;
        private readonly DatabaseReference rootReference;
        private readonly DatabaseReference usersReference;
        private readonly DatabaseReference conversationsReference;
        private readonly DatabaseReference profileDataReference;
        private Dictionary<string, List<ChatMessage>> conversations = new Dictionary<string, List<ChatMessage>>();
        private Dictionary<string, DatabaseReference> conversationsReferences = new Dictionary<string, DatabaseReference>();
        public ProfileData ProfileData { get; private set; }

        //public string LastConversationID { get; private set; }

        //private readonly DatabaseReference messagesReference;

        public FirebaseDatabase()
        {
            rootReference = Firebase.Database.FirebaseDatabase.DefaultInstance.RootReference;
            usersReference = rootReference.Child("Users").Reference;
            conversationsReference = usersReference.Child(DatabaseHelper.User.UserID + "/Conversations/").Reference;
            profileDataReference = usersReference.Child(DatabaseHelper.User.UserID + "/ProfileData/").Reference;
            conversationsReference.ChildAdded += OnConversationAdded;
            conversationsReference.ChildRemoved += OnConversationRemoved;
            profileDataReference.ValueChanged += OnProfileDataValueChanged;
            //Firebase.Database.FirebaseDatabase.DefaultInstance.SetPersistenceEnabled(true);
            //messagesReference = usersReference.Child(DatabaseHelper.User.UserID + "/Messages/");
        }

        public void Dispose()
        {
            conversationsReference.ChildAdded -= OnConversationAdded;
            conversationsReference.ChildRemoved -= OnConversationRemoved;
            /*
            foreach(DatabaseReference conversationReference in conversations.Values)
            {
                conversationReference.ChildAdded -= OnChatMessagedAdded;
                conversationReference.ChildChanged -= OnChatMessageChanged;
                conversationReference.ChildRemoved -= OnChatMessageRemoved;
            }
            */
        }

        public void OpenConversationWith(string userID, Action callback)
        {
            if (string.IsNullOrEmpty(userID))
            {
                Debug.LogError("OpenConversationWithAsync was canceled due to username being null or empty.");
                return;
            }
            DatabaseReference conversationIDReference = conversationsReference.Child(userID).Reference;
            conversationIDReference.Child("ID").SetValueAsync(userID).ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("OpenConversationWithAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("OpenConversationWithAsync encountered an error: " + task.Exception);
                else
                    Debug.Log("Conversation has been open with " + userID);
                //LastConversationID = userID;
                if (!conversations.ContainsKey(userID))
                    conversations.Add(userID, new List<ChatMessage>());
                //ConversationAdded?.Invoke(userID);
                ListenToMessages(userID);
                callback();
            });
        }

        public void OpenConversation(string fromUser, string toUser, Action callback)
        {
            DatabaseReference conversationIDReference = usersReference.Child(fromUser).Child("/Conversations/").Reference;
            conversationIDReference.Child("ID").SetValueAsync(toUser).ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("OpenConversationAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("OpenConversationAsync encountered an error: " + task.Exception);
                else
                    Debug.Log("Conversation has been open with " + toUser);
                callback();
            });
        }
        /*
        public void ListenToConversations()
        {
            conversationsReference.ChildAdded += OnConversationAdded;
        }
        */

        public void ListenToMessages(string conversationID)
        {
            DatabaseReference messagesReference = conversationsReference.Child(conversationID).Child("/Messages/");
            if (!conversationsReferences.ContainsKey(conversationID))
            {
                //conversations.Add(conversationID, new List<ChatMessage>());
                conversationsReferences.Add(conversationID, messagesReference);
                messagesReference.ChildAdded += OnChatMessagedAdded;
                messagesReference.ChildChanged += OnChatMessageChanged;
                messagesReference.ChildRemoved += OnChatMessageRemoved;
            }
        }

        public void StopListeningToMessages(string conversationID)
        {
            DatabaseReference messagesReference = conversationsReference.Child(conversationID).Child("/Messages/");
            if (conversationsReferences.ContainsKey(conversationID))
            {
                //conversations.Add(conversationID, new List<ChatMessage>());
                conversationsReferences.Remove(conversationID);
                messagesReference.ChildAdded -= OnChatMessagedAdded;
                messagesReference.ChildChanged -= OnChatMessageChanged;
                messagesReference.ChildRemoved -= OnChatMessageRemoved;
            }
        }

        public void MarkMessageAsRead(string conversationID, string messageID, Action callback)
        {
            DatabaseReference messageIDReference = conversationsReference.Child(conversationID + "/Messages/").Child(messageID);
            messageIDReference.Child("HasRead").SetValueAsync("True").ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("MarkMessageAsReadAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("MarkMessageAsReadAsync encountered an error: " + task.Exception);
                else
                    Debug.Log("Message has been marked as read " + messageID);
                callback();
            });
        }

        public void MarkConversationAsRead(string conversationID, Action callback)
        {
            List<Task> markMessagesAsReadTasks = new List<Task>();
            foreach(ChatMessage message in GetChatMessages(conversationID))
            {
                DatabaseReference messageIDReference = conversationsReference.Child(conversationID + "/Messages/").Child(message.ID);
                Task task = messageIDReference.Child("HasRead").SetValueAsync(true);
                markMessagesAsReadTasks.Add(task);
            }
            Task.WhenAll(markMessagesAsReadTasks).ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("MarkConversationAsReadAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("MarkConversationAsReadAsync encountered an error: " + task.Exception);
                else
                    Debug.Log("Conversation has been marked as read " + conversationID);
                callback();
            });
        }

        public bool IsConversationRead(string conversationID)
        {
            ChatMessage[] chatMessages = GetChatMessages(conversationID);
            foreach(ChatMessage chatMessage in chatMessages)
            {
                if (!chatMessage.HasRead)
                    return false;
            }
            return true;
        }

        private void OnConversationAdded(object sender, ChildChangedEventArgs e)
        {
            if (e.PreviousChildName != null && e.Snapshot != null && e.Snapshot.Exists)
            {
                if (!conversations.ContainsKey(e.Snapshot.Key))
                    conversations.Add(e.Snapshot.Key, new List<ChatMessage>());
                ConversationAdded?.Invoke(e.Snapshot.Key);
                ListenToMessages(e.Snapshot.Key);
            }
        }

        private void OnConversationRemoved(object sender, ChildChangedEventArgs e)
        {
            if (e.PreviousChildName != null && e.Snapshot != null && e.Snapshot.Exists)
            {
                if (conversations.ContainsKey(e.Snapshot.Key))
                    conversations.Remove(e.Snapshot.Key);
                ConversationRemoved?.Invoke(e.Snapshot.Key);
                StopListeningToMessages(e.Snapshot.Key);
            }
        }

        private void OnChatMessagedAdded(object sender, ChildChangedEventArgs e)
        {
            if (e.PreviousChildName != null && e.Snapshot != null && e.Snapshot.Exists)
            {
                string conversationID = e.Snapshot.Reference.Parent.Parent.Key;
                ChatMessage chatMessage = JsonConvert.DeserializeObject<ChatMessage>(e.Snapshot.GetRawJsonValue());
                chatMessage.SetID(e.Snapshot.Key);
                if (conversations.ContainsKey(conversationID))
                    conversations[conversationID].Add(chatMessage);
                ChatMessageAdded?.Invoke(conversationID, chatMessage);
            }
        }

        private void OnChatMessageChanged(object sender, ChildChangedEventArgs e)
        {
            if (e.Snapshot != null && e.Snapshot.Exists)
            {
                string conversationID = e.Snapshot.Reference.Parent.Parent.Key;
                ChatMessage chatMessage = JsonConvert.DeserializeObject<ChatMessage>(e.Snapshot.GetRawJsonValue());
                chatMessage.SetID(e.Snapshot.Key);
                if (conversations.ContainsKey(conversationID))
                {
                    for (int i = 0; i < conversations[conversationID].Count; ++i)
                    {
                        if (conversations[conversationID][i].ID == e.Snapshot.Key)
                            conversations[conversationID][i] = chatMessage;
                    }
                }
                ChatMessageChanged?.Invoke(conversationID, chatMessage);
            }
        }

        private void OnChatMessageRemoved(object sender, ChildChangedEventArgs e)
        {
            if (e.Snapshot != null && e.Snapshot.Exists)
            {
                string conversationID = e.Snapshot.Reference.Parent.Parent.Key;
                ChatMessage chatMessage = JsonConvert.DeserializeObject<ChatMessage>(e.Snapshot.GetRawJsonValue());
                chatMessage.SetID(e.Snapshot.Key);
                if (conversations.ContainsKey(conversationID))
                {
                    for (int i = 0; i < conversations[conversationID].Count; ++i)
                    {
                        if (conversations[conversationID][i].ID == e.Snapshot.Key)
                            conversations[conversationID].Remove(conversations[conversationID][i]);
                    }
                }
                ChatMessagedRemoved?.Invoke(conversationID, chatMessage);
            }
        }

        private void OnProfileDataValueChanged(object sender, ValueChangedEventArgs e)
        {
            if (e.Snapshot != null && e.Snapshot.Exists)
                ProfileData = JsonConvert.DeserializeObject<ProfileData>(e.Snapshot.GetRawJsonValue());
        }

        /*
        public async Task<ChatMessage[]> GetChatMessages(string userID)
        {
            List<ChatMessage> chatMessages = new List<ChatMessage>();
            DataSnapshot chatMessagesSnapshots = await usersReference.Child(userID + "/Messages/").GetValueAsync();
            foreach (DataSnapshot chatMessageSnapshot in chatMessagesSnapshots.Children)
            {
                ChatMessage chatMessage = JsonConvert.DeserializeObject<ChatMessage>(chatMessageSnapshot.GetRawJsonValue());
                chatMessage.SetID(chatMessageSnapshot.Key);
                chatMessages.Add(chatMessage);
            }
            return chatMessages.ToArray();
        }
        */

        // this is just a test normally the server will do this instead of the client; and the server needs to send both at the same time
        // not one after the other like i am doing here.
        public void SendChatMessage(ChatMessage message)
        {
            switch(message.Type)
            {
                case MessageType.Text:
                    UploadChatMessage(message);
                    break;
                default:
                    string uniqueFileName = Guid.NewGuid().ToString() + Path.GetFileName(message.Content);
                    string destinationFilePath = "Users/" + uniqueFileName;
                    DatabaseHelper.Storage.UploadFile(message.Content, destinationFilePath, null, (url) =>
                    {
                        message.SetContent(url);
                        UploadChatMessage(message);
                    });
                    break;
            }

        }

        private void UploadChatMessage(ChatMessage message)
        {
            //Create sender message
            DatabaseReference messageIDReference = usersReference.Child(message.SenderID + "/Conversations/" + message.ReceiverID + "/Messages/").Push();
            string messageID = messageIDReference.Key;
            message.SetID(messageID);
            messageIDReference.SetRawJsonValueAsync(JsonConvert.SerializeObject(message)).ContinueWith((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("SendChatMessageAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("SendChatMessageAsync encountered an error: " + task.Exception);
                else
                    Debug.Log("Message has been sent by " + message.SenderID);
            });
            //Create receiver message
            messageIDReference = usersReference.Child(message.ReceiverID + "/Conversations/" + message.SenderID + "/Messages/").Child(messageID);
            messageIDReference.SetRawJsonValueAsync(JsonConvert.SerializeObject(message)).ContinueWith((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("SendChatMessageAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("SendChatMessageAsync encountered an error: " + task.Exception);
                else
                    Debug.Log("Message has been sent to " + message.ReceiverID);
            });
        }

        public void UploadProfileData(string userID, ProfileData profileData, Action callback)
        {
            if (!HasProfileDataChanged(profileData))
                callback();
            else
            {
                DatabaseReference profileDataReference = usersReference.Child(userID + "/ProfileData/").Reference;
                profileDataReference.SetRawJsonValueAsync(JsonConvert.SerializeObject(profileData)).ContinueWithOnMainThread((task) =>
                {
                    if (task.IsCanceled)
                        Debug.LogError("UploadProfileDataAsync was canceled.");
                    else if (task.IsFaulted)
                        Debug.LogError("UploadProfileDataAsync encountered an error: " + task.Exception);
                    else
                    {
                        Debug.Log("Profile has been uploaded of " + userID);
                        callback();
                    }
                });
            }
        }

        private bool HasProfileDataChanged(ProfileData profileData)
        {
            if (ProfileData == null)
                return true;
            if (ProfileData.FirstName != profileData.FirstName ||
                ProfileData.LastName != profileData.LastName ||
                ProfileData.Description != profileData.Description ||
                ProfileData.PhotoRemotePath != profileData.PhotoRemotePath)
            {
                return true;
            }
            return false;
        }

        public void GetProfileData(string userID, Action<ProfileData> callback)
        {
            if (userID == DatabaseHelper.User.UserID)
                callback(ProfileData);
            else
                DownloadProfileData(userID, callback);
        }

        private void DownloadProfileData(string userID, Action<ProfileData> callback)
        {
            DatabaseReference profileDataReference = usersReference.Child(userID + "/ProfileData/").Reference;
            profileDataReference.GetValueAsync().ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("DownloadProfileDataAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("DonwloadProfileDataAsync encountered an error: " + task.Exception);
                else
                {
                    Debug.Log("Profile has been downloaded of " + userID);
                    if (task.Result != null && task.Result.Exists)
                    {
                        ProfileData profileData = JsonConvert.DeserializeObject<ProfileData>(task.Result.GetRawJsonValue());
                        profileData.SetUserID(userID);
                        callback(profileData);
                    }
                    else
                        callback(null);
                }
            });
        }


        public Dictionary<string, List<ChatMessage>> LoadConversations()
        {
            try
            {
                return conversations.OrderByDescending(c => c.Value.Last().PostDate).ToDictionary(c => c.Key, c => c.Value);
            }
            catch
            {
                return conversations;
            }
        }

        public ChatMessage[] GetChatMessages(string conversationID)
        {
            if (conversations.ContainsKey(conversationID))
                return conversations[conversationID].ToArray();
            return new ChatMessage[0];
        }

        public void RefreshConversations()
        {
            conversationsReference.ChildAdded -= OnConversationAdded;
            conversationsReference.ChildAdded += OnConversationAdded;
        }
    }
}
