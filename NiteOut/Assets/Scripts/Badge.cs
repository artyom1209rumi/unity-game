using UnityEngine;

[CreateAssetMenu(fileName = "New Badge", menuName = "Badges/New Badge", order = 0)]
public class Badge : ScriptableObject
{
    [SerializeField]
    private int id;
    [SerializeField]
    private string title;
    [SerializeField]
    private Sprite sprite;

    public int ID { get { return id; } }

    public string Title { get { return title; } }

    public Sprite Sprite { get { return sprite; } }
}
