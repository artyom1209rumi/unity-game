﻿namespace NiteOut.Model
{
    public static class DatabaseHelper
    {
        public static IUser User { get; private set; }
        public static IAuthenticator Authenticator { get; private set; }
        public static IDatabase Database { get; private set; }
        public static IStorage Storage { get; private set; }

        public static void Initialize(IAuthenticator authenticator)
        {
            Authenticator = authenticator;
            if (authenticator is FirebaseAuthenticator firebaseAuthenticator)
            {
                User = new FirebaseUser(firebaseAuthenticator.GetFirebaseAuth());
                Database = new FirebaseDatabase();
                Storage = new FirebaseStorage();
            }
        }
    }
}
