﻿using Firebase.Storage;
using System;
using UnityEngine;
using Firebase.Extensions;
using System.IO;
using UnityEngine.Networking;
using System.Collections;

namespace NiteOut.Model
{
    public class FirebaseStorage : IStorage
    {
        public event Action<string> FileUploaded;
        public event Action<byte[]> FileDownloaded;
        public event Action<string> FileDeleted;
        private readonly StorageReference rootReference;
        private readonly StorageReference usersReference;

        public FirebaseStorage()
        {
            rootReference = Firebase.Storage.FirebaseStorage.DefaultInstance.RootReference;
            usersReference = rootReference.Child("Users/");
        }

        public void UploadFile(string filePath, string destinationFilePath, StorageProgress<UploadState> progress = null, Action<string> completedCallback = null)
        {
            StreamReader stream = new StreamReader(filePath);
            rootReference.Child(destinationFilePath).PutStreamAsync(stream.BaseStream, null, progress).ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                {
                    Debug.Log("UploadFileAsync was canceled.");
                }
                else if (task.IsFaulted)
                {
                    Debug.Log("UploadFileAsync encountered an error: " + task.Exception);
                }
                else
                {
                    Debug.Log("File has been uploaded " + task.Result.Path);
                    completedCallback?.Invoke(task.Result.Path);
                    FileUploaded?.Invoke(task.Result.Path);
                }
                stream.Close();
            });
        }

        public void UploadFile(byte[] fileBytes, string destinationFilePath, StorageProgress<UploadState> progress = null, Action<string> completedCallback = null)
        {
            string cachedLocalDirectoryPath = Path.Combine(Application.persistentDataPath, DatabaseHelper.User.UserID);
            if (!Directory.Exists(cachedLocalDirectoryPath))
                Directory.CreateDirectory(cachedLocalDirectoryPath);
            string cachedLocalFilePath = Path.Combine(cachedLocalDirectoryPath, destinationFilePath.Remove(0, 6 + DatabaseHelper.User.UserID.Length + 1));
            File.WriteAllBytes(cachedLocalFilePath, fileBytes);
            rootReference.Child(destinationFilePath).PutBytesAsync(fileBytes, null, progress).ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("UploadFileAsync was canceled.");
                }
                else if (task.IsFaulted)
                {
                    Debug.LogError("UploadFileAsync encountered an error: " + task.Exception);
                }
                else
                {
                    Debug.Log("File has been uploaded " + task.Result.Path);
                    completedCallback?.Invoke(task.Result.Path);
                    FileUploaded?.Invoke(task.Result.Path);
                }
            });
        }

        public void GetRemoteFileURL(string destinationFilePath, Action<string> completedCallback)
        {
            rootReference.Child(destinationFilePath).GetDownloadUrlAsync().ContinueWithOnMainThread((task) =>
            {
                Debug.Log("Got File URL : " + task.Result.AbsoluteUri);
                completedCallback?.Invoke(task.Result.AbsoluteUri);
            });
        }
        /*
        public void UploadFile(string filePath, string destinationFilePath, Action<string> completedCallback = null)
        {
            rootReference.Child(destinationFilePath).PutFileAsync(filePath, null).ContinueWith((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("UploadFileAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("UploadFileAsync encountered an error: " + task.Exception);
                else
                {
                    rootReference.Child(destinationFilePath).GetDownloadUrlAsync().ContinueWithOnMainThread((task2) =>
                    {
                        Debug.Log("File has been uploaded " + task2.Result.AbsolutePath);
                        completedCallback?.Invoke(task2.Result.AbsolutePath);
                        FileUploaded?.Invoke(task2.Result.AbsolutePath);
                    });
                }
            });
        }
        */

        /*
        public void DownloadFile(string url, Action<byte[]> completedCallback)
        {
            const long maxAllowedSize = 1 * 1024 * 1024;
            rootReference.Child(url).GetBytesAsync(maxAllowedSize).ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.LogError("DownloadFileAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.LogError("DownloadFileAsync encountered an error: " + task.Exception);
                else
                {
                    Debug.Log("File has been downloaded " + url);
                    completedCallback?.Invoke(task.Result);
                    FileDownloaded?.Invoke(task.Result);
                }
            });
        }
        */

        public void DownloadFile(string userID, string remoteFilePath, Action<byte[]> completedCallback)
        {
            string cachedLocalDirectoryPath = Path.Combine(Application.persistentDataPath, userID);
            string cachedLocalFilePath = Path.Combine(cachedLocalDirectoryPath, remoteFilePath.Remove(0, 6 + userID.Length + 1));
            if (File.Exists(cachedLocalFilePath))
            {
                byte[] imageBytes = File.ReadAllBytes(cachedLocalFilePath);
                completedCallback?.Invoke(imageBytes);
                FileDownloaded?.Invoke(imageBytes);
            }
            else
            {

                const long maxAllowedSize = 1 * 1024 * 1024;
                rootReference.Child(remoteFilePath).GetBytesAsync(maxAllowedSize).ContinueWithOnMainThread((task) =>
                {
                    if (task.IsCanceled)
                        Debug.Log("DownloadFileAsync was canceled.");
                    else if (task.IsFaulted)
                        Debug.Log("DownloadFileAsync encountered an error: " + task.Exception);
                    else
                    {
                        Debug.Log("File has been downloaded " + remoteFilePath);
                        if (!Directory.Exists(cachedLocalDirectoryPath))
                            Directory.CreateDirectory(cachedLocalDirectoryPath);
                        File.WriteAllBytes(cachedLocalFilePath, task.Result);
                        completedCallback?.Invoke(task.Result);
                        FileDownloaded?.Invoke(task.Result);
                    }
                });
            }
        }

        public void DeleteFile(string remoteFilePath, Action<string> completedCallback)
        {
            rootReference.Child(remoteFilePath).DeleteAsync().ContinueWithOnMainThread((task) =>
            {
                if (task.IsCanceled)
                    Debug.Log("DeleteFileAsync was canceled.");
                else if (task.IsFaulted)
                    Debug.Log("DeleteFileAsync encountered an error: " + task.Exception);
                else
                {
                    Debug.Log("File has been deleted " + remoteFilePath);
                    completedCallback?.Invoke(remoteFilePath);
                    FileDeleted?.Invoke(remoteFilePath);
                }
            });
        }

        private IEnumerator DownloadFileCoroutine(string url, Action<byte[]> completedCallback)
        {
            UnityWebRequest downloadFileRequest = UnityWebRequest.Get(url);
            yield return downloadFileRequest.SendWebRequest();
        }
    }
}
