﻿using UnityEngine;
using UnityEngine.UI;
using NiteOut.Model;
using System;
using System.Collections;
using AdvancedInputFieldPlugin;
using TMPro;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace NiteOut.View
{
    public class ChatUI : MonoBehaviour
    {
        public static ChatUI Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<ChatUI>();
                return instance;
            }
        }

        public string CurrentConversationID { get { return currentConversationID; } }
        [Header("Chat Panel")]
        //[SerializeField]
        private Color senderTextColor = /*new Color(0.3499911f, 0.6132076f, 0.5468594f);*/Color.white;
        //[SerializeField]
        private Color receiverTextColor = /*Color.grey;*/Color.white;
        [SerializeField]
        private Sprite senderSpriteNormal, senderSpriteEdge;
        [SerializeField]
        private Sprite receiverSpriteNormal, receiverSpriteEdge;
        [SerializeField]
        private AdvancedInputField chatInputField;
        private string lastPickedImagePath;
        [SerializeField]
        private RectTransform chatContentParent;
        [SerializeField]
        private Transform chatSenderContentPrefab, chatReceiverContentPrefab;
        [SerializeField]
        private GameObject chatsPanel;
        [SerializeField]
        private GameObject messagesPanel;
        [SerializeField]
        private TextMeshProUGUI noConversationText;
        [SerializeField]
        private Transform horizontalChatPrefab;
        [SerializeField]
        private Transform verticalChatPrefab;
        [SerializeField]
        private Transform horizontalChatContent;
        [SerializeField]
        private Transform verticalChatContent;
        [SerializeField]
        private Animator chatPanelAnimator;
        [SerializeField]
        private GameObject barUICanvas;
        [SerializeField]
        private ScrollRect chatScrollText;
        //[SerializeField]
        //private GameObject chatBodyPanel;

        private static ChatUI instance;

        private IUser user;
        private IDatabase database;
        private IStorage storage;

        private string currentConversationID; // the conversation we currently selected

        private Vector2 lastDragPosition;

        private Dictionary<string, Transform> spawnedConversations = new Dictionary<string, Transform>();
        private Dictionary<string, Transform> spawnedChats = new Dictionary<string, Transform>();
        private RectTransform lastTargetContentInstance;
        private DateTime lastMessageDateTime;
        private string lastMessageSender, nextMessageSender;
        private Image lastSelectedConversationImage; // boarder image
        private bool isComingFromChat;

        private void Start()
        {
            instance = this;
            //GalleryPicker.OnImagePicked += OnImagePicked;
            //GalleryPicker.OnVideoPicked += OnVideoPicked;
            user = DatabaseHelper.User;
            database = DatabaseHelper.Database;
            storage = DatabaseHelper.Storage;
            //storage.FileUploaded += OnChatFileUploaded;
            database.ConversationAdded += OnConversationAdded;
            database.ConversationRemoved += OnConversationRemoved;
            database.ChatMessageAdded += OnChatMessageAdded;
            database.ChatMessageChanged += OnChatMessageChanged;
            database.ChatMessagedRemoved += OnChatMessageRemoved;
            lastMessageDateTime = DateTime.MinValue;
            gameObject.SetActive(false);
            chatPanelAnimator.keepAnimatorControllerStateOnDisable = true;
            //database.ListenToMessages();
            //user.SendChatMessage("AbbesID", "Hello");
        }

        private void Update()
        {
            /*
            if (Input.GetKeyDown(KeyCode.R))
            {
                ChatMessage chatMessage = new ChatMessage("AbbesID", user.UserID, "This is a test remote message", MessageType.Text);
                DatabaseHelper.Database.SendChatMessage(chatMessage);
            }
            */
        }

        private void OnDestroy()
        {
            //GalleryPicker.OnImagePicked -= OnImagePicked;
            //GalleryPicker.OnVideoPicked -= OnVideoPicked;
            //storage.FileUploaded -= OnChatFileUploaded;
            database.ConversationAdded -= OnConversationAdded;
            database.ConversationRemoved -= OnConversationRemoved;
            database.ChatMessageChanged -= OnChatMessageAdded;
            database.ChatMessagedRemoved -= OnChatMessageRemoved;
            database.Dispose();
        }

        public void ShowRedDot(string conversationID, bool show)
        {
            if (spawnedChats.ContainsKey(conversationID))
                spawnedChats[conversationID].Find("ChatElementPanel").Find("NotificationImage").GetComponent<Image>().enabled = show;
        }

        /*
        public void HighlightSelectedConversation(string conversationID)
        {
            if (spawnedConversations.ContainsKey(conversationID))
                ImageSelecter.Select(ref lastSelectedConversationImage, spawnedConversations[conversationID].Find("ProfileMask").Find("ConversationSelectionImage").GetComponent<Image>());
        }
        */

        public bool IsAnyRedDotShown()
        {
            foreach (Transform spawnedChat in spawnedChats.Values)
            {
                if (spawnedChat.Find("ChatElementPanel").Find("NotificationImage").GetComponent<Image>().enabled)
                    return true;
            }
            return false;
        }

        private void OnConversationAdded(string conversationID)
        {
            LoadChats();
        }

        private void OnConversationRemoved(string conversationID)
        {
            LoadChats();
        }

        private void ClearMessages()
        {
            foreach (Transform message in chatContentParent)
                Destroy(message.gameObject);
            lastMessageDateTime = DateTime.MinValue;
        }

        public void OpenConversation(string UUID, ProfileData profileData)
        {
            database.OpenConversationWith(UUID, () =>
            {
                currentConversationID = UUID;
                barUICanvas.SetActive(false);
                gameObject.SetActive(true);
                chatsPanel.SetActive(false);
                messagesPanel.SetActive(true);
                OnConversationClicked(UUID);
                //OpenChat();
                //ClearConversations();
            });
        }

        public void OpenChat()
        {
            //database.ListenToConversations();
            gameObject.SetActive(true);
            StopAllCoroutines();
            StartCoroutine(OpenChatCoroutine());
        }

        public void CloseChat()
        {
            chatScrollText.enabled = false;
            StopAllCoroutines();
            StartCoroutine(CloseChatCoroutine());
        }

        private IEnumerator OpenChatCoroutine()
        {
            LoadChats();
            chatsPanel.SetActive(true);
            messagesPanel.SetActive(false);
            if (isComingFromChat)
                isComingFromChat = false;
            else
            {
                chatPanelAnimator.SetBool("IsSlidingIn", true);
                yield return new WaitForSeconds(.167f);
            }
            if (string.IsNullOrEmpty(currentConversationID))
            {
                print("You have no conversations, try to initiate one");
                yield break;
            }
            //LoadMessages(currentConversationID);
            /*
            DatabaseHelper.Database.MarkConversationAsRead(currentConversationID, () =>
            {
                UpdateRedDots(currentConversationID);
                LoadMessages(currentConversationID);
            });
            */
        }

        public IEnumerator CloseChatCoroutine()
        {
            chatPanelAnimator.SetBool("IsSlidingIn", false);
            //if (TestLayout.Instance.IsKeyboardOpen)
                // this needs to be moved to the messages panel
                //TestLayout.Instance.ScrollBackChat();
            yield return new WaitForSeconds(.167f);
            //if (TestLayout.Instance.IsKeyboardOpen)
                //TestLayout.Instance.OnEndEdit(chatInputField.Text, EndEditReason.KEYBOARD_DONE);
            //barUICanvas.GetComponent<BarUI>().ShowRedDot(false);
            // this is not optimized
            barUICanvas.GetComponent<BarUI>().ShowRedDot(IsAnyRedDotShown());
            chatScrollText.enabled = true;
            barUICanvas.SetActive(true);
            gameObject.SetActive(false);
        }

        private void LoadChats()
        {
            spawnedConversations.Clear();
            spawnedChats.Clear();
            foreach (Transform conversation in horizontalChatContent)
                Destroy(conversation.gameObject);
            foreach (Transform conversation in verticalChatContent)
                Destroy(conversation.gameObject);
            currentConversationID = null;

            Dictionary<string, List<ChatMessage>> conversations = database.LoadConversations();
            noConversationText.enabled = conversations.Count == 0; // when there are no conversations show the message
            foreach (var conversation in conversations)
            {
                /*
                if (string.IsNullOrEmpty(currentConversationID))
                    currentConversationID = conversation.Key;
                */
                LoadConversation(conversation.Key);
                UpdateRedDots(conversation.Key);
            }
            /*
            foreach (Transform conversation in spawnedConversations.Values)
                conversation.SetAsFirstSibling();
            if (!string.IsNullOrEmpty(currentConversationID))
                spawnedConversations[currentConversationID].SetAsFirstSibling();
            */
        }

        private void UpdateRedDots(string conversationID)
        {
            foreach (ChatMessage chatMessage in DatabaseHelper.Database.GetChatMessages(conversationID))
            {
                if (chatMessage.SenderID != DatabaseHelper.User.UserID && !chatMessage.HasRead /*&& currentConversationID != conversationID*/)
                    ShowRedDot(conversationID, true);
                else
                    ShowRedDot(conversationID, false);
            }
        }

        private void RefreshConversation(string conversationID)
        {
            if (spawnedConversations.ContainsKey(conversationID))
            {
                Destroy(spawnedConversations[conversationID].gameObject);
                spawnedConversations.Remove(conversationID);
            }
            if (spawnedChats.ContainsKey(conversationID))
            {
                Destroy(spawnedChats[conversationID].gameObject);
                spawnedChats.Remove(conversationID);
            }
            LoadConversation(conversationID);
            UpdateRedDots(conversationID);
        }

        private void LoadConversation(string conversationID)
        {
            // load horizontal chat content
            Transform horizontalChatInstance = Instantiate(horizontalChatPrefab, horizontalChatContent, false);
            // load vertical chat content
            Transform verticalChatInstance = Instantiate(verticalChatPrefab, verticalChatContent, false);
            horizontalChatInstance.name = conversationID;
            verticalChatInstance.name = conversationID;
            Transform horizontalProfileMask = horizontalChatInstance.Find("ProfileMask");
            RawImage horizontalProfileRawImage = horizontalProfileMask.Find("ProfileRawImage").GetComponent<RawImage>();
            RectTransform verticalChatInstancePanel = verticalChatInstance.Find("ChatElementPanel") as RectTransform;
            RectTransform verticalProfileMask = verticalChatInstancePanel.Find("ProfileMask") as RectTransform;
            RawImage verticalProfileRawImage = verticalProfileMask.Find("ProfileRawImage").GetComponent<RawImage>();
            RectTransform profileInfo = verticalChatInstancePanel.Find("ProfileInfo") as RectTransform;
            RectTransform timeRectTransform = verticalChatInstancePanel.Find("TimeText") as RectTransform;
            //StopCoroutine(AdjustChatProfileInfoSize(verticalChatInstance as RectTransform, verticalProfileMask, profileInfo, timeRectTransform));
            //StartCoroutine(AdjustChatProfileInfoSize(verticalChatInstance as RectTransform, verticalProfileMask, profileInfo, timeRectTransform));
            DatabaseHelper.Database.GetProfileData(conversationID, (profileData) =>
            {
                if (profileData != null && !string.IsNullOrEmpty(profileData.PhotoRemotePath))
                {
                    DatabaseHelper.Storage.DownloadFile(profileData.UserID, profileData.PhotoRemotePath, (imageBytes) =>
                    {
                        horizontalProfileRawImage.texture = TextureLoader.LoadFromBytes(imageBytes);
                        horizontalProfileRawImage.SizeToParent();
                        verticalProfileRawImage.texture = TextureLoader.LoadFromBytes(imageBytes);
                        verticalProfileRawImage.SizeToParent();
                    });
                }
                if (profileData != null)
                {
                    profileInfo.Find("NameText").GetComponent<TextMeshProUGUI>().text = profileData.FirstName + " " + profileData.LastName;
                    ChatMessage[] chatMessages = database.GetChatMessages(conversationID);
                    if (chatMessages.Length > 0)
                    {
                        ChatMessage lastMessage = chatMessages[chatMessages.Length - 1];
                        profileInfo.Find("LastMessageText").GetComponent<TextMeshProUGUI>().text = lastMessage.Content;
                        timeRectTransform.GetComponent<TextMeshProUGUI>().text = lastMessage.PostDate.ToShortTimeString();
                        timeRectTransform.GetComponent<TextMeshProUGUI>().color = DatabaseHelper.Database.IsConversationRead(conversationID) ? new Color(0.3764706f, 0.454902f, 0.4705882f) : new Color(0.4627451f, 0.3215686f, 0.9921569f);
                    }
                }
            });

            // get remote path from user id
            /*
            DatabaseHelper.Storage.DownloadFile(profileData.PhotoRemotePath, (imageBytes) =>
            {
                profileRawImage.texture = TextureLoader.LoadFromBytes(imageBytes);
            });
            */
            if (!spawnedConversations.ContainsKey(conversationID))
                spawnedConversations.Add(conversationID, horizontalChatInstance);
            if (!spawnedChats.ContainsKey(conversationID))
                spawnedChats.Add(conversationID, verticalChatInstance);
            Button horizontalButton = horizontalProfileRawImage.GetComponent<Button>();
            horizontalButton.onClick.RemoveAllListeners();
            horizontalButton.onClick.AddListener(() =>
            {
                OnConversationClicked(conversationID);
            });
            Button verticalButton = verticalChatInstance.GetComponent<Button>();
            verticalButton.onClick.RemoveAllListeners();
            verticalButton.onClick.AddListener(() =>
            {
                OnConversationClicked(conversationID);
            });
            OrderConversations();
        }

        private ChatMessage GetNextMessage(string conversationID, ChatMessage currentMessage)
        {
            ChatMessage[] chatMessages = DatabaseHelper.Database.GetChatMessages(conversationID);
            for(int i = 0; i < chatMessages.Length; ++i)
            {
                ChatMessage chatMessage = chatMessages[i];
                if (chatMessage.ID == currentMessage.ID)
                {
                    int nextMessageIndex = i + 1;
                    if (nextMessageIndex < chatMessages.Length)
                        return chatMessages[nextMessageIndex];
                }
            }
            return null;
        }

        private ChatMessage GetPreviousMessage(string conversationID, ChatMessage currentMessage)
        {
            ChatMessage[] chatMessages = DatabaseHelper.Database.GetChatMessages(conversationID);
            for (int i = 0; i < chatMessages.Length; ++i)
            {
                ChatMessage chatMessage = chatMessages[i];
                if (chatMessage.ID == currentMessage.ID)
                {
                    int previousMessageIndex = i - 1;
                    if (previousMessageIndex >= 0)
                        return chatMessages[previousMessageIndex];
                }
            }
            return null;
        }

        private void OnConversationClicked(string conversationID)
        {
            DatabaseHelper.Database.MarkConversationAsRead(conversationID, () =>
            {
                messagesPanel.SetActive(true);
                chatsPanel.SetActive(false);
                currentConversationID = conversationID;
                //chatBodyPanel.SetActive(true);
                LoadMessages(conversationID);
                ShowRedDot(conversationID, false);
                //database.ListenToMessages(conversationID);
                Transform headerPanel = messagesPanel.transform.Find("HeaderPanel");
                DatabaseHelper.Database.GetProfileData(conversationID, (profileData) =>
                {
                    if (profileData != null && !string.IsNullOrEmpty(profileData.PhotoRemotePath))
                    {
                        DatabaseHelper.Storage.DownloadFile(profileData.UserID, profileData.PhotoRemotePath, (imageBytes) =>
                        {
                            RawImage profileRawImage = headerPanel.Find("ProfilePanel").Find("ProfileMask").Find("ProfileRawImage").GetComponent<RawImage>();
                            profileRawImage.texture = TextureLoader.LoadFromBytes(imageBytes);
                            profileRawImage.SizeToParent();
                        });
                    }
                    if (profileData != null)
                        headerPanel.Find("NameText").GetComponent<TextMeshProUGUI>().text = profileData.FirstName + " " + profileData.LastName;
                });
            });
        }

        private IEnumerator AdjustChatProfileInfoSize(RectTransform verticalChatInstance, RectTransform verticalProfileMask, RectTransform profileInfo, RectTransform timeRectTransform)
        {
            yield return new WaitForEndOfFrame();
            try
            {
                float profileInfoWidth = verticalChatInstance.rect.width - timeRectTransform.rect.width - verticalProfileMask.rect.width - 29 - 18;
                profileInfo.sizeDelta = new Vector2(profileInfoWidth, profileInfo.sizeDelta.y);
            }
            catch { }
        }

        private void LoadMessages(string conversationID)
        {
            ClearMessages();
            foreach (ChatMessage message in database.GetChatMessages(conversationID))
                OnChatMessageAdded(conversationID, message);
            /*
            if (!string.IsNullOrEmpty(currentConversationID))
                HighlightSelectedConversation(currentConversationID);
            */
            //OrderConversations();
        }

        private void OrderConversations()
        {
            int index = 0;
            foreach (string conversationID in DatabaseHelper.Database.LoadConversations().Keys)
            {
                print("CI : " + conversationID);
                if (spawnedConversations.ContainsKey(conversationID))
                    spawnedConversations[conversationID].SetSiblingIndex(index);
                if (spawnedChats.ContainsKey(conversationID))
                    spawnedChats[conversationID].SetSiblingIndex(index);
                index++;
            }
            print("Ordering conversations");
        }

        private string GetElapsedTime(DateTime dateTime)
        {
            TimeSpan TS = DateTime.Now - dateTime;
            int intYears = DateTime.Now.Year - dateTime.Year;
            int intMonths = DateTime.Now.Month - dateTime.Month;
            int intDays = DateTime.Now.Day - dateTime.Day;
            int intHours = DateTime.Now.Hour - dateTime.Hour;
            int intMinutes = DateTime.Now.Minute - dateTime.Minute;
            int intSeconds = DateTime.Now.Second - dateTime.Second;
            if (intYears > 0) return String.Format("{0} {1} ago", intYears, (intYears == 1) ? "year" : "years");
            else if (intMonths > 0) return String.Format("{0} {1} ago", intMonths, (intMonths == 1) ? "month" : "months");
            else if (intDays > 0) return String.Format("{0} {1} ago", intDays, (intDays == 1) ? "day" : "days");
            else if (intHours > 0) return String.Format("{0} {1} ago", intHours, (intHours == 1) ? "hour" : "hours");
            else if (intMinutes > 0) return String.Format("{0} {1} ago", intMinutes, (intMinutes == 1) ? "minute" : "minutes");
            else if (intSeconds > 0) return String.Format("{0} {1} ago", intSeconds, (intSeconds == 1) ? "second" : "seconds");
            else
            {
                return String.Format("{0} {1} ago", dateTime.ToShortDateString(), dateTime.ToShortTimeString());
            }
        }

        private void OnChatMessageAdded(string conversationID, ChatMessage chatMessage)
        {
            if (currentConversationID == conversationID) // make sure we add the right chat message into the right conversation
            {
                print("chat message has been added : " + chatMessage.Content);
                Transform targetContentPrefab = chatMessage.SenderID == user.UserID ? chatSenderContentPrefab : chatReceiverContentPrefab;
                RectTransform targetContentInstance = Instantiate(targetContentPrefab, chatContentParent, false) as RectTransform;
                targetContentInstance.name = chatMessage.SenderID;
                RectTransform dateTextTransform = targetContentInstance.Find("DateText") as RectTransform;
                //StopAllCoroutines();
                StartCoroutine(AdjectTextAndDateWidthContent(targetContentInstance, dateTextTransform, conversationID, chatMessage));
                switch (chatMessage.Type)
                {
                    case MessageType.Text:
                        if (chatMessage.PostDate.Day != lastMessageDateTime.Day)
                        {
                            TextMeshProUGUI dateText = dateTextTransform.GetComponent<TextMeshProUGUI>();
                            dateText.enabled = true;
                            dateText.text = chatMessage.PostDate.ToString("ddd") + " " + chatMessage.PostDate.ToShortDateString();
                            lastMessageDateTime = chatMessage.PostDate;
                        }
                        /*
                        if (chatMessage.SenderID == user.UserID)
                            isLastMessageSender = true;
                        else
                            isLastMessageSender = false;
                        */
                        /*
                        DatabaseHelper.Database.GetProfileData(chatMessage.SenderID, (profileData) =>
                        {
                            if (profileData != null)
                                targetContentInstance.Find("ProfilePanel").Find("ProfileNameText").GetComponent<TextMeshProUGUI>().text = profileData.FirstName;
                        });
                        */
                        //targetContentInstance.Find("ProfilePanel").Find("ProfileNameText").GetComponent<TextMeshProUGUI>().text = chatMessage.SenderID == user.UserID ? DatabaseHelper.User.Username : Globals.GetNameFromUUID(chat).FirstName;
                        Transform textContent = targetContentInstance.Find("Content").Find("TextContent");
                        textContent.gameObject.SetActive(true);
                        textContent.GetComponent<Image>().sprite = chatMessage.SenderID == user.UserID ? senderSpriteNormal : receiverSpriteNormal;
                        //textContent.GetComponent<Image>().color = chatMessage.SenderID == user.UserID ? senderTextColor : receiverTextColor;
                        textContent.GetComponentInChildren<TextMeshProUGUI>().text = chatMessage.Content;
                        textContent.Find("TimePanel").Find("TimeText").GetComponent<TextMeshProUGUI>().text = chatMessage.PostDate.ToShortTimeString();
                        break;
                    case MessageType.Image:
                        Transform imageContent = targetContentInstance.Find("Content").Find("ImageContent");
                        imageContent.gameObject.SetActive(true);
                        storage.DownloadFile(conversationID, chatMessage.Content, (imageBytes) =>
                        {
                            Texture2D texture = new Texture2D(2, 2);
                            texture.LoadImage(imageBytes);
                            imageContent.GetComponent<RawImage>().texture = texture;
                        });
                        break;
                    case MessageType.Video:
                        Transform videoContent = targetContentInstance.Find("Content").Find("VideoContent");
                        videoContent.gameObject.SetActive(true);
                        VideoPlayer videoPlayer = videoContent.GetComponent<VideoPlayer>();
                        videoPlayer.url = chatMessage.Content;
                        Button videoPlayButton = videoContent.Find("PlayButton").GetComponent<Button>();
                        print("We need to render the thumbnail first");
                        videoPlayButton.onClick.RemoveAllListeners();
                        videoPlayButton.onClick.AddListener(() =>
                        {
                            print("Playing Video");
                            videoPlayer.Play();
                        });
                        storage.DownloadFile(conversationID, chatMessage.Content, (videoBytes) =>
                        {
                            Texture2D texture = new Texture2D(2, 2);
                            texture.LoadImage(videoBytes);
                        //imageContent.GetComponent<RawImage>().texture = texture;
                    });
                        break;
                }
            }
            RefreshConversation(conversationID);
            //OrderConversations();
        }

        private void OnChatMessageChanged(string conversationID, ChatMessage chatMessage)
        {
            print("chat message has been changed to : " + chatMessage.Content);
        }

        private void OnChatMessageRemoved(string conversationID, ChatMessage chatMessage)
        {
            print(chatMessage.Content + " has been removed");
        }

        private IEnumerator AdjectTextAndDateWidthContent(RectTransform targetContentInstance, RectTransform dateTextTransform, string conversationID, ChatMessage chatMessage)
        {
            yield return new WaitForEndOfFrame();
            try
            {
                VerticalLayoutGroup targetContentVerticalLayoutGroup = chatContentParent.GetComponent<VerticalLayoutGroup>();
                float targetContentWidth = chatContentParent.rect.width - targetContentVerticalLayoutGroup.padding.left - targetContentVerticalLayoutGroup.padding.right;
                targetContentInstance.sizeDelta = new Vector2(targetContentWidth, targetContentInstance.sizeDelta.y);
                dateTextTransform.sizeDelta = new Vector2(targetContentWidth, dateTextTransform.sizeDelta.y);
                if (lastMessageSender != chatMessage.SenderID) 
                    lastMessageSender = chatMessage.SenderID;
                else if(!dateTextTransform.GetComponent<TextMeshProUGUI>().enabled) // if the last message sender is the same as the current sender excluding the first message and it is on the same date then have less spacing between the message because they belong to the same person
                    targetContentInstance.GetComponent<VerticalLayoutGroup>().spacing = -20f;
                ChatMessage nextMessage = GetNextMessage(conversationID, chatMessage);
                if (nextMessage == null || nextMessage.SenderID != chatMessage.SenderID) // if this is the last message or the next message is sent by a different person then the current message should have an edge
                {
                    print("Last Chat Message is : " + chatMessage.ID);
                    targetContentInstance.Find("Content").Find("TextContent").GetComponent<Image>().sprite = chatMessage.SenderID == user.UserID ? senderSpriteEdge : receiverSpriteEdge;
                }
                if (nextMessage == null && chatContentParent.childCount > 0) // if this is the last message and not the only message then make sure the previous message does not have an edge
                {
                    Transform previousTargetContentInstance = chatContentParent.GetChild(targetContentInstance.GetSiblingIndex() - 1);
                    if (lastMessageSender == previousTargetContentInstance.name)
                    { 
                        previousTargetContentInstance.Find("Content").Find("TextContent").GetComponent<Image>().sprite = chatMessage.SenderID == user.UserID ? senderSpriteNormal : receiverSpriteNormal;
                        //if (!previousTargetContentInstance.Find("DateText").GetComponent<TextMeshProUGUI>().enabled)
                            //previousTargetContentInstance.GetComponent<VerticalLayoutGroup>().spacing = -20f;
                    }
                }
                LayoutRebuilder.ForceRebuildLayoutImmediate(chatContentParent);
            }
            catch { }
        }
        /*
        private async void LoadChats()
        {
            try
            {
                ChatMessage[] chatMessages = await user.GetChatMessages();
                foreach (ChatMessage chatMessage in chatMessages)
                {
                    print(chatMessage.Content + " posted on : " + chatMessage.PostDate);
                }
            }
            catch(Exception ex)
            {
                print(ex.Message);
            }
        }
        */

        public void OnChatBackButtonClicked()
        {
            isComingFromChat = true;
            StopAllCoroutines();
            StartCoroutine(CloseKeyboardIfOpenCoroutine());
            //OpenChat();
        }

        private IEnumerator CloseKeyboardIfOpenCoroutine()
        {
            TestLayout.Instance.ScrollBackChat();
            yield return new WaitForSeconds(.05f); // wait for the keybord animation before closing the chat
            OpenChat();
        }

        public void OnChatSendButtonClicked()
        {
            if (!string.IsNullOrEmpty(chatInputField.Text) && !string.IsNullOrEmpty(currentConversationID) && spawnedConversations.Count > 0)
            {
                user.SendChatMessage(currentConversationID, chatInputField.Text, MessageType.Text);
                chatInputField.Text = "";
            }
        }

        public void OnChatGalleryButtonClicked()
        {
            //GalleryPicker.PickImage(512);
            GalleryPicker.PickMixedMedia();
        }

        private void OnImagePicked(string filePath)
        {
            // make sure the chat is open
            print("Picked " + filePath);
            //user.SendChatMessage("AbbesID", filePath, MessageType.Image);
        }

        private void OnVideoPicked(string filePath)
        {
            print("Picked : " + filePath);
            user.SendChatMessage("AbbesID", filePath, MessageType.Video);
        }

        private void OnChatFileUploaded(string filePath)
        {
            print("File Uploaded : " + filePath);
        }

        public void OnPanelBeginDrag(BaseEventData eventData)
        {
            lastDragPosition = ((PointerEventData)eventData).position;
        }

        public void OnPanelEndDrag(BaseEventData eventData)
        {
            //TestLayout.Instance.OnEndEdit(chatInputField.Text, EndEditReason.KEYBOARD_DONE);
            Vector2 currentDragPosition = ((PointerEventData)eventData).position;
            float distanceX = Mathf.Abs(currentDragPosition.x - lastDragPosition.x);
            float distanceY = Mathf.Abs(currentDragPosition.y - lastDragPosition.y);
            //We are wiping left so close the chat
            if (distanceX > distanceY)
            {
                if (currentDragPosition.x > lastDragPosition.x)
                {
                    lastDragPosition = currentDragPosition;
                    CloseChat();
                }
            }
        }
    }
}
