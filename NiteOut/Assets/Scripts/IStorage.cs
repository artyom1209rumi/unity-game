﻿using Firebase.Storage;
using System;

namespace NiteOut.Model
{
    public interface IStorage
    {
        public event Action<string> FileUploaded;
        public event Action<byte[]> FileDownloaded;
        public event Action<string> FileDeleted;

        void UploadFile(string filePath, string destinationFilePath, StorageProgress<UploadState> progress, Action<string> completedCallback);

        void UploadFile(byte[] fileBytes, string destinationFilePath, StorageProgress<UploadState> progress, Action<string> completedCallback);

        void DownloadFile(string userID, string remoteFilePath, Action<byte[]> completedCallback);

        void DeleteFile(string remoteFilePath, Action<string> completedCallback);

        void GetRemoteFileURL(string destinationFilePath, Action<string> completedCallback);
    }
}
