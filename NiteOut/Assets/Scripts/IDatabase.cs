﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiteOut.Model
{
    public interface IDatabase
    {
        event Action<string, ChatMessage> ChatMessageAdded;
        event Action<string, ChatMessage> ChatMessageChanged;
        event Action<string, ChatMessage> ChatMessagedRemoved;
        event Action<string> ConversationAdded;
        event Action<string> ConversationRemoved;
        ProfileData ProfileData { get; }

        void GetProfileData(string userID, Action<ProfileData> callback);

        //string LastConversationID { get; }

        void RefreshConversations();

        void MarkConversationAsRead(string conversationID, Action callback);

        void MarkMessageAsRead(string conversationID, string messageID, Action callback);

        public bool IsConversationRead(string conversationID);

        Dictionary<string, List<ChatMessage>> LoadConversations();

        ChatMessage[] GetChatMessages(string conversationID);

        void OpenConversationWith(string userID, Action callback);

        void OpenConversation(string fromUser, string toUser, Action callback);

        //void ListenToConversations();

        void ListenToMessages(string conversationID);

        void SendChatMessage(ChatMessage message);

        void UploadProfileData(string userID, ProfileData profileData, Action callback);
        //Task<ChatMessage[]> GetChatMessages(string userID);

        void Dispose();
    }
}
