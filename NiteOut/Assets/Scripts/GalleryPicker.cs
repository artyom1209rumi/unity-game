﻿using System;
using UnityEngine;

public static class GalleryPicker
{
	public static event Action<string> OnImagePicked;
	public static event Action<string> OnVideoPicked;
	public static event Action<string> OnAudioPicked;

	public static void PickMedia(NativeGallery.MediaType mediaType)
    {
		switch(mediaType)
        {
			case NativeGallery.MediaType.Image:
				PickImage(512);
				break;
			case NativeGallery.MediaType.Video:
				PickVideo();
				break;
			case NativeGallery.MediaType.Audio:
				PickAudio();
				break;
        }
    }

	public static void PickImage(int maxSize)
	{
		NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				/*
				// Create Texture from selected image
				Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					return;
				}*/
				OnImagePicked?.Invoke(path);
			}
		}, "Select a PNG image", "image/png");

		Debug.Log("Permission result: " + permission);
	}

	public static void PickVideo()
	{
		NativeGallery.Permission permission = NativeGallery.GetVideoFromGallery((path) =>
		{
			Debug.Log("Video path: " + path);
			if (path != null)
			{
				// Play the selected video
#if !UNITY_SERVER && !UNITY_STANDALONE_WIN
				Handheld.PlayFullScreenMovie("file://" + path);
#endif
				OnVideoPicked?.Invoke(path);
			}
		}, "Select a video");

		Debug.Log("Permission result: " + permission);
	}

	public static void PickAudio()
	{
		NativeGallery.Permission permission = NativeGallery.GetAudioFromGallery((path) =>
		{
			Debug.Log("Audio path: " + path);
			if (path != null)
				OnAudioPicked?.Invoke(path);
		}, "Select an audio");

		Debug.Log("Permission result: " + permission);
	}

	public static void PickMixedMedia()
    {
		NativeGallery.Permission permission = NativeGallery.GetMixedMediaFromGallery((path) =>
		{
			Debug.Log("media path: " + path);
			if (path != null)
			{
				NativeGallery.MediaType mediaType = NativeGallery.GetMediaTypeOfFile(path);
				switch(mediaType)
                {
					case NativeGallery.MediaType.Image:
						OnImagePicked?.Invoke(path);
						break;
					case NativeGallery.MediaType.Video:
						OnVideoPicked?.Invoke(path);
						break;
					case NativeGallery.MediaType.Audio:
						OnAudioPicked?.Invoke(path);
						break;
                }
			}
		}, NativeGallery.MediaType.Audio | NativeGallery.MediaType.Image | NativeGallery.MediaType.Video, "Select a file");

		Debug.Log("Permission result: " + permission);
	}
}
