using System.Collections;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InstagramItemPreviewer : MonoBehaviour
{
    public SimpleScrollSnap scrollSnap;
    [SerializeField] private GameObject itemPreviewerPanel;
    [SerializeField] private GameObject itemPrefab;
    [SerializeField] private GameObject parent;

    List<Datum> _dataList = new List<Datum>();
    List<InstagramPreviewItemView> _itemList = new List<InstagramPreviewItemView>();

    public void OnBackButtonPressed()
    {
        itemPreviewerPanel.SetActive(false);

    }

    public void ShowPanel(List<Datum> dataList, int index)
    {
        // DeleteALl();
        itemPreviewerPanel.SetActive(true);

        if (_itemList == null || _itemList.Count <= 0)
        {
           // scrollSnap.enabled = false;
            _dataList = dataList;
            for (int i = 0; i < dataList.Count; i++)
            {
                GameObject obj = Instantiate(itemPrefab);
                obj.transform.parent = parent.transform;
                InstagramPreviewItemView scriptItem = obj.GetComponent<InstagramPreviewItemView>();
                scriptItem.SetUp(dataList[i]);
                _itemList.Add(scriptItem);
            }

            scrollSnap.CustomSetup();

        }
        if (index < dataList.Count)
        {
            StartCoroutine(GoToPanel(index));
        }

       // StartCoroutine(DownloadImage(_data.media_url));
    }

    private IEnumerator GoToPanel(int index)
    {
        yield return new WaitForSeconds(0.1f);
    
            scrollSnap.GoToPanel(index);
    }

    private void DeleteALl()
    {
        if (_itemList == null || _itemList.Count <= 0)
            return;

        for (int i = 0; i < _itemList.Count; i++)
        {
            if (_itemList[i].gameObject != null)
                Destroy(_itemList[i].gameObject);
        }
    }

  
}
