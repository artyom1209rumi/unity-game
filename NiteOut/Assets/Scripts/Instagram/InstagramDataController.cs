using System;
using System.Collections;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;
using UnityEngine.UI;

public class InstagramDataController : MonoBehaviour,IInstagramPostItemView, IWebViewDeligate
{
    public SimpleScrollSnap scroller;

    //[SerializeField] private Text titleTxt;
    //[SerializeField] private GameObject connectButtonObj;
    [SerializeField] private DotItemController paginationController;
    //[SerializeField] private GameObject itemPrefab;
    //[SerializeField] private GameObject parent;
    public List<InstagramPostItemRoot> _itemList = new List<InstagramPostItemRoot>();
    private List<Datum> dataList = new List<Datum>();

    [SerializeField] private SampleWebView webViewController;

    [SerializeField] private InstagramItemPreviewer previewer;

    [SerializeField] private List<GameObject> ui_during_disconnect;
    [SerializeField] private List<GameObject> ui_during_connection;

    private IInstagramRepository _instagramApiRepository;
    int maxItemCount = 0;

    private void Awake()
    {
       // scroller.enabled = false;
    }


    void Start()
    {
       

    }


    public void FetchData()
    {
        //scroller.enabled = false;
        scroller.CustomSetup();
        // connectButtonObj.SetActive(false);
        SetUiGraphics(false);

        string url = ApiConfig.GetInstagramAuthURL();
        // Application.OpenURL(url);
        webViewController.SetUp(url, this);

        if (IsTokenAvailable())
        {
            LoadUserPosts();
        }
        else
        {
            //connectButtonObj.SetActive(true);
            //paginationController.gameObject.SetActive(false);
            //titleTxt.text = "Connect Instagram";
        }
    }



    public void OnConnectButtonPressed()
    {
        webViewController.SetUp(ApiConfig.GetInstagramAuthURL(), this);
        webViewController.ShowWebView();
        //connectButtonObj.SetActive(false);
    }

    private bool IsTokenAvailable()
    {
        if (_instagramApiRepository == null)
            _instagramApiRepository = FindObjectOfType<InstagramRepository>();

        string localToken = _instagramApiRepository.GetInstagramLocalAccessToken();
        if(localToken == null || localToken =="")
            return false;
        else
            return true;
    }

    private void FetchAccessToken(string authCode)
    {
        InstagramAccessTokenRequest request = new InstagramAccessTokenRequest(authCode);
        _instagramApiRepository.GetAccessToken(request, (InstagramAccessTokenResult result) =>
        {
            _instagramApiRepository.StoreAccessTokenLocally(result.access_token);
            //Debug.Log("UNITY>> Load Data response " + result.access_token);

            LoadUserPosts();

        }, (string error) =>
        {
            OnAuthCodeGenerationFailed();
            //Debug.Log("UNITY>> API TEST error is " + error);
        });
    }

    public void LoadUserPosts()
    {
       
        string token = _instagramApiRepository.GetInstagramLocalAccessToken();
        InstagramUserPostsRequest request = new InstagramUserPostsRequest(token);
        _instagramApiRepository.GetInstagramUserPosts(request, (InstagramUserPostResult result) =>
        {
            Debug.Log("UNITY>> User Feed Count  " + result.data.Count);
            // Debug.Log("UNITY>> User Feed Count  " + result.data[0].caption);
            DeleteALl();
            if (result.data != null && result.data.Count > 0)
            {
                SetUiGraphics(true);
                dataList = result.data;

                int mainCount = result.data.Count / 3;
                int remaining = result.data.Count % 3;

                mainCount = mainCount + remaining;
                int count = 0;
                
                for (int i = 0; i < mainCount; i++)
                {
                    List<Datum> perItemDataList = new List<Datum>();
                    for (int j = 0; j < 3; j++)
                    {
                        if(count < result.data.Count)
                        {
                            perItemDataList.Add(result.data[count]);
                            count = count + 1;
                        }
                    }

                    if(perItemDataList.Count > 0)
                    {
                        // GameObject obj = Instantiate(itemPrefab, parent.transform);
                        //obj.transform.parent = parent.transform;
                        //GameObject obj = _itemList[i].gameObject;
                        // InstagramPostItemRoot itemScript = obj.GetComponent<InstagramPostItemRoot>();
                        _itemList[i].SetUp(perItemDataList, this,i);
                       // _itemList.Add(itemScript);
                    }
                   
                }

                maxItemCount = mainCount - 1;
                paginationController.SetUpDotPanel(mainCount - 1 );
            }
            else
            {
                OnAuthCodeGenerationFailed();
            }

        }, (string error) =>
        {
            OnAuthCodeGenerationFailed();
            Debug.Log("UNITY>> API TEST error is " + error);
        });
    }

    private void enableScroller()
    {
        scroller.enabled = true;
    }

    public void OnItemClicked(int index)
    {
        previewer.ShowPanel(dataList,index);
    }

    private void DeleteALl()
    {
        if (_itemList == null || _itemList.Count <= 0)
            return;
        //scroller.DeleteAllReferences();
        for (int i = 0; i < _itemList.Count; i++)
        {
            _itemList[i].ResetItem();
            //if (_itemList[i].gameObject != null)
            //    Destroy(_itemList[i].gameObject);
        }

        //_itemList = new List<InstagramPostItemRoot>();
        //_itemList.Clear();
    }

    public void OnAuthCodeGeneratedCompleted(string code)
    {
       // Debug.Log("UNITY>> DATA CONTROLLER Code is " + code);
        if(string.IsNullOrEmpty(code) == false)
            FetchAccessToken(code);

        webViewController.CloseWebView();

    }

    public void OnAuthCodeGenerationFailed()
    {
        //DeleteALl();
        //Debug.Log("UNITY>> DATA CONTROLLER Code Failed " );
        _instagramApiRepository.ClearInstagramAccessToken();
        //connectButtonObj.SetActive(true);
        //paginationController.gameObject.SetActive(false);
        //titleTxt.text = "Connect Instagram";

        SetUiGraphics(false);
    }

    public void OnScrollItemChanged()
    {
        if(scroller.CurrentPanel == maxItemCount)
        {
            scroller.GoToPreviousPanel();
        }
    }
    public void OnPanelChanging()
    {
        if (scroller.TargetPanel == maxItemCount)
            scroller.GoToPreviousPanel();
    }

    private void SetUiGraphics(bool isConnected)
    {

        if (isConnected)
        {
            for (int i = 0; i < ui_during_connection.Count; i++)
            {
                ui_during_connection[i].SetActive(true);
            }
            for (int i = 0; i < ui_during_disconnect.Count; i++)
            {
                ui_during_disconnect[i].SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < ui_during_connection.Count; i++)
            {
                ui_during_connection[i].SetActive(false);
            }
            for (int i = 0; i < ui_during_disconnect.Count; i++)
            {
                ui_during_disconnect[i].SetActive(true);
            }
        }
    }
}

public interface IWebViewDeligate
{
    void OnAuthCodeGeneratedCompleted(string code);
    void OnAuthCodeGenerationFailed();
}
