using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InstagramPostItemView : MonoBehaviour
{
   // [SerializeField] private Text titleLbl;
    [SerializeField] private RawImage image;

    Datum _data;
    private IInstagramPostItemView _delegate;
    private int _index = -1;

    public void Setup(Datum data, IInstagramPostItemView itemDelegate,int index)
    {
        _index = index;
        _data = data;
        _delegate = itemDelegate;
        SetUI();
    }

    private void SetUI()
    {
       // titleLbl.text = _data.title;
        // Debug.Log("UNITY>> title " + _data.title + "  Url  " + _data.posterUrl);
        StartCoroutine(DownloadImage(_data.media_url));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
            image.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
    }

    public void OnItemClicked()
    {
        _delegate?.OnItemClicked(_index);
    }
}

public interface IInstagramPostItemView
{
    void OnItemClicked(int index);
}
