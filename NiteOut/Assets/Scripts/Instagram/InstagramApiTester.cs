using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstagramApiTester : MonoBehaviour
{

    private IInstagramRepository _instagramApiRepository;
    void Start()
    {
        _instagramApiRepository = GetComponent<InstagramRepository>();

        //LoadData();
        LoadUserPosts();
    }


    //public void LoadData()
    //{
    //    InstagramAccessTokenRequest request = new InstagramAccessTokenRequest();
    //    _instagramApiRepository.GetAccessToken(request, (InstagramAccessTokenResult result) =>
    //    {
    //        Debug.Log("UNITY>> Load Data response " + result.access_token);
    //    }, (string error) =>
    //    {

    //        Debug.Log("UNITY>> API TEST error is " + error);
    //    });
    //}

    public void LoadUserPosts()
    {
        InstagramUserPostsRequest request = new InstagramUserPostsRequest(_instagramApiRepository.GetInstagramLocalAccessToken());
        _instagramApiRepository.GetInstagramUserPosts(request, (InstagramUserPostResult result) =>
        {
            Debug.Log("UNITY>> User Feed Count  " + result.data.Count);
            Debug.Log("UNITY>> User Feed Count  " + result.data[0].caption);


        }, (string error) =>
        {

            Debug.Log("UNITY>> API TEST error is " + error);
        });
    }
}
