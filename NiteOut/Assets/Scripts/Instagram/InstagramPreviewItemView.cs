using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InstagramPreviewItemView : MonoBehaviour
{
    [SerializeField] private RawImage image;

    Datum _data;

    public void SetUp(Datum data)
    {
        _data = data;
        StartCoroutine(DownloadImage(_data.media_url));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            image.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            image.gameObject.SetActive(true);
        }

    }
}
