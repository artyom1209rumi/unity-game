using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstagramPostItemRoot : MonoBehaviour
{
    public HorizontalLayoutGroup horizontalLayoutGroup;
    public LayoutElement layoutElement; 
    public List<InstagramPostItemView> _items = new List<InstagramPostItemView>();

    public void ResetItem()
    {
        for (int i = 0; i < _items.Count; i++)
        {
            _items[i].gameObject.SetActive(false);
        }

        this.gameObject.SetActive(false);
    }

    public void SetUp(List<Datum> datas, IInstagramPostItemView itemDeligate, int index)
    {
        ResetItem();
        this.gameObject.SetActive(true);
        float itemWidth = Screen.width - 72;
        layoutElement.preferredWidth = itemWidth;
        float spacing = itemWidth / 3;
        spacing = spacing - 280;

        if (spacing < 0)
            spacing = 0;

        horizontalLayoutGroup.spacing = spacing;

        for(int i = 0; i < datas.Count; i++)
        {
            _items[i].gameObject.SetActive(true);
            _items[i].Setup(datas[i], itemDeligate, ((index * 3) + i));
        }
    }
}
