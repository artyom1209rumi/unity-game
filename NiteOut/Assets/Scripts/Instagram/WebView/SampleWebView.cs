/*
 * Copyright (C) 2012 GREE, Inc.
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;



public class SampleWebView : MonoBehaviour
{
    // public GameObject webViewUi;
    [SerializeField] private GameObject canvasUI;
    private string Url;
    private WebViewObject webViewObject;
    private bool _isLoad = false;

    //public SampleWebView webView;
  //  private bool authComplete;
    public Uri uri;

   // string msg;

  //  string token = "AQBJUruAB0N6uHSwcrl6QSn7KzNBbbRQpMteJQBglb8XoCthu5NYBKNK06Q0ySgMwtgVxxmRbyHhDGmbt7lEqPCiR2tHp9wQkrBZH2NtNR8lZWSSxXBEboJR3o4Sbi5lbmqjqNJmMorxyBg4hzLJNBedtPudyeYI-6A6RvDYuNNrsjWc7IibSVPiJapebB2N71zAVlViYyYPKRcPhVuZFxC5inQaakQZMK2LiIL26hwxhA";

    private IWebViewDeligate _deligate;
    private string _url;


    public void SetUp(string webUrl, IWebViewDeligate _webDeligate)
    {
        _url = webUrl;
        _deligate = _webDeligate;
    }


    //private void Start()
    //{
        
    //}
  
    public void ShowWebView()
    {
        canvasUI.SetActive(true);
        // ClearCokies();
        //string url = "https://www.google.com/";
       // _url = ApiConfig.GetInstagramAuthURL();
        string url = _url;//"https://api.instagram.com/oauth/authorize?app_id=342102897919786&redirect_uri=https://ashtoy.com/&scope=user_profile,user_media&response_type=code";
        StartCoroutine(LoadWebView(url));

    }

    public void CloseWebView()
    {
        //canvasUI.SetActive(false);
        //Destroy(webViewObject.gameObject);
        ClearCokies();
        canvasUI.SetActive(false);
    }

    public void ClearCokies()
    {
        //textM.text = "Welcome in Clear Cokie";
        webViewObject.ClearCookies();
        Destroy(webViewObject.gameObject);
       
    }


    public void getAccessToken(string myurl)
    {
       // Debug.Log("UNITY>> return URL " + myurl);
        string url1 =myurl;
        string accessToken = "code";
        string error = "? error";

        string containingUrl = ApiConfig.INSTAGRAM_REDIRECT_URL + "?code=";
        //if (myurl.Contains("https://ashtoy.com/?code="))
        if (myurl.Contains(containingUrl))
        {
            var data = myurl.Substring(myurl.LastIndexOf("=") + 1);
            //Debug.Log("UNITY>> Code >>  " + data);
            string actualCode = data.Substring(0, data.Length - 2);
           // Debug.Log("UNITY>> Actual Code >>  " + actualCode);
            //PlayerPrefs.SetInt("authComplete", 1);
            //PlayerPrefs.SetString("token", actualCode);
            _deligate.OnAuthCodeGeneratedCompleted(actualCode);

        }
        else
        {
            _deligate.OnAuthCodeGenerationFailed();
            // textM.text = "Something Went wrong";
        }
    }
   
   

   

    private IEnumerator LoadWebView(string url)
    {
        //if(_isLoad == false)
        //{
        //    _isLoad = true;
        //}
        //else
        //{
        //    webViewObject.gameObject.SetActive(true);
        //    yield return null;
        //}

        webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
        //if (webViewObject == null)
        //    webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();


       // webViewObject.gameObject.SetActive(true);       
        Url = url;              
      
        webViewObject.Init(
            cb: (msg) => { },
            err: (msg) => {
                _deligate?.OnAuthCodeGenerationFailed();
            },
            started: (msg) =>  { },
            ld: (msg) =>
            {
                Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
               getAccessToken(msg);
           
#if UNITY_EDITOR_OSX || !UNITY_ANDROID
                // NOTE: depending on the situation, you might prefer
                // the 'iframe' approach.
                // cf. https://github.com/gree/unity-webview/issues/189
#if true
                webViewObject.EvaluateJS(@"
                  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                    window.Unity = {
                      call: function(msg) {
                        window.webkit.messageHandlers.unityControl.postMessage(msg);
                      }
                    }
                  } else {
                    window.Unity = {
                      call: function(msg) {
                        window.location = 'unity:' + msg;
                      }
                    }
                  }
                ");
#else
                webViewObject.EvaluateJS(@"
                  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                    window.Unity = {
                      call: function(msg) {
                        window.webkit.messageHandlers.unityControl.postMessage(msg);
                      }
                    }
                  } else {
                    window.Unity = {
                      call: function(msg) {
                        var iframe = document.createElement('IFRAME');
                        iframe.setAttribute('src', 'unity:' + msg);
                        document.documentElement.appendChild(iframe);
                        iframe.parentNode.removeChild(iframe);
                        iframe = null;
                      }
                    }
                  }
                ");
#endif
#endif
                webViewObject.EvaluateJS(@"Unity.call('ua=' + navigator.userAgent)");
            },
            //ua: "custom user agent string",
            enableWKWebView: true);
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        webViewObject.bitmapRefreshCycle = 1;
#endif
        //webViewObject.SetMargins(5, 100, 5, Screen.height / 4);
        webViewObject.SetMargins(20, Screen.height / 5, 20, Screen.height / 5);
        webViewObject.SetVisibility(true);

#if !UNITY_WEBPLAYER
        Debug.Log("my url " + Url);
        if (Url.StartsWith("http")) {
            webViewObject.LoadURL(Url.Replace(" ", "%20"));
        } else {
            var exts = new string[]{
                ".jpg",
                ".js",
                ".html" 
                
                // should be last
            };
            foreach (var ext in exts) {
                var processedUrl = Url.Replace(".html", ext);
                var src = System.IO.Path.Combine(Application.streamingAssetsPath, processedUrl);
                var dst = System.IO.Path.Combine(Application.persistentDataPath, processedUrl);
                byte[] result = null;
                if (src.Contains("://")) {  // for Android
                    var www = new WWW(src);
                    yield return www;
                    result = www.bytes;
                   // textM.text = result.ToString();
                } else {
                    result = System.IO.File.ReadAllBytes(src);
                   // textM.text = result.ToString();
                }
                System.IO.File.WriteAllBytes(dst, result);
                if (ext == ".html") {
                    webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
                    break;
                }
            }
        }
#else
        if (Url.StartsWith("http")) {
            webViewObject.LoadURL(Url.Replace(" ", "%20"));
        } else {
            webViewObject.LoadURL("StreamingAssets/" + Url.Replace(" ", "%20"));
        }
        webViewObject.EvaluateJS(
            "parent.$(function() {" +
            "   window.Unity = {" +
            "       call:function(msg) {" +
            "           parent.unityWebView.sendMessage('WebViewObject', msg)" +
            "       }" +
            "   };" +
            "});");
#endif
        yield break;
       
    }
}
