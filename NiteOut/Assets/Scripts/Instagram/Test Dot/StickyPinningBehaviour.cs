using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickyPinningBehaviour : MonoBehaviour
{
    [SerializeField] private ScrollRect rootScroller;
    [SerializeField] private ScrollRect childScroller;




    public int topMargin = 300;
    private bool isChildScrollingUP = false;
    float prevChildPos;

    private void FixedUpdate()
    {
        float childPos = childScroller.verticalNormalizedPosition;
        float rootPos = rootScroller.verticalNormalizedPosition;

        if (rootPos <= .3)
        {
            if (childPos >= 1)
            {
                if (prevChildPos > childPos)
                {
                    Debug.Log("Move UP ");
                    rootScroller.enabled = false;
                    childScroller.enabled = true;
                }
                else if (prevChildPos < childPos)
                {
                    rootScroller.enabled = true;
                    childScroller.enabled = false;
                    Debug.Log("Move Down ");
                }
                else
                {
                    Debug.Log("Neutral");
                    rootScroller.enabled = false;
                    childScroller.enabled = true;
                }

            }
        }
        prevChildPos = childPos;
    }

    private void Update1()
    {
        // Debug.Log("Child Pos  " + childScroller.verticalNormalizedPosition + " prev  " + prevChildPos + "Diff " +);
        float childPos = childScroller.verticalNormalizedPosition;
        float rootPos = rootScroller.verticalNormalizedPosition;


        //if (prevChildPos > childPos)
        //{
        //    Debug.Log("Move UP ");
        //}
        //else if (prevChildPos < childPos)
        //{
        //    Debug.Log("Move Down ");
        //}
        //else
        //{
        //    Debug.Log("Neutral");
        //}

       
        // float diff = (childPos - prevChildPos);

        if (rootPos <= .3)
        {
            if (childPos >= 1)
            {
                if (prevChildPos > childPos)
                {
                    Debug.Log("Move UP ");
                    rootScroller.enabled = false;
                    childScroller.enabled = true;
                }
                else if (prevChildPos < childPos)
                {
                    rootScroller.enabled = true;
                    childScroller.enabled = false;
                    Debug.Log("Move Down ");
                }
                else
                {
                    Debug.Log("Neutral");
                    rootScroller.enabled = false;
                    childScroller.enabled = true;
                }

            }
        }
        prevChildPos = childPos;
        //Debug.Log("Root Pos  " + rootScroller.verticalNormalizedPosition + " is enebled " + rootScroller.enabled);
        //Debug.Log("Child Pos  " + childScroller.verticalNormalizedPosition + " is enebled " + childScroller.enabled);
        //if (rootScroller.verticalNormalizedPosition <= .3 )
        //{
        //    //if(childScroller.verticalNormalizedPosition >= 1)
        //    //{
        //    //    //isChildScrollingUP = true;
        //    //    //SetUpScrollRect(isReachedTop);
        //    //    rootScroller.enabled = false;
        //    //    childScroller.enabled = true;

        //    //}


        //    if ( childPos <= 0.98 && childPos >=1)
        //    {
        //        rootScroller.enabled = false;
        //        childScroller.enabled = true;
        //    }

        //    else if (childScroller.verticalNormalizedPosition < 1)
        //    {
        //        isChildScrollingUP = true;

        //        rootScroller.enabled = false;
        //        childScroller.enabled = true;


        //    }
        //    if (childScroller.verticalNormalizedPosition > 1)
        //    {
        //        isChildScrollingUP = false;


        //    }

        //    //if (childScroller.verticalNormalizedPosition <= 0)
        //    //{
        //    //    rootScroller.enabled = true;
        //    //    childScroller.enabled = false;
        //    //}

        //    //rootScroller.enabled = false;
        //    //childScroller.enabled = true;
        //}
        //else
        //{
        //    //isReachedTop = false;
        //    //SetUpScrollRect(isReachedTop);
        //}
    }

    public void OnRootScrollerValueChanged()
    {
        float pos = rootScroller.verticalNormalizedPosition;
       

        //if(rootScroller.verticalNormalizedPosition <= .3 && childScroller.verticalNormalizedPosition <= 0)
        //{
        //    isReachedTop = true;
        //    SetUpScrollRect(isReachedTop);
        //    //rootScroller.enabled = false;
        //    //childScroller.enabled = true;
        //}
        //else
        //{
        //    isReachedTop = false;
        //    SetUpScrollRect(isReachedTop);
        //}
      
    }

    public void OnChildScrollerValueChanged()
    {
        float pos = childScroller.verticalNormalizedPosition;
       

        //if (pos >= 1 && isReachedTop == true)
        //{
        //    isReachedTop = false;
        //    SetUpScrollRect(isReachedTop);

        //}
        //else
        //{
        //    isReachedTop = true;
        //    SetUpScrollRect(isReachedTop);
        //}
    }

    private void SetUpScrollRect(bool isSticked)
    {
        if (isSticked)
        {
            rootScroller.enabled = false;
            childScroller.enabled = true;
        }
        else
        {
            rootScroller.enabled = true;
            childScroller.enabled = false;
        }
    }

}
