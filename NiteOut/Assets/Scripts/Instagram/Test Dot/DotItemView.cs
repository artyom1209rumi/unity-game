using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotItemView : MonoBehaviour
{
    [SerializeField] private Image dotImage;
    [SerializeField] private Color selectedColor = Color.blue;
    [SerializeField] private Color unselectedColor = Color.white;


    private bool _isSelected = false;

    public void Setup()
    {
        _isSelected = false;
    }

    public void SetSelected(bool isSelected)
    {
        _isSelected = isSelected; 
    }

    void SetGraphics()
    {
        if (_isSelected)
            dotImage.color = selectedColor;
        else
            dotImage.color = unselectedColor;
    }


}
