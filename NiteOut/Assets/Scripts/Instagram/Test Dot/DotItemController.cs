using System.Collections;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;
using UnityEngine.UI;

public class DotItemController : MonoBehaviour
{
    public SimpleScrollSnap scroller;

   // [SerializeField] private GameObject itemPrefab;
    [SerializeField] private GameObject parent;

    public List<GameObject> _itemList = new List<GameObject>();

    public void SetUpDotPanel(int count)
    {
        DeleteALl();
        for (int i = 0; i < count; i++)
        {
            if( count < _itemList.Count)
            {
                _itemList[i].SetActive(true);
            }
        }

        scroller.CustomSetup();
        scroller.pagination = parent;
        scroller.ResetPaggination();
    }


    private void DeleteALl()
    {
        if (_itemList == null || _itemList.Count <= 0)
            return;
       // scroller.DeleteAllReferences();
        for (int i = 0; i < _itemList.Count; i++)
        {
            _itemList[i].SetActive(false);
        }
    }

}
