using System.Linq;
using System.Text.RegularExpressions;

public static class PasswordChecker
{
    public static PasswordStrength GetPasswordStrength(string password)
    {
        int score = 0;
        if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(password.Trim())) return PasswordStrength.Blank;
        if (HasMinimumLength(password, 5)) score++;
        if (HasMinimumLength(password, 8)) score++;
        if (HasUpperCaseLetter(password) && HasLowerCaseLetter(password)) score++;
        if (HasDigit(password)) score++;
        if (HasSpecialChar(password)) score++;
        return (PasswordStrength)score;
    }

    public static bool HasMinimumLength(string password, int minLength)
    {
        return password.Length >= minLength;
    }

    public static bool HasMinimumUniqueChars(string password, int minUniqueChars)
    {
        return password.Distinct().Count() >= minUniqueChars;
    }

    /// <summary>
    /// Returns TRUE if the password has at least one digit
    /// </summary>
    public static bool HasDigit(string password)
    {
        return Regex.IsMatch(password, @"[0-9]");
    }

    /// <summary>
    /// Returns TRUE if the password has at least one special character
    /// </summary>
    public static bool HasSpecialChar(string password)
    {
        return Regex.IsMatch(password, @"[^A-Za-z0-9]");
    }

    /// <summary>
    /// Returns TRUE if the password has at least one uppercase letter
    /// </summary>
    public static bool HasUpperCaseLetter(string password)
    {
        return Regex.IsMatch(password, @"[A-Z]");
    }

    /// <summary>
    /// Returns TRUE if the password has at least one lowercase letter
    /// </summary>
    public static bool HasLowerCaseLetter(string password)
    {
        return Regex.IsMatch(password, @"[a-z]");
    }

    public static bool IsValidPassword(string password)
    {
        return (int)GetPasswordStrength(password) > 1;
    }
}

public enum PasswordStrength
{
    /// <summary>
    /// Blank Password (empty and/or space chars only)
    /// </summary>
    Blank = 0,
    /// <summary>
    /// Either too short (less than 5 chars), one-case letters only or digits only
    /// </summary>
    VeryWeak = 1,
    /// <summary>
    /// At least 5 characters, one strong condition met (>= 8 chars with 1 or more UC letters, LC letters, digits & special chars)
    /// </summary>
    Weak = 2,
    /// <summary>
    /// At least 5 characters, two strong conditions met (>= 8 chars with 1 or more UC letters, LC letters, digits & special chars)
    /// </summary>
    Medium = 3,
    /// <summary>
    /// At least 8 characters, three strong conditions met (>= 8 chars with 1 or more UC letters, LC letters, digits & special chars)
    /// </summary>
    Strong = 4,
    /// <summary>
    /// At least 8 characters, all strong conditions met (>= 8 chars with 1 or more UC letters, LC letters, digits & special chars)
    /// </summary>
    VeryStrong = 5
}
