using UnityEngine.UI;

public static class ImageSelecter
{
    public static void Select(ref Image lastSelectedImage, Image currentSelectedImage)
    {
        if (lastSelectedImage == currentSelectedImage) // the button is already selected so do nothing
            return;
        if (lastSelectedImage != null) // disable the last selected button before selecting the new one
            lastSelectedImage.enabled = false;
        lastSelectedImage = currentSelectedImage;
        currentSelectedImage.enabled = true;
    }
}
