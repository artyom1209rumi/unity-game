using UnityEngine;
using UnityEngine.UI;

public static class ImageAspectRatioFitter
{
    public static void SizeToParent(this RawImage image)
    {
        Texture texture = image.texture;
        if (texture.width > texture.height)
            image.transform.localScale = new Vector3(((float)texture.width / texture.height), 1, 1);
        else if (texture.height > texture.width)
            image.transform.localScale = new Vector3(1, ((float)texture.height / texture.width), 1);
        else
            image.transform.localScale = Vector3.one;
    }
}
