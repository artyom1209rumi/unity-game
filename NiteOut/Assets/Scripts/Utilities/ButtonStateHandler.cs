using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonStateHandler : MonoBehaviour
{
    public static ButtonStateHandler Instance { get; private set; }
    [Header("Settings")]
    [SerializeField]
    private Color disabledButtonColor;
    [SerializeField]
    private Color enabledButtonColor;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }

    public void SetInteractable(Button button, bool interactable)
    {
        if (interactable)
        {
            button.interactable = true;
            button.GetComponent<Image>().color = enabledButtonColor;
            button.transform.GetComponentInChildren<TextMeshProUGUI>().color = enabledButtonColor;
        }
        else
        {
            button.interactable = false;
            button.GetComponent<Image>().color = disabledButtonColor;
            button.transform.GetComponentInChildren<TextMeshProUGUI>().color = disabledButtonColor;
        }
    }
}
