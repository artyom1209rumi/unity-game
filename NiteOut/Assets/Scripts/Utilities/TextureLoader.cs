using System.IO;
using UnityEngine;

public static class TextureLoader
{
    public static Texture LoadFromFilePath(string filePath)
    {
        try
        {
            return LoadFromBytes(File.ReadAllBytes(filePath));
        }
        catch
        {
            return null;
        }
    }

    public static Texture LoadFromBytes(byte[] imageBytes)
    {
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(imageBytes);
        return texture;
    }
}
