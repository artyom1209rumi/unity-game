using UnityEngine;

public static class ScreenObserver
{
    public delegate void ScreenChangeHandler();
    public static event ScreenChangeHandler ScreenChanged;

    private static int lastScreenWidth, lastScreenHeight;

    public static void Initialize()
    {
        lastScreenWidth = Screen.width;
        lastScreenHeight = Screen.height;
    }

    public static void DetectScreenChanges()
    {
        int newScreenWidth = Screen.width;
        int newScreenHeight = Screen.height;
        if (lastScreenWidth != newScreenWidth || lastScreenHeight != newScreenHeight)
        {
            lastScreenWidth = newScreenWidth;
            lastScreenHeight = newScreenHeight;
            ScreenChanged?.Invoke();
        }
    }
}
