using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MusicNetworked : NetworkBehaviour
{
    public string DebugSong = "https://open.spotify.com/playlist/37i9dQZF1DX7SEhw42DW5b?si=278f76ffa0d44798";

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            RpcPlaySong(DebugSong);
        }
    }

    [ClientRpc]
    public void RpcPlaySong(string uri)
    {
        MusicManager.instance.PlaySong(uri);
    }
}
