using NiteOut.Model;
using NiteOut.View;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Mirror;

public class BarUI : MonoBehaviour
{
    public static BarUI instance;
    [SerializeField]
    private GameObject chatCanvas;
    [SerializeField]
    private GameObject chatsPanel;
    [SerializeField]
    private GameObject profileCanvas;
    [SerializeField]
    private GameObject joystick;
    [SerializeField]
    private GameObject danceButton;
    [SerializeField]
    private GameObject closeDanceButton;
    [SerializeField]
    private GameObject danceMovesPanel;
    [SerializeField]
    private Image redDotImage;
    [SerializeField]
    private GameObject playingMusicPanel;
    [SerializeField]
    private GameObject connectoToSpotifyButton;
    [SerializeField]
    private TextMeshProUGUI[] playingMusicTitles;

    private void Awake()
    {
        if(DatabaseHelper.Database != null){
            DatabaseHelper.Database.ChatMessageAdded += OnChatMessageAdded;
        
            foreach(var conversation in DatabaseHelper.Database.LoadConversations())
            {
                foreach (ChatMessage chatMessage in conversation.Value)
                    OnChatMessageAdded(conversation.Key, chatMessage);
            }

        }
        instance = this;
        gameObject.SetActive(false);
    }

    /*
    private void LateUpdate()
    {
        switch(SwipeManager.Instance.SwipeDirection)
        {
            case SwipeDirection.Left:
                if (!chatCanvas.activeSelf)
                    OnMessageButtonClicked();
                break;
            case SwipeDirection.Up:
                if (!profileCanvas.activeSelf)
                    OnProfileButtonClicked();
                break;
        }
        SwipeManager.Instance.ResetSwipe();
    }
    */

    public void OnSwipeRight()
    {
        if (chatCanvas.activeSelf && !profileCanvas.activeSelf && chatsPanel.activeSelf)
            ChatUI.Instance.CloseChat();
    }

    public void OnSwipeLeft()
    {
        if (!chatCanvas.activeSelf && !profileCanvas.activeSelf && gameObject.activeSelf)
            OnMessageButtonClicked();
    }

    public void OnSwipeUp()
    {
        if (!profileCanvas.activeSelf && !chatCanvas.activeSelf && gameObject.activeSelf)
            OnProfileButtonClicked();
    }

    private void OnChatMessageAdded(string conversationID, ChatMessage chatMessage)
    {
        print(chatMessage.Content + " " + chatMessage.HasRead);
        if (chatMessage.SenderID == DatabaseHelper.User.UserID || chatMessage.HasRead) // no need to notify if you are the one sending the message
            return;
        redDotImage.enabled = true;
        if (ChatUI.Instance.CurrentConversationID != conversationID)
            ChatUI.Instance.ShowRedDot(conversationID, true);
    }

    private void OnDestroy()
    {
        DatabaseHelper.Database.ChatMessageAdded -= OnChatMessageAdded;
    }

    public void ShowRedDot(bool show)
    {
        redDotImage.enabled = show;
    }

    public void OnMessageButtonClicked()
    {
        ChatUI.Instance.OpenChat();
        gameObject.SetActive(false);
    }

    public void OnProfileButtonClicked()
    {
        ProfileUI.Instance.ShowProfile(DatabaseHelper.User.UserID);
        //gameObject.SetActive(false);
        // this is sometimes throwing an error
        if (ProfileModelStudio.Instance != null && PlayerManager.LocalPlayer != null)
            ProfileModelStudio.Instance.UpdateModel(PlayerManager.LocalPlayer.GetComponent<NetworkAnimator>().animator.gameObject);
    }

    public void OnDanceMoveButtonClicked(TextMeshProUGUI danceMoveText)
    {
        print("We clicked on " + danceMoveText.text + " dance");
        PlayerManager.LocalPlayer.PlayDance(danceMoveText.text);
    }

    public void OnDanceButtonClicked()
    {
        ShowDanceMenu(true);
    }

    public void OnCloseDanceButtonClicked()
    {
        ShowDanceMenu(false);
    }

    private void ShowDanceMenu(bool show)
    {
        if (show)
        {
            danceMovesPanel.SetActive(true);
            closeDanceButton.SetActive(true);
            danceButton.SetActive(false);
            joystick.SetActive(false);
        }
        else
        {
            danceMovesPanel.SetActive(false);
            closeDanceButton.SetActive(false);
            danceButton.SetActive(true);
            joystick.SetActive(true);
        }
    }

    public void UpdateSong(string song)
    {
        Debug.Log("UpdateSong: " + song);
        if (string.IsNullOrWhiteSpace(song))
        {
            connectoToSpotifyButton.SetActive(true);
            playingMusicPanel.SetActive(false);
        }
        else
        {
            connectoToSpotifyButton.SetActive(false);
            playingMusicPanel.SetActive(true);
            playingMusicTitles[0].text = playingMusicTitles[1].text = song;
        }
    }

    public void OnConnectToSpotifyButtonClicked()
    {
         MusicManager.instance.AuthorizeSpotify(() =>
         {
             // do something upong connecting
            UpdateSong("Starting spotify connection...");
         });
        //MusicManager.instance.PlaySong("spotify:track:20I6sIOMTCkB6w7ryavxtO");
    }
}
