using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBakerController : MonoBehaviour
{
    public MB3_MeshBakerCommon meshBaker;
    public static MeshBakerController instance;
    GameObject[] objs = new GameObject[1];
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (meshBaker == null)
            meshBaker = GetComponent<MB3_MeshBakerCommon>();
    }

    // Update is called once per frame
    void Update()
    {
        meshBaker.UpdateGameObjects(meshBaker.GetObjectsToCombine().ToArray());
    }

    internal void BakeModel(MeshRenderer[] meshes)
    {
        List<GameObject> mesh = new List<GameObject>();
        foreach (var item in meshes)
        {
            mesh.Add(item.gameObject);
        }
        meshBaker.AddDeleteGameObjects(mesh.ToArray(), null,true);
        meshBaker.Apply(false, true, false, false, false, false, false, false, false);
    }
}
