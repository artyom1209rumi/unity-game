using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Globals : NetworkBehaviour
{
    public static Globals Instance;

    public void Awake()
    {
        Instance = this;
    }

    public static void AddProfile(string UUID, ProfileData data)
    {
        //if (Instance.Profiles.ContainsKey(UUID))
        //{
        //    //Destroy old profile data
        //    Instance.Profiles.Remove(UUID);
        //}
        //Instance.Profiles.Add(UUID, data);

    }

    Dictionary<string, ProfileData> Profiles = new Dictionary<string, ProfileData>();
    public static ProfileData GetNameFromUUID(string UUID)
    {
        ProfileData data = null;
        Instance.Profiles.TryGetValue(UUID, out data);
        if (data == null)
            Debug.LogError("Could not find ProfileData with UUID: " + UUID);
        return data;
    }
}
