﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Web;
using UnityEngine.UI;
using Newtonsoft.Json;

public class AuthDiaLog : MonoBehaviour
{
    public Text textM;
    string Req_Url ="https://api.instagram.com/v1/users/self/?access_token=";
    public Text userNameText;
    int i = 0;
    public Image userProfile;
    private void FixedUpdate()
    {

     //   textM.text =PlayerPrefs.GetString("token");
        if (PlayerPrefs.GetInt("authComplete")== 1)
        {
            textM.text = "Requested";
            Request();
           
        }
       
    }
    public void Request()
    {
       
        i++;
        userNameText.text = "" + i;
       
            string NewReqUrl =Req_Url+PlayerPrefs.GetString("token");
            
            WWW request = new WWW(NewReqUrl);
            PlayerPrefs.SetInt("authComplete", 0);
            textM.text = "Requested2";
            StartCoroutine(OnResponse(request));
        
    }
    private IEnumerator OnResponse(WWW req)
    {
        yield return req;
        textM.text = req.text;
        //Json Parsing

        // RootObject user = JsonUtility.FromJson<RootObject>(req.text);
        // string userName = user.data.full_name;
        // userNameText.text=userName+":"+i;
        var rt = JsonConvert.DeserializeObject<RootObject>(req.text);
        string fullName=rt.data.full_name;
        userNameText.text = fullName+":"+i;
        string userProfile = rt.data.profile_picture;
        StartCoroutine(loadImage(userProfile));
    }

    IEnumerator loadImage(string url)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {

            yield return null;
        }
        var www = new WWW(url);
        Debug.Log("Download image on progress");
        yield return www;
        if (string.IsNullOrEmpty(www.text))
            Debug.Log("Download Failed");
        else
        {
            Debug.Log("Download Success");
            Texture2D texture = new Texture2D(128, 128);  //create tex=1 and height=1
            www.LoadImageIntoTexture(texture);

            userProfile.sprite= Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one / 2);
            // btn= GetComponent<Button>();
           
        }

    }

}
/*
[Serializable]
public class UserData
{
    public List<Data> data;
}
[Serializable]
public class Data
{
    public string id;
    public string username;
    public string profile_picture;
    public string full_name;
}
*/

public class Counts
{
    public int media { get; set; }
    public int follows { get; set; }
    public int followed_by { get; set; }
}

public class Data
{
    public string id { get; set; }
    public string username { get; set; }
    public string profile_picture { get; set; }
    public string full_name { get; set; }
    public string bio { get; set; }
    public string website { get; set; }
    public bool is_business { get; set; }
    public Counts counts { get; set; }
}

public class Meta
{
    public int code { get; set; }
}

public class RootObject
{
    public Data data { get; set; }
    public Meta meta { get; set; }
}
